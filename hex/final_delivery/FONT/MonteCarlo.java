
import java.util.List;

/**
 * Create the MonteCarlo algorithm to compute the next best move
 * @author hex team
 */
public class MonteCarlo extends Strategy {
    
  /*
    private void transforNode(Node node, NodeMonte nodeMonte){
         for (NodeMonte m : nodeMonte.childrenlist) {
                Node n = new Node();
                n.depth = m.depth;              
                n.setWeight(m.getWeight());
                Coordinates co = new Coordinates(m.getx(),m.gety(), m.getz());
                n.setCoordinate(co);
                if (!m.childrenlist.isEmpty()) {
                    transforNode(n,m);
                }
                node.childrenlist.add(n);
         }
    }
    */
    /**
     * Compute the next best move with MonteCarlo Algorithm
     * @param board
     * @param playerColor
     * @param width 
     * @param depth 
     * @return
     */
    @Override
    public Coordinates computeBestMove(Board board, PlayerColor playerColor,
            int width, int depth) {
        NodeMonte treeRootMonte = new NodeMonte();
        treeRootMonte.setColor(playerColor);
        treeRootMonte.setboard(board);

        int[] res = monte_carlo(width, treeRootMonte, depth);
        treeRoot = treeRootMonte.noderoot;
        return new Coordinates(res[1], res[2], res[3]);
    }

    /**
     * compute the next best move with MonteCarlo Algorithm
     * @param iterations
     * @param player
     * @param depth
     * @return
     */
    private int[] monte_carlo(int iterations, NodeMonte player, int depth) {
        int[] bestvalue = {99999999, 0, 0, 0, 0};
        creatTree(player, depth, iterations);

        for (int i = 0; i < iterations; i++) {
            int[] value = player.selectAction(depth);
            if (value[0] < bestvalue[0]) {
                bestvalue[0] = value[0];
                bestvalue[1] = value[1];
                bestvalue[2] = value[2];
                bestvalue[3] = value[3];
                bestvalue[4] = value[4];
            }
        }
        return bestvalue;
    }

    private void creatTree(NodeMonte root, int depth, int iteratoins) {
        List<int[]> nextMoves = root.generateMoves(iteratoins);
        if (depth != 0) {
            for (int[] move : nextMoves) {
                NodeMonte child = new NodeMonte();
                root.Monte_childrenlist.add(child);
                for (Cell c : root.cells) {
                    child.cells.add(c);
                }
                Coordinates coordinates = new Coordinates(move[0], move[1], move[2]);
		child.setWeight(move[3]);
                if (root.getColor() == PlayerColor.black) {
                    child.setColor(PlayerColor.white);
                    Cell cell = new Cell(PlayerColor.white, coordinates);
                    child.cells.add(cell);
                    child.generateNextBoard(child.cells, root.board.getSize());
                } else if (root.getColor() == PlayerColor.white) {
                    child.setColor(PlayerColor.black);
                    Cell cell = new Cell(PlayerColor.black, coordinates);
                    child.cells.add(cell);
                    child.generateNextBoard(child.cells, root.board.getSize());
                }
                child.depth = root.depth;
                child.depth++;
                child.setCoordinate(coordinates);
                NodeRoot nr = new NodeRoot();
                nr.depth = root.depth+1 ;
                nr.setCoordinate(coordinates);
                nr.setWeight(move[3]);
                child.noderoot = nr;
                creatTree(child, depth - 1, iteratoins);
                root.Monte_childrenlist.add(child);
                root.noderoot.childrenlist.add(nr);
            }
        }

    }
}
