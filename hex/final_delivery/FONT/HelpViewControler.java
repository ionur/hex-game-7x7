/**
 * 
 * @author Hex team
 */
public class HelpViewControler {

    private HelpView helpView;

    /**
     * 
     */
    public HelpViewControler() {
        helpView = new HelpView(this);
    }
    
    /**
     * 
     * @return
     */
    public AbstractTab getView() {
        return helpView;
    }

    /**
     * 
     */
    public void displayHelp() {
        
    }
}