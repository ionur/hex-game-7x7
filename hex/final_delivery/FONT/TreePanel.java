import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Panel containing the tree representation of the algorithm
 * @author Hex team
 */
public class TreePanel extends JPanel {

    private PlayViewControler playViewControler;
    private JScrollPane treePane;
    private int depth;
    private int width;
    private ArrayList<int[]> info;
    private int currInd;

    /**
     * Constructor
     * @param playViewControler the play view controler
     */
    public TreePanel(PlayViewControler playViewControler) {
        this.playViewControler = playViewControler;
        depth = 0;
        width = 0;
        info = null;
        currInd = 0;
    }

    private DefaultMutableTreeNode initTreePane() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Execution tree");
        JTree tree = new JTree(root);
        treePane = new JScrollPane(tree);
        treePane.setPreferredSize(this.getSize());
        add(treePane);
        repaint();
        return root;
    }
    
    /**
     * Updates the tree with the new values
     * @param info the values of the nodes
     */
    public void updateTree(ArrayList<int[]> info) {
        if (treePane != null) {
            remove(treePane);
            repaint();
        }
        
        DefaultMutableTreeNode root = initTreePane();

        this.info = info; 
        currInd = 0;
        depth = 0;
        width = 0;
        for (int i = 0; i < info.size(); ++i) {
            int[] t = info.get(i);
            depth = (t[0] > depth) ? t[0] : depth;
            if (t[0] == 1) {
                width++;
            }
        }

        buildSubTree(root, 0);
    }

    private void buildSubTree(DefaultMutableTreeNode root, int currDepth) {
        if (currDepth == depth) {
            return;
        }

        for (int i = 0; i < width; ++i) {
            if (currInd >= info.size()) {
                return;
            }
            int[] t = info.get(currInd);
            if (t[0] == currDepth + 1) {
                currInd++;
                String nodeString = "depth=" + t[0]
                        + ", weight=" + t[1]
                        + ", x=" + t[2]
                        + ", y=" + t[3]
                        + ", z=" + t[4];

                DefaultMutableTreeNode node = new DefaultMutableTreeNode(nodeString);
                root.add(node);
                buildSubTree(node, currDepth + 1);
            }
        }
    }
}