
import java.util.ArrayList;
import java.util.List;

/**
 * define the node for the alphabeta algorithm
 * @author hex team
 */
public class NodeAlpha extends NodeRoot {
    private int value;
    private int alpha = -999999;
    private int beta = 999999;
    private int minmax = 1;
    public List<int[]> valuelist = new ArrayList<int[]>();
    public NodeRoot noderoot = new NodeRoot();
    public List<NodeAlpha> childrenlistAlpha = new ArrayList<NodeAlpha>();


    /**
     *  set the value of this node
     * @param v
     */
    public void setvalue(int v) {

        value = v;
    }

    /**
     * get the value of this node
     * @return the value of this node
     */
    public int getvalue() {

        return value;
    }

    /**
     *  set the alpha of this node
     * @param al
     */
    public void setalpha(int al) {

        alpha = al;
    }

    /**
     * get the alpha of this node
     * @return the alpha of this node
     */
    public int getalpha() {

        return alpha;
    }

    /**
     * set the beta of this node
     * @param be
     */
    public void setbeta(int be) {

        beta = be;
    }

    /**
     * get the beta of this node
     * @return the beta of this node
     */
    public int getbeta() {

        return beta;
    }

    /**
     * get the minmax of this node
     * @return the minimax of this node
     */
    public int getminmax() {

        return minmax;
    }

    /**
     * set the minmax of this node
     * @param mm
     */
    public void setminmax(int mm) {

        minmax = mm;
    }

}
