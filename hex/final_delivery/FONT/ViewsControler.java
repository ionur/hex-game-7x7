/**
 * Main controler, creating all the views controlers and passing information 
 * from one to another 
 * @author Hex team
 */
public final class ViewsControler {

    private static ViewsControler instance;
    private MainWindow mainWindow;
    private GameViewControler gameViewControler;
    private PlayViewControler playViewControler;
    private ConfigViewControler configViewControler;
    private HelpViewControler helpViewControler;
    private RankingViewControler rankingViewControler;

    private ViewsControler() {
        mainWindow = new MainWindow(this);

        gameViewControler = new GameViewControler();
        playViewControler = new PlayViewControler();

        configViewControler = new ConfigViewControler();
        helpViewControler = new HelpViewControler();
        rankingViewControler = new RankingViewControler();

        mainWindow.addTab("Game", playViewControler.getView());

        mainWindow.makeVisible();
        startGame(false);
    }

    /**
     * Getter of singleton design pattern
     * @return the controler instance
     */
    public static ViewsControler getInstance() {
        if (instance == null) {
            instance = new ViewsControler();
        }
        return instance;
    }

    /**
     * Adds the tab for the configuration
     */
    public void addConfigTab() {
        mainWindow.addTab("Configuration", configViewControler.getView());
    }

    /** 
     * Adds the tab for the help
     */
    public void addHelpTab() {
        mainWindow.addTab("Help", helpViewControler.getView());
    }

    /**
     * Adds the tab for the rankings
     */
    public void addRankingTab() {
        mainWindow.addTab("Rankings", rankingViewControler.getView(TypeOfRanking.HCeasy));
    }

    /**
     * Load the game thanks to GameViewControler and PlayViewControler
     * Also show the good tab in the main window
     */
    public void loadGame() {
        gameViewControler.loadGame();
        playViewControler.startGameAfterDefiningSituation();
        mainWindow.updateConfigTab("Configuration", configViewControler.getViewUpdated());
        mainWindow.showTab(playViewControler.getView());
    }

    /**
     * Save the game thanks to GameViewControler
     */
    public void saveGame() {
        gameViewControler.saveGame();
    }

    /**
     * Start (or restart) a game thanks to GameViewControler and PlayViewControler
     * @param isRestart true whether this is a restart
     */
    public void startGame(boolean isRestart) {
        if (isRestart) {
            gameViewControler.restartGame();
        } else {
            gameViewControler.createGame();
        }
        playViewControler.startGameBeforeDefiningSituation(isRestart);
        mainWindow.showTab(playViewControler.getView());
    }

    /**
     * Handle a new score by calling the RankingViewControler
     * @param typeOfRanking the ranking in which we need to save
     * @param score the score to save
     * @param name the name to save
     */
    public void handleNewScore(TypeOfRanking typeOfRanking, int score, String name) {
        rankingViewControler.handleNewScore(typeOfRanking, score, name);
    }
}