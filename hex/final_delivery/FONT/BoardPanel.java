/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;

/**
 * Represents the board in the playView
 * @author Hex team
 */
public class BoardPanel extends JPanel implements MouseListener {

    private class Hexagon extends Polygon {

        private Color color;

        public Hexagon(int xref, int yref, int side, double r, double h,
                Color color) {
            super();
            this.color = color;

            npoints = 6;

            xpoints = new int[npoints];
            ypoints = new int[npoints];

            xpoints[0] = xref;
            ypoints[0] = yref;

            xpoints[1] = (int) (xref + r);
            ypoints[1] = (int) (yref + h);

            xpoints[2] = (int) (xref + r);
            ypoints[2] = (int) (yref + side + h);

            xpoints[3] = xref;
            ypoints[3] = (int) (yref + side + 2 * h);

            xpoints[4] = (int) (xref - r);
            ypoints[4] = (int) (yref + side + h);

            xpoints[5] = (int) (xref - r);
            ypoints[5] = (int) (yref + h);

        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }
    }
    private PlayViewControler playViewControler;
    private int boardSize;
    private Hexagon[][] board;
    private Hexagon[] leftBorder;
    private Hexagon[] rightBorder;
    private Hexagon[] topBorder;
    private Hexagon[] bottomBorder;
    private final Color emptyCellColor = Color.LIGHT_GRAY;
    private final Color whiteCellColor = Color.WHITE;
    private final Color blackCellColor = Color.BLACK;
    private Hexagon lastComputerMove;
    private Hexagon lastHint;

    /**
     * Constructor
     * @param controler the controler of this view
     * @param size the size of the board to draw
     */
    public BoardPanel(PlayViewControler controler, int size) {
        playViewControler = controler;

        boardSize = size;
        lastComputerMove = null;

        createBoard();

        addMouseListener(this);

        setPreferredSize(new Dimension(800, 800));
    }

    private void createBoard() {
        final int side = 30;
        final double r = Math.cos(30 * Math.PI / 180) * side;
        final double h = Math.sin(30 * Math.PI / 180) * side;
        final int x0 = 100;
        final int y0 = (int) ((boardSize - 1) * (side + 2 * h));

        board = new Hexagon[boardSize][boardSize];
        leftBorder = new Hexagon[boardSize];
        rightBorder = new Hexagon[boardSize];
        topBorder = new Hexagon[boardSize];
        bottomBorder = new Hexagon[boardSize];

        for (int i = 0; i < boardSize; ++i) {
            for (int j = 0; j < boardSize; ++j) {
                board[i][j] = new Hexagon(
                        x0 + (int) ((2 * i + j) * r),
                        y0 + (int) (-j * (h + side)),
                        side, r, h, emptyCellColor);
            }
        }

        for (int j = 0; j < boardSize; ++j) {
            leftBorder[j] = new Hexagon(
                    x0 + (int) ((j - 2) * r),
                    y0 + (int) (-j * (h + side)),
                    side, r, h, blackCellColor);
        }

        for (int j = 0; j < boardSize; ++j) {
            rightBorder[j] = new Hexagon(
                    x0 + (int) ((2 * boardSize + j) * r),
                    y0 + (int) (-j * (h + side)),
                    side, r, h, blackCellColor);
        }

        for (int i = 0; i < boardSize; ++i) {
            topBorder[i] = new Hexagon(
                    x0 + (int) ((2 * i + boardSize) * r),
                    y0 + (int) (-boardSize * (h + side)),
                    side, r, h, whiteCellColor);
        }

        for (int i = 0; i < boardSize; ++i) {
            bottomBorder[i] = new Hexagon(
                    x0 + (int) ((2 * i - 1) * r),
                    y0 + (int) (h + side),
                    side, r, h, whiteCellColor);
        }
    }

    /**
     * Handler for the clicks on the board
     * @param event the event click
     */
    @Override
    public void mouseClicked(MouseEvent event) {
        for (int i = 0; i < boardSize; ++i) {
            for (int j = 0; j < boardSize; ++j) {
                if ((board[i][j] != null)
                        && (board[i][j].contains(event.getPoint()))) {
                    int x = i + 1;
                    int y = j + 1;
                    playViewControler.handleBoardClic(x, y);

                }
            }
        }
    }

    /**
     * Doing nothing
     * @param me
     */
    @Override
    public void mousePressed(MouseEvent me) {
    }

    /**
     * Doing nothing
     * @param me
     */
    @Override
    public void mouseReleased(MouseEvent me) {
    }

    /**
     * Doing nothing
     * @param me
     */
    @Override
    public void mouseEntered(MouseEvent me) {
    }

    /**
     * Doing nothing
     * @param me
     */
    @Override
    public void mouseExited(MouseEvent me) {
    }

    private Color fromColorEnumToAwtColor(PlayerColor color) {
        switch (color) {
            case noColor:
                return emptyCellColor;
            case black:
                return blackCellColor;
            case white:
                return whiteCellColor;
        }
        return null;
    }

    /**
     * Update the color of the cells on the board
     * @param colors the colors to set
     */
    public void updateBoardColors(PlayerColor[][] colors) {
        if (colors.length != boardSize) {
            boardSize = colors.length;
            createBoard();
        }
        for (int i = 0; i < boardSize; ++i) {
            for (int j = 0; j < boardSize; ++j) {
                board[i][j].setColor(fromColorEnumToAwtColor(colors[i][j]));
            }
        }
        repaint();
    }

    /**
     * Color the given cell
     * @param x
     * @param y
     * @param color
     * @param isComputer is useful to keep track of the last computer move
     * because it is drawn in another color
     */
    public void colorCell(int x, int y, PlayerColor color, boolean isComputer) {
        Hexagon hex = board[x - 1][y - 1];
        hex.setColor(fromColorEnumToAwtColor(color));
        if (isComputer) {
            lastComputerMove = hex;
        }
        repaint();
    }

    /**
     * Put a frame arround the cell that has been proposed as a hint
     * @param hint the coordinates of the hint
     */
    public void markHint(int[] hint) {
        lastHint = board[hint[0] - 1][hint[1] - 1];
        repaint(lastHint.getBounds());
    }
    
    /**
     * Forgets the last computer move
     */
    public void resetLastComputerMove() {
        lastComputerMove = null;
    }
    
//    /**
//     * Forgets the last computer move
//     */
//    public void resetLastHint() {
//        lastHint = null;
//    }

    private void paintHexagonCollection(Graphics g, Hexagon[] hexs) {
        for (Hexagon hex : hexs) {
            g.setColor(hex.getColor());
            g.fillPolygon(hex);

            g.setColor(Color.DARK_GRAY);
            g.drawPolygon(hex);
        }
    }

    /**
     * Draws this panel
     * @param g the Graphics object used to draw
     */
    @Override
    public void paint(Graphics g) {
        for (Hexagon[] hexs : board) {
            paintHexagonCollection(g, hexs);
        }

        paintHexagonCollection(g, leftBorder);
        paintHexagonCollection(g, rightBorder);
        paintHexagonCollection(g, topBorder);
        paintHexagonCollection(g, bottomBorder);
        
        if (lastComputerMove != null) {
            g.setColor(Color.GREEN);
            g.drawPolygon(lastComputerMove);
        }
        
        if (lastHint != null) {
            g.setColor(Color.RED);
            g.drawPolygon(lastHint);
            lastHint = null;
        }
    }
}