
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Apply for the global value
 * @author hex team
 */
//class globalvalue {
//public static Map mMap = new HashMap();
//}
/**
 * Compute the best next move with the alphabeta algorithm
 * @author hex team
 */
public class AlphaBeta extends Strategy {

    //globalvalue global = new globalvalue();
    /**
     * compute the next best move
     * @param board Board of the game
     * @param playerColor Color of the player
     * @param level Level of accuracy of the algorithm
     * @return
     */
    @Override
    public Coordinates computeBestMove(Board board, PlayerColor playerColor,
            int width, int depth) {

        NodeAlpha treeRootAlpha = new NodeAlpha();
        treeRootAlpha.setColor(playerColor);
        treeRootAlpha.setboard(board);

        int[] result = minimax(depth, treeRootAlpha, width);
        treeRoot = treeRootAlpha.noderoot;
        return new Coordinates(result[1], result[2], result[3]);
    }

    private class CompareMAX implements Comparator {

        @Override
        public int compare(Object b, Object a) {
            int aa[] = (int[]) a;
            int bb[] = (int[]) b;
            for (int i = 0; i < aa.length && i < bb.length; i++) {
                if (aa[i] != bb[i]) {
                    return aa[i] - bb[i];
                }
            }
            return aa.length - bb.length;
        }
    }

    private class CompareMIN implements Comparator {

        @Override
        public int compare(Object a, Object b) {
            int aa[] = (int[]) a;
            int bb[] = (int[]) b;
            for (int i = 0; i < aa.length && i < bb.length; i++) {
                if (aa[i] != bb[i]) {
                    return aa[i] - bb[i];
                }
            }
            return aa.length - bb.length;
        }
    }

    /**
     * minimax algorithm with alphabeta pruning the tree
     * @param depth
     * @param player
     * @param width
     * @return
     */
    private int[] minimax(int depth, NodeAlpha player, int width) {

        List<int[]> nextMoves = player.generateMoves(width);
        int[] oppvalue;
        List<int[]> keeplist = new ArrayList<int[]>();
        if (depth == 1) {
            for (int[] move : nextMoves) {
                if (move[3] <= 0) {
                    //NodeRoot rootopp = new NodeRoot();
                    NodeAlpha playeropp = new NodeAlpha();

                    Coordinates coordinates = new Coordinates(move[0], move[1], move[2]);
                    playeropp.setCoordinate(coordinates);
                    player.childrenlistAlpha.add(playeropp);
                    playeropp.depth = player.depth + 1;
                    playeropp.setWeight(move[3]);
                    keeplist.add(move);
                    NodeRoot nr = new NodeRoot();
                    nr.depth = player.depth + 1;
                    nr.setCoordinate(coordinates);
                    nr.setWeight(move[3]);
                    playeropp.noderoot = nr;
                    if (player.getColor() == PlayerColor.black) {
                        playeropp.setColor(PlayerColor.white);
                    } else if (player.getColor() == PlayerColor.white) {
                        playeropp.setColor(PlayerColor.black);
                    }
                    player.noderoot.childrenlist.add(nr);
                } else {
                    player.setWeight(move[3]);
                    return new int[]{move[3], move[0], move[1], move[2]};
                }
            }
            if (player.getminmax() == 1) {
                int[] max = {0, 0, 0, -99999};
                for (int[] c : keeplist) {
                    if (c[3] > max[3]) {
                        max = c;
                    }
                }
                player.setWeight(max[3]);
                if (player.depth == 0) {
                    Coordinates coordinate = new Coordinates(max[0], max[1], max[2]);
                    player.setCoordinate(coordinate);
                    return new int[]{max[3], player.getx(), player.gety(), player.getz()};
                } else {
                    return new int[]{max[3], player.getx(), player.gety(), player.getz()};
                }

            } else {
                int[] min = {0, 0, 0, 99999};
                for (int[] c : keeplist) {
                    if (c[3] < min[3]) {
                        min = c;
                    }
                }
                player.setWeight(min[3]);
                if (player.depth == 0) {
                    Coordinates coordinate = new Coordinates(min[0], min[1], min[2]);
                    player.setCoordinate(coordinate);
                    return new int[]{min[3], player.getx(), player.gety(), player.getz()};
                } else {
                    return new int[]{min[3], player.getx(), player.gety(), player.getz()};
                }
            }
        } else {
            MOSTLOOP:
            for (int[] move : nextMoves) {
                //if(move[3] <= 0){
                NodeAlpha playeropp = new NodeAlpha();
                Coordinates coordinates = new Coordinates(move[0], move[1], move[2]);
                playeropp.setalpha(player.getalpha());
                playeropp.setbeta(player.getbeta());
                playeropp.setCoordinate(coordinates);
                player.childrenlistAlpha.add(playeropp);
                playeropp.depth = player.depth + 1;
                NodeRoot nr = new NodeRoot();
                nr.depth = player.depth + 1;
                nr.setCoordinate(coordinates);
                //nr.setWeight(move[3]);
                playeropp.noderoot = nr;
                player.noderoot.childrenlist.add(nr);
                for (Cell c : player.cells) {
                    playeropp.cells.add(c);
                }

                if (player.getminmax() == 1) {
                    playeropp.setminmax(0);
                    playeropp.setboard(player.board);
                    if (player.getColor() == PlayerColor.black) {
                        playeropp.setColor(PlayerColor.white);
                        Cell cell = new Cell(PlayerColor.white, coordinates);
                        playeropp.cells.add(cell);
                        playeropp.generateNextBoard(playeropp.cells, player.board.getSize());
                    } else if (player.getColor() == PlayerColor.white) {
                        playeropp.setColor(PlayerColor.black);
                        Cell cell = new Cell(PlayerColor.black, coordinates);
                        playeropp.cells.add(cell);
                        playeropp.generateNextBoard(playeropp.cells, player.board.getSize());
                    }

                    oppvalue = minimax(depth - 1, playeropp, width);
                    if (oppvalue[0] > player.getalpha()) {
                        player.setalpha(oppvalue[0]);
                    }
                    oppvalue[1] = playeropp.getx();
                    oppvalue[2] = playeropp.gety();
                    oppvalue[3] = playeropp.getz();
                    player.valuelist.add(oppvalue);

                } else if (player.getminmax() == 0) {
                    playeropp.setminmax(1);
                    playeropp.setboard(player.board);

                    if (player.getColor() == PlayerColor.black) {
                        playeropp.setColor(PlayerColor.white);
                        Cell cell = new Cell(PlayerColor.white, coordinates);
                        playeropp.cells.add(cell);
                        playeropp.generateNextBoard(playeropp.cells, player.board.getSize());
                    } else if (player.getColor() == PlayerColor.white) {
                        playeropp.setColor(PlayerColor.black);
                        Cell cell = new Cell(PlayerColor.black, coordinates);
                        playeropp.cells.add(cell);
                        playeropp.generateNextBoard(playeropp.cells, player.board.getSize());
                    }
                    oppvalue = minimax(depth - 1, playeropp, width);
                    if (oppvalue[0] < player.getbeta()) {
                        player.setbeta(oppvalue[0]);
                    }

                    oppvalue[1] = playeropp.getx();
                    oppvalue[2] = playeropp.gety();
                    oppvalue[3] = playeropp.getz();
                    player.valuelist.add(oppvalue);
                }
                if (player.getalpha() >= player.getbeta()) {
                    break MOSTLOOP;
                }

                //}
            }
        }
        if (player.getminmax() == 1) {
            Collections.sort(player.valuelist, new CompareMAX());
            int[] objvalue = player.valuelist.get(0);
            player.setvalue(objvalue[0]);
        } else if (player.getminmax() == 0) {
            Collections.sort(player.valuelist, new CompareMIN());
            int[] objvalue = player.valuelist.get(0);
            player.setvalue(objvalue[0]);
        }
        player.noderoot.setWeight(player.valuelist.get(0)[0]);
        player.setWeight(player.valuelist.get(0)[0]);

        return new int[]{player.valuelist.get(0)[0], player.valuelist.get(0)[1], player.valuelist.get(0)[2], player.valuelist.get(0)[3]};
    }
}