import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;

/**
 * The main frame of the application
 * @author Hex team
 */
public class MainWindow extends JFrame {

    private ViewsControler viewsControler;
    private JMenuBar menuBar;
    private JTabbedPane tabPane;
    private JMenuItem saveItem;

    /**
     * Constructor
     * @param viewsControler the views controler
     */
    public MainWindow(ViewsControler viewsControler) {
        super("Hex game");
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);

        this.viewsControler = viewsControler;

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        createMenuBar();

        tabPane = new JTabbedPane();
        add(tabPane);
    }

    /**
     * Put the window visible
     */
    public void makeVisible() {
        pack();
        setMinimumSize(new Dimension(600, 400));

        setVisible(true);
    }

    /**
     * Add a tab to the window
     * @param label the name of the tab
     * @param tab the tab to add
     */
    public void addTab(String label, AbstractTab tab) {
        if (tabPane.indexOfComponent(tab) == -1) {
            //tab was not already present
            tabPane.add(label, tab);
        }
        setActiveTab(tabPane.indexOfTab(label));
    }
    
    /**
     * Set the given tab to active mode: visible
     * @param tab the tab to show
     */
    public void showTab(AbstractTab tab) {
        int index = tabPane.indexOfComponent(tab);
        if (index != -1) {
            //tab was not already present
            setActiveTab(index);
        }
    }

    private void createMenuBar() {
        menuBar = new JMenuBar();
        addGameJMenu();
        addConfigMenu();
        addRankingMenu();
        addHelpMenu();
        setJMenuBar(menuBar);
    }

    private void addGameJMenu() {
        JMenu game = new JMenu("Game");

        addMenuEntry(game, "New Game", KeyEvent.VK_N, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                viewsControler.startGame(false);
            }
        });
        
        addMenuEntry(game, "Restart Game", KeyEvent.VK_R, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                viewsControler.startGame(true);
            }
        });

        addMenuEntry(game, "Load Game", KeyEvent.VK_L, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                viewsControler.loadGame();
            }
        });

        saveItem = addMenuEntry(game, "Save Game", KeyEvent.VK_S, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                viewsControler.saveGame();
            }
        });

        addMenuEntry(game, "Quit", KeyEvent.VK_Q, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });

        menuBar.add(game);
    }

    private void addConfigMenu() {
        JMenu menu = new JMenu("Configuration");
        addMenuEntry(menu, "Edit configuration", 0, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                viewsControler.addConfigTab();
            }
        });
        menuBar.add(menu);
    }

    private void addRankingMenu() {
        JMenu menu = new JMenu("Ranking");

        addMenuEntry(menu, "Show rankings", 0, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                viewsControler.addRankingTab();
            }
        });

        menuBar.add(menu);
    }

    private void addHelpMenu() {
        JMenu menu = new JMenu("Help");

        addMenuEntry(menu, "Show help", 0, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                viewsControler.addHelpTab();
            }
        });

        menuBar.add(menu);
    }

    private JMenuItem addMenuEntry(JMenu menu, String label,
            int mnemonic, ActionListener listener) {
        JMenuItem menuItem = new JMenuItem(label, mnemonic);
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        return menuItem;
    }

    private void setActiveTab(int i) {
        tabPane.setSelectedIndex(i);
    }

    private void setTabVisible(int i, boolean visible) {
        tabPane.setEnabledAt(i, visible);
    }

    public void updateConfigTab(String label, AbstractTab tab) {
        int indexConfig = tabPane.indexOfTab(label);
        if (indexConfig == -1) {
            tabPane.add(label, tab);
        } else {
            tabPane.remove(indexConfig);
            tabPane.add(label, tab);
        }
        setActiveTab(tabPane.indexOfTab(label));
    }
}