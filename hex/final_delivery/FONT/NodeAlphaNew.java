/**
 *
 * @author Michael and Anael
 */
public class NodeAlphaNew extends NodeRoot {
    
    public NodeAlphaNew(Board board, PlayerColor color) {
        this.board = board;
        this.color = color;
        depth = 0;
    }
    
    public NodeAlphaNew(NodeAlphaNew parent, Move mv) {
        board = new Board(parent.getBoard());
        color = parent.getColor() == PlayerColor.black ? 
                PlayerColor.white : PlayerColor.black;
        setCoordinate(mv.getCoords());
        setWeight(mv.getWeight());
        board.addToken(color,mv.getCoords());
        depth = parent.getDepth()+1;
        parent.childrenlist.add(this);
    }

    public Board getBoard() {
        return board;
    }

    public int getDepth() {
        return depth;
    }
}
