/**
 * 
 * @author Hex team
 */
public enum NextTurnAfter {
    
    
    /**
     * 
     */
    immediate,
    /**
     * 
     */
    clickFromUser,
    /**
     * 
     */
    twoSecondsWait,
    /**
     * 
     */
    fiveSecondsWait,
    /**
     * 
     */
    tenSecondsWait;
    
}