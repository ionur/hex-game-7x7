
import java.util.ArrayList;
import java.util.List;

/**
 * define the root node for the tree
 * @author hex team
 */
public class NodeRoot extends Node {

    public List<NodeRoot> childrenlist = new ArrayList<NodeRoot>();
    private List<NodeRoot> treeNode = new ArrayList<NodeRoot>();

    /**
     * generate the tree for minimax algorithm
     * @param root
      * @return a list of node
     */
    public List<NodeRoot> getTree() {
        for (NodeRoot n : childrenlist) {
            treeNode.add(n);
            if (!n.childrenlist.isEmpty()) {
                treeNode.addAll(n.getTree());
            }
        }
        return treeNode;
    }

}
