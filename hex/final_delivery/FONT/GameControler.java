import java.util.Stack;

/**
 * Controler supplying the different type of games by inititializing the objects needed
 * and supplying the save/load game methods
 * @author Hex team
 */
public class GameControler {
    private static ConfigControler savedConfigControler;
    private static ConfigControler configControler;
    private static PlayControler playControler;
    private SaveMDControler saveMDControler;
    private Player player1;
    private Player player2;
    private Board board;

    /**
     * Constructor for GameControler    
     */
    public GameControler() {
        configControler = new ConfigControler();
        saveMDControler = new SaveMDControler();
        applyConfig(configControler);
    }

    private void applyConfig(ConfigControler controler) {
        player1 = createPlayer1(
                controler.getPlayer1Color(),
                controler.getMode(),
                controler.getplayingTimeInMs());

        player2 = createPlayer2(
                (controler.getPlayer1Color() == PlayerColor.white) ? PlayerColor.black : PlayerColor.white,
                controler.getMode(),
                controler.getplayingTimeInMs());

        board = new Board(controler.getBoardSize());

        playControler = new PlayControler(player1, player2, board, controler);
    }

    private Player createPlayer1(PlayerColor color, Mode mode, int playingTime) {
        boolean isComputer = (mode == Mode.CC);

        return new Player(color,
                100, //score
                playingTime,
                isComputer,
                new AlphaBeta());
    }

    private Player createPlayer2(PlayerColor color, Mode mode, int playingTime) {
        boolean isComputer = ((mode == Mode.HC) || (mode == Mode.CC));

        return new Player(color,
                100,
                playingTime,
                isComputer,
                new AlphaBetaNew());
    }

    /**
     * Creates a new game by applying the current configuration
     */
    public void createGame() {
        savedConfigControler = new ConfigControler(configControler);
        applyConfig(configControler);
    }
    
    /**
     * Restarts the same game
     */
    public void restartSameGame() {
        if (savedConfigControler == null) {
            applyConfig(configControler);
        } else {
            applyConfig(savedConfigControler);
        }
    }

    /**
     * Save the game under the give name in the appFolder/saves folder
     * @param filePath the name of the file in which we will save
     * @return true if the game has been saved successfully
     */
    public boolean saveGame(String filePath) {

        String configStr = configControler.saveConfigAsString();
        String boardStr = saveBoardAsString();
        String p1Str = savePlayerAsString(player1);
        String p2Str = savePlayerAsString(player2);
        String currPlayerStr = playControler.getCurrentPlayerAsString();
        String oldMovesStr = saveOldMovesAsString(playControler.getOldMoves());
        String lineSep = System.getProperty("line.separator");

        String saveStr = configStr + lineSep + boardStr + lineSep + p1Str
                + lineSep + p2Str + lineSep + currPlayerStr + lineSep + oldMovesStr;

        return saveMDControler.writeData(saveStr, filePath);
    }

    /**
     * Does the inverse operation of saveGame(String name) by loading a game
     * @param filePath the name of the file from which we will load the game
     * @return true if the game has been loaded successfully
     */
    public boolean loadGame(String filePath) {
        String saveStr = saveMDControler.readData(filePath);
        if (saveStr == null) {
            return false;
        }

        String strs[] = saveStr.split(System.getProperty("line.separator"));

        if (strs.length != 6) {
            return false;
        }

        configControler.setConfigFromString(strs[0]);

        board = createBoardFromString(strs[1]);

        player1 = createPlayerFromString(strs[2]);
        player1.setStrategy(new AlphaBeta());

        player2 = createPlayerFromString(strs[3]);
        player2.setStrategy(new MonteCarlo());

        playControler = new PlayControler(player1, player2, board, configControler);
        boolean correct = playControler.setCurrentPlayerFromString(strs[4]);

        Stack<Coordinates> oldMoves = createOldMovesFromString(strs[5]);
        playControler.setOldMoves(oldMoves);
        
        savedConfigControler = new ConfigControler(configControler);

        return ((board != null) && (player1 != null)
                && (player2 != null) && (oldMoves != null) && correct);
    }

    /**
     * Getter for the configuration controler
     * @return the configuration controler
     */
    public static ConfigControler getConfigControler() {
        return configControler;
    }
    
    /**
     * Getter for the configuration controler
     * @return the configuration controler
     */
    public static ConfigControler getSavedConfigControler() {
        return savedConfigControler;
    }

    /**
     * Getter for the player controler
     * @return the play controler
     */
    public static PlayControler getPlayControler() {
        return playControler;
    }

    /**
     * Save the configurations as string
     * @return the resulting string
     */
    private String savePlayerAsString(Player p) {
        return p.getColor() + "," + p.getTimeRemaining() + ","
                + p.getScore() + "," + p.isComputer();
    }

    /**
     * Create a Player object from a string
     * @param string the string resulting from a previous savePlayerAsString
     * @return the Player object
     */
    private Player createPlayerFromString(String string) {
        try {
            String[] strs = string.split(",");
            if (strs.length != 4) {
                return null;
            }

            PlayerColor color = PlayerColor.valueOf(strs[0]);
            int time = Integer.parseInt(strs[1]);
            int score = Integer.parseInt(strs[2]);
            boolean isComputer = Boolean.parseBoolean(strs[3]);

            return new Player(color, score, time, isComputer, null);

        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Save the stack of moves as a string
     * @param oldMoves the stack
     * @return the created string
     */
    private String saveOldMovesAsString(Stack<Coordinates> oldMoves) {
        String res = ";";
        int size = oldMoves.size();

        for (int i = 0; i < size-1; ++i) {
            Coordinates c = oldMoves.get(size - i - 1);
            res += c.getX() + "," + c.getY() + ";";
        }
        Coordinates c = oldMoves.get(0);
        res += c.getX() + "," + c.getY();

        return res;
    }

    /**
     * Does the opposite operation of saveOldMovesAsString
     * @param string
     * @return 
     */
    private Stack<Coordinates> createOldMovesFromString(String string) {
        Stack<Coordinates> oldMoves = new Stack<Coordinates>();
        try {
            String[] coords = string.split(";");

            for (String coord : coords) {
                if (!("".equals(coord))) {
                    String[] s = coord.split(",");
                    oldMoves.push(new Coordinates(Integer.parseInt(s[0]),
                            Integer.parseInt(s[1])));
                }
            }

            return oldMoves;

        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Builds a string containing the color and the coordinates of the cell,
     * separated by ","
     * @return the created string
     */
    private String saveCellAsString(Cell c) {
        return c.getColor() + "," + c.getCoordinates().getX() + ","
                + c.getCoordinates().getY() + "," + c.getCoordinates().getZ();
    }

    /**
     * Does the exact opposite operation than saveCellAsString: builds the cell object
     * according to the given string which is the result of a previous toString
     * @param string the string from which the method builds the cell
     * @return the built cell
     */
    private Cell createCellFromString(String string) {
        try {
            String[] strs = string.split(",");
            if (strs.length != 4) {
                return null;
            }

            PlayerColor color = PlayerColor.valueOf(strs[0]);
            int x = Integer.parseInt(strs[1]);
            int y = Integer.parseInt(strs[2]);
            int z = Integer.parseInt(strs[3]);

            return new Cell(color, new Coordinates(x, y, z));

        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Create a Board from a given sting
     * @param string String of the board
     * @return The board
     */
    private Board createBoardFromString(String string) {
        try {
            String[] lines = string.split(";");
            Board res = new Board(lines.length);

            for (int i = 0; i < lines.length; ++i) {
                String[] rows = lines[i].split(" ");

                if (rows.length != lines.length) {
                    return null;
                }

                for (int j = 0; j < rows.length; ++j) {
                    Cell cell = createCellFromString(rows[j]);
                    res.addToken(cell.getColor(), cell.getCoordinates());
                }
            }

            return res;

        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Trasform the board in a string
     * @return The stirng of the board
     */
    private String saveBoardAsString() {
        String str = "";
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                str = str + saveCellAsString(board.getCell(i+1, j+1)) + " ";
            }
            str += ";";
        }
        return str;
    }
}