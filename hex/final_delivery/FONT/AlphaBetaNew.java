
import java.util.ArrayList;

/**
 *
 * @author Michael and Anael
 */
public class AlphaBetaNew extends Strategy {

    private int maxDepth;
    private int maxWidth;

    @Override
    public Coordinates computeBestMove(Board board, PlayerColor playerColor,
            int width, int depth) {
        maxDepth = depth;
        maxWidth = width;

        NodeAlphaNew root = new NodeAlphaNew(board, playerColor);

        Coordinates res = new Coordinates(1, 1);
        alphaBeta(root, res, -999999, 999999);
        treeRoot = root;

        return res;
    }

    private int alphaBeta(NodeAlphaNew node, Coordinates res, int a, int b) {
        if (node.getDepth() == maxDepth) {
            // we are at the maximum depth, we return the assesment of the position           
            int value = EvaluatingFunction.applyEvaluatingFunction(
                    node.getBoard()).get(0).getWeight();
            return value;
        } else {
            ArrayList<Move> moves = EvaluatingFunction.applyEvaluatingFunction(node.getBoard());
            int currScore;
            int bestScore = -999999;
            int alpha = a, beta = b;

            for (int i = 0; (i < moves.size() && i < maxWidth); ++i) {
                if (node.getDepth() == 0 && moves.get(i).getWeight() >= 0) {
                    //special cases
                    changeCoords(res, moves.get(i));
                    return 0;
                }
                currScore = -alphaBeta(new NodeAlphaNew(node, moves.get(i)), res, -beta, -alpha);
                if (currScore > bestScore) {
                    bestScore = currScore;
                    if (node.getDepth() == 0) {
                        changeCoords(res, moves.get(i));
                    }
                    //We update the value of alpha (or beta)					
                    if (bestScore > alpha) {
                        alpha = bestScore;
                        //if necessary, we prune
                        if (alpha >= beta) {
                            return bestScore;
                        }
                    }
                }
            }
            return bestScore;
        }
    }

    private void changeCoords(Coordinates res, Move move) {
        res.setX(move.getCoords().getX());
        res.setY(move.getCoords().getY());
        res.setZ(move.getCoords().getZ());
    }
}
