import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * 
 * @author Hex team
 */
public abstract class AbstractMDControler {

    /**
     * The directory of the application
     */
    protected String appDir;
    /**
     * The file separator of the OS
     */
    protected String fileSeparator;

    /**
     * Constructor for AbstractMDControler
     */
    protected AbstractMDControler() {
        appDir = System.getProperty("user.dir");
        fileSeparator = System.getProperty("file.separator");
    }

    /**
     * Checks if a file exists
     * @param path the path of the file
     * @return true if the file exists
     */
    protected boolean isExistingFile(String path) {
        File f = new File(path);
        return f.exists();
    }

    /**
     * Create a directory if it doesn't already exist
     * @param path the path for the directory
     * @return the File object reprensenting this directory
     */
    protected File createDirectoryIfDoesntExist(String path) {
        File dir = new File(path);

        if (!dir.exists()) {
            dir.mkdir();
        }

        return dir;
    }

    /**
     * Creates a file if it doesn't already exist
     * @param path the path of the file
     * @return the File object representing this file
     */
    protected File createFileIfDoesntExist(String path) {
        File f = new File(path);

        try {
            f.createNewFile();
        } catch (Exception ex) {
            return null;
        }

        return f;
    }

    /**
     * Reads the content from a file
     * @param file the file to read
     * @return the content of the file
     */
    protected String readStringFromFile(File file) {
        BufferedReader reader = null;
        String content = "";
        
        try {
            reader = new BufferedReader(new FileReader(file));

            String line = null;
            String ls = System.getProperty("line.separator");

            while ((line = reader.readLine()) != null) {
                content += line+ls;
            }

        } catch (Exception ex) {
            return null;
        }
        
        return content;
    }

    /**
     * Write content in a file
     * @param content the content to write
     * @param file the file in which write
     * @return true if the file has been successfully written
     */
    protected boolean writeStringInFile(String content, File file) {
        try {
            FileWriter fw = new FileWriter(file);

            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();

        } catch (Exception ex) {
            return false;
        }

        return true;
    }

}
