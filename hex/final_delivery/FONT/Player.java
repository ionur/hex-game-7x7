/**
 * 
 * @author Hex team
 */
public class Player {

    private PlayerColor color;
    private Timer timer;
    private int score;
    private boolean isComputer;
    private Strategy strategy;

    /**
     * Constructor for the Player class
     * @param color
     * @param score
     * @param timeRemaining
     * @param isComputer
     * @param strategy
     */
    public Player(PlayerColor color,
            int score,
            int timeRemaining,
            boolean isComputer,
            Strategy strategy) {
        this.color = color;
        this.timer = new Timer(timeRemaining);
        this.score = score;
        this.isComputer = isComputer;
        this.strategy = strategy;
    }

    /**
     * Returns the color of the player
     * @return the color of the player
     */
    public PlayerColor getColor() {
        return color;
    }

    /**
     * Set the color of the token
     * @param color the color to set
     */
    public void setColor(PlayerColor color) {
        this.color = color;
    }

    /**
     * Gets the score
     * @return the score of the player
     */
    public int getScore() {
        return score;
    }

    /**
     * Set the score
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Gets the time remaining
     * @return the remaining time
     */
    public int getTimeRemaining() {
        return timer.getTimeMilliSeconds();
    }

    /**
     * Starts the timer for thinking
     */
    public void startThinking() {
        timer.start();
    }

    /**
     * Stops the timer for thinking
     */
    public void stopThinking() {
        timer.stop();
    }

    /**
     * Asks if the user is thinking
     * @return true if he is thinking
     */
    public boolean isThinking() {
        return timer.isRunning();
    }

    /**
     * Asks whether the player is a computer 
     * @return true if the player is a computer
     */
    public boolean isComputer() {
        return isComputer;
    }

    /**
     * Set the boolean value if the movement is for the computer
     * @param isComputer the value to set
     */
    public void setIsComputer(boolean isComputer) {
        this.isComputer = isComputer;
    }

    /**
     * gets the strategy
     * @return the strategy
     */
    public Strategy getStrategy() {
        return strategy;
    }

    /**
     * sets the strategy
     * @param strategy the strategy to set
     */
    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    /**
     * applies the strategy
     * @param board
     * @param width 
     * @param depth 
     * @return
     */
    public Coordinates applyStrategy(Board board, int width, int depth) {
        return strategy.computeBestMove(board, color, width, depth);
    }

    /**
     * Returns if the time is over
     * @return true if time is down to 0
     */
    public boolean isTimeOver() {
        return timer.getTimeMilliSeconds() <= 0;
    }
}
