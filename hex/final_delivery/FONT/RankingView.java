import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author Hex team
 */
public class RankingView extends AbstractTab {

    private RankingViewControler rankingViewControler;
    private JTable rankingTable;
    private JComboBox rankingList;
    private int listIndex;

    /**
     * Constructor for the ranking view
     * @param rankingViewControler the ranking view controler 
     */
    public RankingView(final RankingViewControler rankingViewControler) {
        this.rankingViewControler = rankingViewControler;

        String[] rankings = {"HCeasy", "HCmedium", "HCdifficult",
            "HCexpert", "HH"};
        
        rankingList = new JComboBox(rankings);
        listIndex = 0;
        rankingList.setSelectedIndex(listIndex);
        rankingList.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                TypeOfRanking rankingType = TypeOfRanking.valueOf(
                        (String) rankingList.getSelectedItem());
                if (rankingViewControler.getView(rankingType) == null) {
                    rankingList.setSelectedIndex(listIndex);
                } else {
                    listIndex = rankingList.getSelectedIndex();
                }

            }
        });

        add(rankingList,BorderLayout.NORTH);
    }

    /**
     * Fill for thr object of ranking view
     * @param export the export object 
     */
    public void fill(Object[][] export) {
        String[] columnNames = {
            "Rank", "Name", "Score", "Date"
        };

        if (rankingTable != null) {
            DefaultTableModel tableModel = new DefaultTableModel(export, columnNames);
            rankingTable.setModel(tableModel);
        } else {
            rankingTable = new JTable(export, columnNames);
            JScrollPane pane = new JScrollPane(rankingTable);
            add(pane,BorderLayout.CENTER);
        }
        validate();
    }
}
