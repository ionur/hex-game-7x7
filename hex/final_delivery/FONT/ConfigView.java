
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

//import apple.laf.JRSUIConstants.Size;
/**
 * 
 * @author Hex team
 */
public class ConfigView extends AbstractTab {

    private ConfigViewControler configViewControler;

    /**
     * Constructor of the ConfigView class
     * @param configViewControler 
     * @param modeOfGame to set the mode of the game. Human-Human, Human-Computer, Computer-Computer
     * @param undoEnabled undo in the game can be enabled if its true
     * @param level the level of the game. easy-medium-difficult-expert
     * @param playingTimeInMs the playing time of the game. in miliseconds
     * @param nexTurnAfter the next turn can be set in seconds (immediate-clickFromUse-twoSecondsWait-fiveSecondsWait-tenSecondsWait)
     * @param boardSize the size of the board between 4-9
     * @param player1Name player 1 name
     * @param player2Name player 2 name
     * @param player1Color token color of the player 1
     */
    public ConfigView(ConfigViewControler configViewControler, Mode modeOfGame,
            boolean undoEnabled, int depth, int width, int playingTimeInMs,
            NextTurnAfter nexTurnAfter, int boardSize, String player1Name,
            String player2Name, PlayerColor player1Color) {

        this.configViewControler = configViewControler;
        setLayout(new GridLayout(4, 2));

        createModePanel(modeOfGame);
        createUndoEnabledPanel(undoEnabled);
        createDepthWidthPanel(depth, width);
        createPlayingTimeInMinutesPanel(playingTimeInMs);
        createBoardSize(boardSize);
        createNextTurnAfter(nexTurnAfter);
        createPlayerColor(player1Color);
        createUsernameTextField(player1Name, player2Name);

    }

    /**
     * This function creates a radiobutton
     * @param label the label of the radio button
     * @param isSelected true if selected
     * @param group buttongroup
     * @param panel Jpanel component
     * @param listener Actionlistener
     * @return a radiobutton
     */
    private JRadioButton addRadioButton(String label, boolean isSelected,
            ButtonGroup group, JPanel panel, ActionListener listener) {

        JRadioButton button = new JRadioButton(label);
        button.setSelected(isSelected);
        group.add(button);
        button.addActionListener(listener);
        button.addActionListener(listener);
        panel.add(button, BorderLayout.CENTER);
        return button;
    }

    /**
     * This function creates a text field to enter the user names
     * @param label label of the text field
     * @param panel Jpanel component
     * @param listener Action listener
     * @param mouseListener MouseListener
     * @return a text field
     */
    private JTextField addTextField(String label, JPanel panel,
            ActionListener listener, MouseListener mouseListener) {

        JTextField textField = new JTextField(label, 25);
        textField.addActionListener(listener);
        textField.addMouseListener(mouseListener);
        label = textField.getText();
        panel.add(textField, BorderLayout.CENTER);
        return textField;
    }

    /**
     * This function creates a checkbox button
     * @param label the label of the checkbox area
     * @param isSelected true if selected
     * @param panel Jpanel component
     * @param listener ActionListener
     * @return a checkbox button
     */
    private JCheckBox addCheckBoxButton(String label, boolean isSelected,
            JPanel panel, ActionListener listener) {

        JCheckBox checkbox = new JCheckBox(label);
        checkbox.setSelected(isSelected);
        checkbox.addActionListener(listener);
        panel.add(checkbox, BorderLayout.CENTER);
        return checkbox;
    }

    private JSpinner addSpinner(String label, JPanel panel, int defaultVal,
            int min, int max, ChangeListener listener) {
        GridLayout layout = new GridLayout(1, 2);
        JPanel subPanel = new JPanel();
        subPanel.setLayout(layout);

        SpinnerModel sm = new SpinnerNumberModel(defaultVal, min, max, 1);

        JSpinner spinner = new JSpinner(sm);
        spinner.setValue(defaultVal);
        spinner.addChangeListener(listener);
        spinner.setEnabled(true);
        subPanel.add(new JLabel(label));
        subPanel.add(spinner);
        panel.add(subPanel);
        return spinner;
    }

    /**
     * This function creates the panel and the components to change the mode of the game
     * @param mode gets the default mode
     */
    private void createModePanel(Mode mode) {
        JPanel modePanel = new JPanel();
        modePanel.setLayout(new GridLayout(7, 1));

        ButtonGroup modeGroup = new ButtonGroup();
        addRadioButton("Human - Human", mode == Mode.HH, modeGroup, modePanel,
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        configViewControler.setMode(Mode.HH);
                    }
                });
        addRadioButton("Human - Computer", mode == Mode.HC, modeGroup,
                modePanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setMode(Mode.HC);
            }
        });
        addRadioButton("Computer - Computer", mode == Mode.CC, modeGroup,
                modePanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setMode(Mode.CC);
            }
        });

        modePanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Mode of the game"));
        add(modePanel);

    }

    /**
     * This function creates the panel and the components to change the undoEnable
     * @param undoEnabled boolean, default value
     */
    private void createUndoEnabledPanel(final boolean undoEnabled) {
        JPanel undoPanel = new JPanel();
        undoPanel.setLayout(new GridLayout(4, 1));

        addCheckBoxButton("Enable undo", undoEnabled == true, undoPanel,
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ae) {

                        configViewControler.setUndoEnabled(((JCheckBox) ae.getSource()).isSelected());
                    }
                });

        undoPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Undo"));
        add(undoPanel);

    }

    /**
     * This function creates the panel and the components to change the level of the game
     * @param level the level of the game, default value
     */
    private void createDepthWidthPanel(int depth, int width) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 1));

        addSpinner("Depth", panel, depth, 1, 30, new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent ce) {
                configViewControler.setDepth(
                        (Integer) ((JSpinner) ce.getSource()).getValue());
            }
        });

        addSpinner("Width", panel, width, 1, 30, new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent ce) {
                configViewControler.setWidth(
                        (Integer) ((JSpinner) ce.getSource()).getValue());
            }
        });

        panel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(),
                "Parameters for the algorithms"));
        add(panel);

    }

    /**
     * This function creates the panel and the components to change the playing time
     * @param playingTimeInMs integer, default value
     */
    private void createPlayingTimeInMinutesPanel(int playingTimeInMs) {

        JPanel timePanel = new JPanel();
        timePanel.setLayout(new GridLayout(4, 1));

        addSpinner("Playing time in seconds", timePanel,
                playingTimeInMs / 1000, 10, 1000, new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                Object val = ((JSpinner) e.getSource()).getValue();
                int timeInMs = (Integer) val * 1000;
                configViewControler.setPlayingTimeInMs(timeInMs);
            }
        });

        timePanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Playing time in seconds"));
        add(timePanel);

    }

    /**
     * This function creates the panel and the components to change the size of the board
     * @param boardSize integer, default value
     */
    private void createBoardSize(final int boardSize) {
        JPanel boardSizePanel = new JPanel();
        boardSizePanel.setLayout(new GridLayout(4, 1));

        addSpinner("Size of the board", boardSizePanel, boardSize, 4, 9,
                new ChangeListener() {

                    @Override
                    public void stateChanged(ChangeEvent e) {
                        Object val = ((JSpinner) e.getSource()).getValue();
                        int size = (Integer) val;
                        configViewControler.setBoardSize(size);
                    }
                });

        boardSizePanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Board size"));
        add(boardSizePanel);

    }

    /**
     * This function creates the panel and the components to change the next turn after option
     * @param nexTurnAfter default value
     */
    private void createNextTurnAfter(final NextTurnAfter nexTurnAfter) {
        JPanel nextTurnPanel = new JPanel();
        nextTurnPanel.setLayout(new GridLayout(7, 1));

        ButtonGroup modeGroup = new ButtonGroup();
        addRadioButton("Immediate", nexTurnAfter == NextTurnAfter.immediate,
                modeGroup, nextTurnPanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setNextTurnAfter(NextTurnAfter.immediate);
            }
        });
        addRadioButton("After a click",
                nexTurnAfter == NextTurnAfter.clickFromUser, modeGroup,
                nextTurnPanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setNextTurnAfter(NextTurnAfter.clickFromUser);
            }
        });
        addRadioButton("After 2s",
                nexTurnAfter == NextTurnAfter.twoSecondsWait, modeGroup,
                nextTurnPanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setNextTurnAfter(NextTurnAfter.twoSecondsWait);
            }
        });
        addRadioButton("After 5s",
                nexTurnAfter == NextTurnAfter.fiveSecondsWait, modeGroup,
                nextTurnPanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setNextTurnAfter(NextTurnAfter.fiveSecondsWait);
            }
        });
        addRadioButton("After 10s",
                nexTurnAfter == NextTurnAfter.tenSecondsWait, modeGroup,
                nextTurnPanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setNextTurnAfter(NextTurnAfter.tenSecondsWait);
            }
        });

        nextTurnPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(),
                "Transition between turns during the game"));
        add(nextTurnPanel);

    }

    /**
     * This function creates the panel and the components to change the player1 token color
     * @param player1Color Color, default value
     */
    private void createPlayerColor(PlayerColor player1Color) {
        JPanel colorPanel = new JPanel();
        colorPanel.setLayout(new GridLayout(7, 1));

        ButtonGroup modeGroup = new ButtonGroup();
        addRadioButton("Black", player1Color == PlayerColor.black, modeGroup,
                colorPanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setPlayer1Color(PlayerColor.black);
            }
        });
        addRadioButton("White", player1Color == PlayerColor.white, modeGroup,
                colorPanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setPlayer1Color(PlayerColor.white);
            }
        });
        addRadioButton("Random", player1Color == PlayerColor.white, modeGroup,
                colorPanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                configViewControler.setPlayer1ColorRandomly();
            }
        });

        colorPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(),
                "Color of the player1 (white starts)"));
        add(colorPanel);

    }

    /**
     * This function creates the panel and the components to change the names of the players
     * @param player1Name string, default value
     * @param player2Name string, default value
     */
    private void createUsernameTextField(final String player1Name,
            final String player2Name) {
        JPanel namePanel = new JPanel();
        namePanel.setLayout(new GridLayout(4, 1));

        addTextField(player1Name, namePanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                Object val = ((JTextField) ae.getSource()).getText();
                configViewControler.setPlayer1Name(val.toString());
            }
        }, new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }
        });

        addTextField(player2Name, namePanel, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                Object val = ((JTextField) ae.getSource()).getText();
                configViewControler.setPlayer2Name(val.toString());
            }
        }, new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }
        });

        namePanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(),
                "Player Names (press enter to set)"));
        add(namePanel);

    }
}