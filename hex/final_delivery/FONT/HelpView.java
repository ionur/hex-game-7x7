import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * 
 * @author Hex team
 */
public class HelpView extends AbstractTab {

    private HelpViewControler helpViewControler;
    private JTextArea txt;
    private JScrollPane scrolltxt;

    /**
     * 
     * @param helpViewControler
     */
    public HelpView(HelpViewControler helpViewControler) {
        this.helpViewControler = helpViewControler;

        StyledDocument document = new DefaultStyledDocument();
        SimpleAttributeSet attributes = new SimpleAttributeSet();

        try {
            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.FontFamily, "Edwardian Script ITC");
            attributes.addAttribute(StyleConstants.FontSize, new Integer(50));
            attributes.addAttribute(StyleConstants.Foreground, Color.BLUE);
            document.insertString(document.getLength(), "\t\t\t\t\tHelp for the game Hex\n", attributes);

            attributes.addAttribute(StyleConstants.FontFamily, "Times New Roman");
            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            attributes.addAttribute(StyleConstants.FontSize, new Integer(20));
            attributes.addAttribute(StyleConstants.Foreground, Color.black);

            document.insertString(document.getLength(), "\tThe target of the game Hex is to create a path from an edge to another using your tokens. "
                    + "If you are having troubles you can always ask for a hint, but remember "
                    + "this will decrease your score at the end of the game. "
                    + "Every player has an amount of time to play, faster you play, higher will be your score.\n\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.BLUE);
            attributes.addAttribute(StyleConstants.FontSize, new Integer(34));
            attributes.addAttribute(StyleConstants.SpaceBelow, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.FontFamily, "Edwardian Script ITC");
            document.insertString(document.getLength(), "\t1. Start a New Game\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            attributes.addAttribute(StyleConstants.Foreground, Color.black);
            attributes.addAttribute(StyleConstants.FontSize, new Integer(20));
            attributes.addAttribute(StyleConstants.FontFamily, "Times New Roman");

            document.insertString(document.getLength(), "\tA game can start in two ways:\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.BLUE);
            document.insertString(document.getLength(), "#", attributes);


            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.black);
            document.insertString(document.getLength(), "  The first way:", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            document.insertString(document.getLength(), " you can define a situation by putting your tokens on board before start a new game.\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.BLUE);
            document.insertString(document.getLength(), "#", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.black);
            document.insertString(document.getLength(), "  The second way:", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            document.insertString(document.getLength(), " you can start a New Game without defining a situation: just click to start a new game.\n\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.BLUE);
            attributes.addAttribute(StyleConstants.FontSize, new Integer(34));
            attributes.addAttribute(StyleConstants.SpaceBelow, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.FontFamily, "Edwardian Script ITC");
            document.insertString(document.getLength(), "\t2. Restart Game\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            attributes.addAttribute(StyleConstants.Foreground, Color.black);
            attributes.addAttribute(StyleConstants.FontSize, new Integer(20));
            attributes.addAttribute(StyleConstants.FontFamily, "Times New Roman");
            document.insertString(document.getLength(), "\tWhile you are playing a game you can edit configuration and "
                    + "if you restart a game it means that you start exatcly the same game again, "
                    + "in the same conditions. So your changes in configuration are not taken in account.\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.BLUE);
            attributes.addAttribute(StyleConstants.FontSize, new Integer(34));
            attributes.addAttribute(StyleConstants.FontFamily, "Edwardian Script ITC");
            document.insertString(document.getLength(), "\t3. End a Game\n", attributes);


            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            attributes.addAttribute(StyleConstants.Foreground, Color.black);
            attributes.addAttribute(StyleConstants.FontSize, new Integer(20));
            attributes.addAttribute(StyleConstants.FontFamily, "Times New Roman");
            document.insertString(document.getLength(), "\tA game can end in three ways:\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.BLUE);
            document.insertString(document.getLength(), "#", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.black);
            document.insertString(document.getLength(), "  Forced Termination: ", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            document.insertString(document.getLength(), " the player presses the 'Stop Game' in this case no one wins\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.BLUE);
            document.insertString(document.getLength(), "#", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.black);
            document.insertString(document.getLength(), "  The time out: ", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            document.insertString(document.getLength(), " if your time is over the game automatically ends.\n", attributes);


            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.BLUE);
            document.insertString(document.getLength(), "#", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.TRUE);
            attributes.addAttribute(StyleConstants.Foreground, Color.black);
            document.insertString(document.getLength(), "  Natural end:", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            document.insertString(document.getLength(), " the game ends because one player has won the game, either the computer or the player.\n\n", attributes);

            attributes.addAttribute(StyleConstants.Bold, Boolean.FALSE);
            document.insertString(document.getLength(), "\tMoreover, by clicking in the menu you can edit the configuration for the game, "
                    + "and you can check the best scores whenever you want in each level by show ranking.", attributes);

        } catch (BadLocationException badLocationException) {
        }

        JTextPane textPane = new JTextPane(document);
        textPane.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textPane);
        add(scrollPane, BorderLayout.CENTER);
    }
}