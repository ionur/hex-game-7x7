/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Hex team
 */
public class InformationsPanel extends JPanel {

    private PlayViewControler playViewControler;
    private GridBagConstraints lim;
    private GridBagLayout layout;
    private JButton hintButton;
    private JButton startButton;
    private JButton nextTurnButton;
    private JButton undoButton;
    private JButton pauseButton;
    private JButton continueButton;
    private JButton stopButton;
    private PlayerInformation player1;
    private PlayerInformation player2;
    private JLabel status;

    /**
     * 
     * @param controler
     */
    public InformationsPanel(PlayViewControler controler,
            String namep1, int scorep1, int timep1,
            String namep2, int scorep2, int timep2) {
        playViewControler = controler;

        layout = new GridBagLayout();
        lim = new GridBagConstraints();
        this.setLayout(layout);
        //JPanel board = new JPanel();


        JPanel rightPanel = new JPanel();
        lim.weightx = 0.2;
        lim.weighty = 0.2;
        lim.fill = GridBagConstraints.BOTH;
        layout.setConstraints(rightPanel, lim);
        this.add(rightPanel);



        rightPanel.setLayout(layout);

        JPanel labelPanel = new JPanel();
        lim.weighty = 0.05;
        lim.gridy = 0;
        layout.setConstraints(labelPanel, lim);
        rightPanel.add(labelPanel);

        status = addStatusLabel("Defining Situation",true);

        labelPanel.add(status);


        JPanel players = new JPanel();
        lim.weightx = 1;
        lim.weighty = 1;
        lim.gridy = 1;

        layout.setConstraints(players, lim);
        rightPanel.add(players);
        players.setLayout(layout);
        player1 = new PlayerInformation(namep1,scorep1,timep1);
        lim.gridy = 0;
        layout.setConstraints(player1, lim);
        players.add(player1);

        player2 = new PlayerInformation(namep2, scorep2, timep2);

        lim.gridy = 1;
        layout.setConstraints(player2, lim);
        players.add(player2);

        JPanel buttons = generateButtonArea();
        lim.gridy = 2;
        lim.weighty = 1.3;
        lim.fill = GridBagConstraints.BOTH;
        layout.setConstraints(buttons, lim);
        rightPanel.add(buttons);


    }
    
    private JLabel addStatusLabel(String label, boolean visible) {
        JLabel res = new JLabel(label);
        Font statusFont = res.getFont();
        res.setFont(statusFont.deriveFont(
                statusFont.getStyle() | Font.BOLD));
        res.setForeground(Color.BLUE);
        res.setVisible(visible);
        return res;
    }

    private JPanel generateButtonArea() {
        JPanel buttonArea = new JPanel();
        buttonArea.setLayout(layout);
        startButton = addButton(buttonArea, "Start Game", 0.8, 0.8, 0, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                playViewControler.startGameAfterDefiningSituation();
            }
        });

        stopButton = addButton(buttonArea, "Stop Game", 0.8, 0.8, 1, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                playViewControler.stopToPlay();
            }
        });


        nextTurnButton = addButton(buttonArea, "Next Turn", 0.8, 0.8, 2, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                playViewControler.handleNextTurnClick();
            }
        });

        undoButton = addButton(buttonArea, "Undo", 0.8, 0.8, 4, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                playViewControler.handleUndoClick();
            }
        });

        hintButton = addButton(buttonArea, "Ask for Hint", 0.8, 0.8, 5, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                playViewControler.handleHintClick();
            }
        });

        pauseButton = addButton(buttonArea, "Pause", 0.8, 0.8, 6, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                playViewControler.pauseGame();
            }
        });

        continueButton = addButton(buttonArea, "Continue", 0.8, 0.8, 7, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                playViewControler.continueGame();
            }
        });

        return buttonArea;
    }

    /**
     * 
     * @param timeP1
     * @param timeP2
     */
    public void updateTimes(int timeP1, int timeP2) {
        player1.setTime(timeP1);
        player2.setTime(timeP2);

    }

    /**
     * 
     * @param score1
     * @param score2
     */
    public void updateScores(int score1, int score2) {
        player1.setScore(score1);
        player2.setScore(score2);

    }

    private JButton addButton(JPanel container, String label, double weightx,
            double weighty, int gridy, ActionListener al) {
        JButton button = new JButton(label);
        lim.weightx = weightx;
        lim.weighty = weighty;
        lim.gridy = gridy;
        lim.fill = GridBagConstraints.BOTH;
        layout.setConstraints(button, lim);
        container.add(button);
        button.addActionListener(al);
        return button;
    }

    private String renderTime(int milliseconds) {
        int seconds = milliseconds / 1000;
        return seconds / 60 + ":" + String.format("%02d", seconds % 60);

    }

    /**
     * 
     * @param player1Name
     * @param player2Name
     */
    public void updateNames(String player1Name, String player2Name) {
        player1.setPlayerName(player1Name);
        player2.setPlayerName(player2Name);
    }

    /**
     * 
     * @param color
     */
    public void setPlayerWonLabel(PlayerColor color) {
        status.setText(color + " player won!");
    }
    
    /**
     * 
     * @param color 
     */
    public void setPlayerLostLabel(PlayerColor color) {
        status.setText(color + " player loose!");
    }

    /**
     * 
     * @param playerNumber
     */
    public void putFrameCurrentPlayer(PlayerNumber playerNumber) {
        if (playerNumber == PlayerNumber.player1) {
            player1.addBlueBorder();
        } else {
            player2.addBlueBorder();
        }
    }

    /**
     * 
     */
    public void removeFrames() {
        player1.removeBlueBorder();
        player2.removeBlueBorder();
    }

    /**
     * 
     * @param isUndoEnabled
     * @param isWaitingForNextTurnClic
     * @param isGameStarted
     * @param isGamePaused
     * @param isSituationBeingDefined
     * @param isHumanTurn
     * @param hasGameBeenStopped
     * @param isBetweenTwoTurns
     */
    public void updateButtonsAndStatus(
            boolean isUndoEnabled,
            boolean isWaitingForNextTurnClic,
            boolean isGameStarted,
            boolean isGamePaused,
            boolean isSituationBeingDefined,
            boolean isHumanTurn,
            boolean hasGameBeenStopped,
            boolean isBetweenTwoTurns) {
        undoButton.setEnabled(isUndoEnabled && ((isGameStarted && !isGamePaused)
                || isSituationBeingDefined) && !hasGameBeenStopped);

        nextTurnButton.setEnabled(isWaitingForNextTurnClic
                && !hasGameBeenStopped && !isGamePaused);

        pauseButton.setVisible(!isGameStarted || !isGamePaused);
        pauseButton.setEnabled(isHumanTurn
                && !hasGameBeenStopped);
        continueButton.setVisible(isGameStarted && isGamePaused);

        startButton.setVisible(isSituationBeingDefined);
        stopButton.setVisible(isGameStarted || hasGameBeenStopped);
        stopButton.setEnabled(!hasGameBeenStopped);

        hintButton.setEnabled(isHumanTurn
                && !hasGameBeenStopped && !isGamePaused);

        if (isGameStarted) {
            status.setText("Playing");
        }

        if (isGamePaused) {
            status.setText("Paused");
        }
        if (isSituationBeingDefined) {
            status.setText("Defining situation");
        }
        if (hasGameBeenStopped) {
            status.setText("Aborted");
        }
        if (isBetweenTwoTurns) {
            status.setText("In transition between turns");
        }
        if (isWaitingForNextTurnClic && isBetweenTwoTurns) {
            status.setText("Waiting NextTurn click");
        }
    }
    

    private class PlayerInformation extends JPanel {

        private JLabel name;
        private JLabel score;
        private JLabel time;
        private final String scoreString = "score:  ";
        private final String timeString = "time:   ";

        public PlayerInformation(String n, int s, int t) {

            setLayout(layout);

            name = new JLabel(n);
            score = new JLabel(scoreString + s);

            time = new JLabel(timeString + renderTime(t));
            lim.gridy = 1;
            lim.fill = GridBagConstraints.CENTER;
            layout.setConstraints(name, lim);
            add(name);
            lim.gridy = 2;
            add(score);
            layout.setConstraints(score, lim);
            lim.gridy = 3;
            layout.setConstraints(time, lim);
            add(time);
        }

        public void setPlayerName(String newName) {
            name.setText(newName);
        }

        public void setTime(int newTime) {
            time.setText(timeString + renderTime(newTime));
        }

        public void setScore(int newScore) {
            score.setText(scoreString + newScore);
        }

        public void addBlueBorder() {
            setBorder(BorderFactory.createLineBorder(Color.BLUE));
        }

        public void removeBlueBorder() {
            setBorder(BorderFactory.createEmptyBorder());
        }
    }

    
}
