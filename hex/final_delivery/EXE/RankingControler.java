import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Model class representing RankingControler. 
 * @author Hex team
 */
public class RankingControler {

    private final int rankingsSize = 10;
    /**RankingMDControler of RankingControler*/
    private RankingMDControler rankingMDControler;
    /**RankingHCEasy of RankingControler*/
    private Ranking[] rankingHCEasy;
    /**RankingHCMedium of RankingControler*/
    private Ranking[] rankingHCMedium;
    /**RankingHCDifficult of RankingControler*/
    private Ranking[] rankingHCDifficult;
    /**RankingHCExpert of RankingControler*/
    private Ranking[] rankingHCExpert;
    /**RankingHH of RankingControler*/
    private Ranking[] rankingHH;

    /**
     * Constructor for rankingControler.
     */
    public RankingControler() {
        rankingMDControler = new RankingMDControler();

        rankingHCEasy = new Ranking[rankingsSize];
        rankingHCMedium = new Ranking[rankingsSize];
        rankingHCDifficult = new Ranking[rankingsSize];
        rankingHCExpert = new Ranking[rankingsSize];
        rankingHH = new Ranking[rankingsSize];
    }

    /**
     * 
     * @return
     */
    public int getRankingsSize() {
        return rankingsSize;
    }
    
    /**
     * Select the good ranking array according to rankingType.
     * @param rankingType the rankingType of ranking to select.
     * @return the selected array
     */
    private Ranking[] selectArray(TypeOfRanking rankingType) {
        Ranking[] ranking = null;
        switch (rankingType) {
            case HCeasy:
                ranking = rankingHCEasy;
                break;
            case HCmedium:
                ranking = rankingHCMedium;
                break;
            case HCdifficult:
                ranking = rankingHCDifficult;
                break;
            case HCexpert:
                ranking = rankingHCExpert;
                break;
            case HH:
                ranking = rankingHH;
                break;
        }
        return ranking;
    }

    /**
     * Load the content of the ranking file in the corresponding array
     * @param rankingType the rankingType to load
     * @return true if the content has been loaded correctly
     */
    private boolean load(TypeOfRanking rankingType) {
        String content = rankingMDControler.readData(rankingType);

        try {
            //split the string in 10 strings
            String[] strs = content.split(System.getProperty("line.separator"));

            if (strs.length > rankingsSize) {
                return false;
            }

            Ranking[] ranking = selectArray(rankingType);


            if ((strs.length == 1) && ("".equals(strs[0]))) {
                for (int i = 0; i < rankingsSize; ++i) {
                    ranking[i] = new Ranking(0, "", new Date());
                }
                return true;
            }

            for (int i = 0; i < strs.length; ++i) {
                Ranking rank = createFromString(strs[i]);
                if (rank == null) {
                    return false;
                } else {
                    ranking[i] = rank;
                }
            }
            for (int i = strs.length; i < rankingsSize; ++i) {
                ranking[i] = new Ranking(0, "", new Date());
            }

            return true;

        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Save the array in the corresponding ranking file
     * @param rankingType the rankingType to load
     * @return true if the save succeded
     */
    private boolean save(TypeOfRanking rankingType) {
        Ranking[] ranking = selectArray(rankingType);

        String str = "";
        for (int i = 0; i < 9; ++i) {
            str = str + saveAsString(ranking[i]) 
                    + System.getProperty("line.separator");
        }
        str = str + saveAsString(ranking[9]);

        return rankingMDControler.writeData(str, rankingType);
    }

    /**
     * Handle the new score by:
     * 1- Loading from the ranking file to the array
     * 2- Adding the new score in the array if it is a top10 score
     * 3 - Saving the array in the ranking file
     * @param rankingType the rankingType
     * @param score the score of the player
     * @param name the name of the player
     * @return true if the new score is in the top10
     * @throws RuntimeException if the file couldn't be read or written
     */
    public boolean handleNewScore(TypeOfRanking rankingType,
            int score, String name) throws RuntimeException {

        if (!load(rankingType)) {
            throw new RuntimeException("Ranking file couldn't be read");
        }

        Ranking[] ranking = selectArray(rankingType);

        for (int i = 0; i < rankingsSize; ++i) {
            if (ranking[i].getScore() < score) {
                
                for (int j = i; j < 9; ++j) {
                    ranking[i + 1] = ranking[i];
                }
                ranking[i] = new Ranking(score, name, new Date());
                if (!save(rankingType)) {
                    throw new RuntimeException("Ranking file couldn't be written");
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Do an export of the ranking given by rankingType
     * @param rankingType
     * @return the export
     * @throws RuntimeException if the file couldn't be read
     */
    public Object[][] exportData(TypeOfRanking rankingType)
            throws RuntimeException {

        if (!load(rankingType)) {
            throw new RuntimeException("Ranking file couldn't be read");
        }

        Ranking[] ranking = selectArray(rankingType);

        Object[][] data = new Object[rankingsSize][4];
        for (int i = 0; i < rankingsSize; i++) {
            data[i][0] = i + 1;
            data[i][1] = ranking[i].getName();
            data[i][2] = ranking[i].getScore();
            data[i][3] = ranking[i].getDate();
        }

        return data;
    }
    
     /**
     * Builds a string containing the score, the name and the date of the ranking,
     * separated by ",". It's used to save a ranking object in a file
     * @return the created string.
     */
    private String saveAsString(Ranking rank) {
        return rank.getScore() + ";" + rank.getName() + ";" + 
        new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z").format(rank.getDate());
    }
    
     /**
     * Does the exact opposite operation than toString: builds the ranking object
     * according to the given string which is the result of a previous toString
     * @param str the string from which the method builds the ranking.
     * @return the built ranking.
     */
    private Ranking createFromString(String str) {
        try {
            String[] strs = str.split(";");
            if (strs.length != 3) {
                return null;
            }
            
            int score = Integer.parseInt(strs[0]);
            String name = strs[1];
            Date date = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z").parse(strs[2]);

            return new Ranking(score, name, date);
            
        } catch (Exception ex) {
            return null;
        }
    }
}