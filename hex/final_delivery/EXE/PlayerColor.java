/**
 * 
 * @author Hex team
 */
public enum PlayerColor {

    /**
     * 
     */
    white,
    
    /**
     * 
     */
    black,
    
    /**
     * 
     */
    noColor;

}