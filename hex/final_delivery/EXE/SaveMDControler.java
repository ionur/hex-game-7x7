import java.io.File;

/**
 * 
 * @author Hex team
 */
public class SaveMDControler extends AbstractMDControler {

    /**
     * Constructor for SaveMDControler
     */
    public SaveMDControler() {
        super();
    }

    /**
     * Reads the content of the file whose path is filePath
     * @param filePath the path of the file
     * @return the content of the file
     */
    public String readData(String filePath) {
        File save = new File(filePath);
        return readStringFromFile(save);
    }

    /**
     * Write one content in the file whose path is filePath
     * @param content the content to write
     * @param filePath the path of the file
     * @return true if the file has been successfuly written, false otherwise
     */
    public boolean writeData(String content, String filePath) {
        File save = createFileIfDoesntExist(filePath);
        return writeStringInFile(content, save);
    }
}