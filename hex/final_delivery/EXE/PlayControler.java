import java.util.Stack;

/**
 * Controler that enable to play Hex by managing the models during the game.
 * If offers all the methods to play to Hex game.
 * @author Hex team
 */
public class PlayControler {

    private Stack<Coordinates> oldMoves;
    private Player currentPlayer;
    private Player player1;
    private Player player2;
    private Board board;
    private ConfigControler configControler;

    /**
     * Constructor for PlayControler
     * @param player1 the player#1
     * @param player2 the player#2
     * @param board the board with which the game will start
     * @param configControler the controler for the configuration of the game
     */
    public PlayControler(Player player1,
            Player player2,
            Board board,
            ConfigControler configControler) {
        this.player1 = player1;
        this.player2 = player2;
        this.board = board;
        this.configControler = configControler;

        //white starts
        currentPlayer = (player1.getColor() == PlayerColor.white) ? player1 : player2;
        oldMoves = new Stack<Coordinates>();
        
    }

    /**
     * Is used for writing the current player as "1" or "2"
     * @return "1" if current player is player1, "2" if it's player2
     */
    public String getCurrentPlayerAsString() {
        return (currentPlayer == player1) ? "1" : "2";
    }

    /**
     * Does the opposite operation of getCurrentPlayerAsString
     * @param oneOrTwo "1" for player1 starting or "2" for player2
     * @return false if the oneOrTwo string is different from "1" and "2"
     */
    public boolean setCurrentPlayerFromString(String oneOrTwo) {
        if ("1".equals(oneOrTwo)) {
            currentPlayer = player1;
            return true;
        } else if ("2".equals(oneOrTwo)) {
            currentPlayer = player2;
            return true;
        }

        return false;
    }

    private void changeCurrentPlayer() {
        currentPlayer = (currentPlayer == player1) ? player2 : player1;
    }

    private int getWidthOrDepthAccordingToLevel() {
        switch (configControler.getLevel()) {
            case easy:
                return 1;
            case medium:
                return 2;
            case difficult:
                return 3;
            case expert:
                return 4;
        }
        return 0;
    }

    /**
     * Starts the next game turn.
     * Run the clock of the current player and end the turn (by updating board, scores and time)
     * if the player is a computer one.
     * If the player is a human one, playHumanMove needs to be called once the player
     * chose the move he wants to do. 
     * @return true if playHumanMove needs to be called (human player)
     * false if playHumanMove doesn't need to be called (computer player)
     */
    public int[] startNextTurn() {

        currentPlayer.startThinking();

        if (currentPlayer.isComputer()) {
            Coordinates move = currentPlayer.applyStrategy(board,
                    getWidthOrDepthAccordingToLevel(),
                    getWidthOrDepthAccordingToLevel());
            endTurn(move);
            int[] res = {move.getX(),move.getY()};
            return res;
        }

        return null;
    }

    /**
     * Check that the selected cell is correct and end the turn by updating board, scores and time
     * @param selectedCell the place where the human player wants to put a token
     * @return false if the selectedCell is incorrect (already occupied) 
     * or if this is the computer's turn
     */
    public boolean playHumanMove(int[] selectedCell) {
        if (currentPlayer.isComputer()) {
            return false;
        }
        Coordinates move = new Coordinates(selectedCell[0],
                selectedCell[1]);
        if (isInvalidMove(move)) {
            return false;
        } else {
            endTurn(move);
            return true;
        }
    }

    private void endTurn(Coordinates move) {

        currentPlayer.stopThinking();

        oldMoves.push(new Coordinates(move));

        addToken(move);
        decreaseCurrentPlayerScore();

        if (!isEndOfGame()) {
            changeCurrentPlayer();
        }
    }

    private boolean isInvalidMove(Coordinates move) {
        return ((!isValidCoordinates(move))
                || board.isCellOccupied(move));
    }

    private boolean isValidCoordinates(Coordinates coords) {
        boolean valid = true;
        int x = coords.getX();
        int y = coords.getY();
        int z = coords.getZ();
        int boardSize = board.getSize();
        valid = valid && x <= boardSize && x > 0;
        valid = valid && y <= boardSize && y > 0;
        valid = valid && z == x + y - 1;
        return valid;
    }

    private void addToken(Coordinates move) {
        board.addToken(currentPlayer.getColor(), move);
    }

    private void removeToken(Coordinates move) {
        board.removeToken(move);
    }

    /**
     * Gives a hint (a cell where to play) to the player and apply a penalty to 
     * his score
     * @return the int as an array [x,y] 
     */
    public int[] giveAHint() {
        Coordinates hint = currentPlayer.applyStrategy(board,
                getWidthOrDepthAccordingToLevel(),
                getWidthOrDepthAccordingToLevel());
        decreaseCurrentPlayerScore();

        int[] res = new int[2];
        res[0] = hint.getX();
        res[1] = hint.getY();
        return res;
    }

    /**
     * If the game is:
     * - HH: Undo the last move without score penalties 
     * - HC : Undo the last computer move and the last human move 
     *      and apply some score penalties to the human player.
     * @param isSituationBeingDefined 
     */
    public void undo(boolean isSituationBeingDefined) {
        if ((!configControler.isUndoEnabled()) || (oldMoves.empty())) {
            return;
        }
        if (configControler.getMode() == Mode.HH) {
            removeToken(oldMoves.pop());
            changeCurrentPlayer();
        } else if (configControler.getMode() == Mode.HC) {
            removeToken(oldMoves.pop());
            
            if (isSituationBeingDefined) {
                changeCurrentPlayer();
                return;
            }
            
            if (currentPlayer.isComputer()) {
                changeCurrentPlayer();
                decreaseCurrentPlayerScore();
                return;
            }
            
            decreaseCurrentPlayerScore();
            if (!oldMoves.empty()) {
                removeToken(oldMoves.pop());
            } else {
                changeCurrentPlayer();
            }
        }
    }

    /**
     * Stops the clock of the current player
     */
    public void pauseGame() {
        currentPlayer.stopThinking();
    }

    /**
     * Start the clock of the current player, needs to be called only if Pause()
     * has been called before
     */
    public void continueGame() {
        currentPlayer.startThinking();
    }

    /**
     * Check if this is the end the game
     * @return true if one player won or if one of the player has no remaining 
     * time to play
     */
    public boolean isEndOfGame() {
        return (EvaluatingFunction.playerWon(board, currentPlayer.getColor())
                || player1.isTimeOver()
                || player2.isTimeOver());
    }

    /**
     * To add a new token when defining the situation (state) of the board
     * @param selectedCell
     * @return false if the selectedCell is incorrect or if a path has been created
     * when adding this cell, true otherwise
     */
    public boolean defineSituation(int[] selectedCell) {

        Coordinates move = null;

        move = new Coordinates(selectedCell[0],
                selectedCell[1]);

        if (isInvalidMove(move)) {
            return false;
        }

        addToken(move);
        if (isEndOfGame()) {
            removeToken(move);
            return false;
        }

        oldMoves.add(new Coordinates(move));
        changeCurrentPlayer();

        return true;
    }

    private void decreaseCurrentPlayerScore() {
        currentPlayer.setScore(currentPlayer.getScore() - 1);
    }

    /**
     * Getter for the board state
     * @return a matrix of Colors as representation of the board state
     */
    public PlayerColor[][] getBoard() {
        if (board == null) {
            return null;
        }
        return board.exportColors();
    }

    /**
     * 
     * @return
     */
    public Stack<Coordinates> getOldMoves() {
        return oldMoves;
    }

    /**
     * 
     * @param oldMoves
     */
    public void setOldMoves(Stack<Coordinates> oldMoves) {
        this.oldMoves = oldMoves;
    }

    private Player selectPlayer(PlayerNumber player) {
        if (player == PlayerNumber.player1) {
            return player1;
        } else if (player == PlayerNumber.player2) {
            return player2;
        }
        return null;
    }
    
     /**
     * Getter for the size of the board
     * @return the size of the board or 0 if the board is not instantiated yet
     * (i.e. if startGame has not been called yet)
     */
    public int getBoardSize() {
        return board.getSize();
    }

    /**
     * Getter for the remaining time of the players
     * @param player which is either player1 or player2
     * @return the remaining time of this player or 0 if the player is not instantiated yet
     * (i.e. if startGame has not been called yet)
     */
    public int getTime(PlayerNumber player) {
        Player p = selectPlayer(player);
        if (p != null) {
            return p.getTimeRemaining();
        }
        return 0;
    }

    /**
     * Getter for the scores of the players
     * @param player which is either player1 or player2
     * @return the score of this player or 0 if the player is not instantiated yet
     * (i.e. if startGame has not been called yet)
     */
    public int getScore(PlayerNumber player) {
        Player p = selectPlayer(player);
        if (p != null) {
            return p.getScore();
        }
        return 0;
    }
    
    /**
     * Getter for the color of the current player
     * @return the color of the current player
     */
    public PlayerColor getCurrentPlayerColor() {
        return currentPlayer.getColor();
    }
    
        
    /**
     * Getter for the number of the current player
     * @return the number of the current player
     */
    public PlayerNumber getCurrentPlayerNumber() {
        if (currentPlayer == player1) {
            return PlayerNumber.player1;
        }
        return PlayerNumber.player2;
    }
    
    /**
     * Asks whether the player is computer or not
     * @param p the player number
     * @return true if it is a computer player
     */
    public boolean isComputerPlayer(PlayerNumber p) {
        if (p == PlayerNumber.player1) {
            return player1.isComputer();
        }
        return player2.isComputer();
    }
    
    private void applyTimePenalitiesOnScore(Player player) {
        int score = player.getScore();
        int time = player.getTimeRemaining();
        int totalTime = configControler.getplayingTimeInMs();
        score -= 100*(totalTime - time)/((float)totalTime);
        player.setScore(score);
    }
    
    public void applyTimePenalitiesOnScore() {
        applyTimePenalitiesOnScore(player1);
        applyTimePenalitiesOnScore(player2);
    }
}