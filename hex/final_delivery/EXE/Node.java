import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Apply for the global value
 * @author hex team
 */
class Globvalue{
    public static Map mMap = new HashMap();
}
/**
 * Create the Node class to generate the searching tree
 * @author hex team
 */
public class Node {

    private PlayerColor color;
    private Coordinates coordinate; // save the coordinate of minimum value from the current board
    private int value; //compute it according evaluation function
    private int alpha = -999999;
    private int beta = 999999;
    private int minmax = 1;
    public Board board;
    public List<Cell> cells = new ArrayList<Cell>();
    public List<int[]> valuelist = new ArrayList<int[]>();
    public List<int[]> nextMoves = new ArrayList<int[]>();
    public List<Node> childrenlist = new ArrayList<Node>();
    public int nVisits = 1;
    public int depth = 0;
    public boolean visited = false;
    private int[] totvalue = {1, -1, -1, -1, -1};
    private int weight = -1;
    private List<Node> treeNode = new ArrayList<Node>();
    Globvalue global = new Globvalue();
    /**
     * set the weight of this node
     * @param w
     */
    public void setWeight(int w) {
        this.weight = w;
    }

    /**
     * get the weight of this node
     * @return
     */
    public int getWeight() {
        return this.weight;
    }

    /**
     * get the color of this node
     * @return
     */
    public PlayerColor getColor() {
        return color;
    }

    /**
     * set the color of this node
     * @param color
     */
    public void setColor(PlayerColor color) {
        this.color = color;
    }

    /**
     * generate the next new board with its parents' moves for this node
     * @param cells
     * @param boardSize
     */
    public Board generateNextBoard(List<Cell> cells, int boardSize) {
        board = new Board(boardSize);
        for (Cell c : cells) {
            board.addToken(c.getColor(), c.getCoordinates());
        }
        return board;
    }

    /**
     * get the coordinate x of this node
     * @return coordinate x
     */
    public int getx() {
        return coordinate.getX();
    }

    /**
     * get the coordinate y of this node
     * @return coordinate y
     */
    public int gety() {
        return coordinate.getY();
    }

    /**
     * get the coordinate z of this node
     * @return coordinate z
     */
    public int getz() {
        return coordinate.getZ();
    }

    /**
     * set the coordinate of this node
     * @param co
     */
    public void setCoordinate(Coordinates co) {
        this.coordinate = co;
    }

    /**
     * get the coordinate of this node
     * @return coordinate of this node
     */
    public Coordinates getCoordinate() {
        return this.coordinate;
    }

    /**
     *  set the value of this node
     * @param v
     */
    public void setvalue(int v) {

        value = v;
    }

    /**
     * get the value of this node
     * @return the value of this node
     */
    public int getvalue() {

        return value;
    }

    /**
     *  set the alpha of this node
     * @param al
     */
    public void setalpha(int al) {

        alpha = al;
    }

    /**
     * get the alpha of this node
     * @return the alpha of this node
     */
    public int getalpha() {

        return alpha;
    }

    /**
     * set the beta of this node
     * @param be
     */
    public void setbeta(int be) {

        beta = be;
    }

    /**
     * get the beta of this node
     * @return the beta of this node
     */
    public int getbeta() {

        return beta;
    }

    /**
     * get the minmax of this node
     * @return the minimax of this node
     */
    public int getminmax() {

        return minmax;
    }

    /**
     * set the minmax of this node
     * @param mm
     */
    public void setminmax(int mm) {

        minmax = mm;
    }

    /**
     * evaluate the node'board weight
     * @param board1
     * @return the node'board weight
     */
    
    public int[] evaluate(Board board1) {
        ArrayList<Move> moves = new ArrayList<Move>();
        moves = EvaluatingFunction.applyEvaluatingFunction(board);
        
        int x = moves.get(0).getCoords().getX();
        int y = moves.get(0).getCoords().getY();
        int z = moves.get(0).getCoords().getZ();
        
        String str = board.convert();
        if(globalvalue.mMap.get(str)!= null){
            return new int[]{(Integer) globalvalue.mMap.get(str),x,y,z};
        }
        Globvalue.mMap.put(str, moves.get(0).getWeight());
        
        return new int[]{moves.get(0).getWeight(),x,y,z};

    }

    /**
     * Give a new board to this node
     * @param board
     */
    public void setboard(Board board) {
        this.board = board;
    }

    /**
     * generate all the next moves from this node
     * @param width
     * @return a list of next moves
     */
    public List<int[]> generateMoves(int width) {
        ArrayList<Move> moves = new ArrayList<Move>();
        moves = EvaluatingFunction.applyEvaluatingFunction(this.board);
        
        for (Move s : moves) {
                Coordinates coordinates = new Coordinates(s.getCoords().getX(), s.getCoords().getY(), s.getCoords().getZ());
                List<Cell> cells = new ArrayList<Cell>();
                Cell cell = null;
                for (Cell c : this.cells) {
                    cells.add(c);
                }
                if (this.getColor() == PlayerColor.black) {
                    cell = new Cell(PlayerColor.white, coordinates);
                }
                if (this.getColor() == PlayerColor.white) {
                    cell = new Cell(PlayerColor.black, coordinates);
                }
                cells.add(cell);
                String str = generateNextBoard(cells, this.board.getSize()).convert();
                if(globalvalue.mMap.get(str)!= null){
                    nextMoves.add(new int[]{s.getCoords().getX(), s.getCoords().getY(), s.getCoords().getZ(), (Integer) globalvalue.mMap.get(str)});
                }
                else{
                    globalvalue.mMap.put(str, moves.get(0).getWeight());
                    nextMoves.add(new int[]{s.getCoords().getX(), s.getCoords().getY(), s.getCoords().getZ(), s.getWeight()});
                }
                width--;
                if (width == 0) {
                    break;
            }
        }
        return nextMoves;
    }

    /**
     * generate a path in the tree with the limitation of depth
     * @param depth
     * @return the coordinate of the child of the root node and this child's weight
     */
    public int[] selectAction(int depth) {
        int i = 1;
        int j = 1;
        Random generator = new Random();
        List<Node> visited = new LinkedList<Node>();
        Node cur = this;
        cur.depth = 0;
        visited.add(cur);

        int counter = generator.nextInt(depth);
        while (counter > 1) {
            int tempdepth = cur.depth;
            cur = cur.select();
            cur.depth = tempdepth + 1;
            if(cur.depth == 1){
                totvalue[1] = cur.getx();
                totvalue[2] = cur.gety();
                totvalue[3] = cur.getz();
            }
            visited.add(cur);
            cur.nVisits += i;
            i++;
            counter--;
        }
        int tempdepth = cur.depth;
        Node newNode = cur.select();
        newNode.depth = tempdepth + 1;      
        visited.add(newNode);
        int[] result=evaluate(newNode.board);
        newNode.totvalue[0] = result[0];
        int temp = newNode.totvalue[0];
        if(newNode.depth == 1){
            totvalue[1] = newNode.getx();
            totvalue[2] = newNode.gety();
            totvalue[3] = newNode.getz();
        }
        Collections.reverse(visited);
        for (Node c : visited) {
            c.totvalue[0] = temp - j;
            j++;
        }

        totvalue[4] = visited.size();
        return totvalue;
    }

    private Node select() {
        Node selected = null;
        Random r = new Random();
        int random = r.nextInt(100);
        int bestValue = -99999999;

        for (Node c : childrenlist) {
            int uctValue = (int) (c.totvalue[0] / (c.nVisits)
                    + Math.sqrt(Math.log(nVisits + 1) / (c.nVisits)) + random);
            if (uctValue > bestValue) {
                selected = c;
                bestValue = uctValue;
            }
        }
        return selected;
    }


    /**
     * generate the tree for minimax algorithm
     * @param root
      * @return a list of node
     */
    public List<Node> getTree() {
        //treeNode.clear();
        for (Node n : childrenlist) {
            treeNode.add(n);
            if (!n.childrenlist.isEmpty()) {
                treeNode.addAll(n.getTree());
            }

        }
        return treeNode;
    }

}
