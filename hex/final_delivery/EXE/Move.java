/**
 * 
 * @author Hex team
 */
public class Move {

    private Coordinates coords;
    private int weight;

    /**
     * 
     * @param coords
     * @param weight
     */
    public Move(Coordinates coords, int weight) {
        this.coords = coords;
        this.weight = weight;
    }

    /**
     * 
     * @return
     */
    public Coordinates getCoords() {
        return coords;
    }

    /**
     * 
     * @param i
     */
    public void setWeight(int i) {
        weight = i;
    }

    /**
     * 
     * @return
     */
    public int getWeight() {
        return weight;
    }
}
