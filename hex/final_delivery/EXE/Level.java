/**
 * 
 * @author Hex team
 */
public enum Level {

    /**
     * 
     */
    easy,

    /**
     * 
     */
    medium,

    /**
     * 
     */
    difficult,

  /**
   * 
   */
  expert;
  
}