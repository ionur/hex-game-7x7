
import java.util.Random;

/**
 * 
 * @author Hex team
 */
public class ConfigViewControler {

    private ConfigView configView;
    private ConfigControler configControler;

    /**
     * constructor for the ConfigViewControler
     */
    public ConfigViewControler() {
        configControler = GameControler.getConfigControler();
        updateView();
    }

    private void updateView() {
        configView = new ConfigView(this, configControler.getMode(),
                configControler.isUndoEnabled(), configControler.getLevel(),
                configControler.getplayingTimeInMs(),
                configControler.getNextTurnAfter(),
                configControler.getBoardSize(),
                configControler.getPlayer1Name(),
                configControler.getPlayer2Name(),
                configControler.getPlayer1Color());
    }

    /**
     * gets the view
     * @return
     */
    public AbstractTab getView() {
        return configView;
    }

    /**
     * sets the mode of the game
     * @param mode
     */
    public void setMode(Mode mode) {
        configControler.setMode(mode);
    }

    /**
     * sets the level of the game
     * @param level
     */
    public void setLevel(Level level) {
        configControler.setLevel(level);
    }

    /**
     * sets the undoenabled false or true
     * @param undoEnabled
     */
    public void setUndoEnabled(boolean undoEnabled) {
        configControler.setUndo(undoEnabled);
    }

    /**
     * sets the playing time in miliseconds
     * @param playingTimeInMs
     */
    public void setPlayingTimeInMs(int playingTimeInMs) {
        configControler.setplayingTimeInMs(playingTimeInMs);
    }

    /**
     * sets the board size
     * @param boardSize
     */
    public void setBoardSize(int boardSize) {
        configControler.setBoardSize(boardSize);
    }

    /**
     * sets the next turn after
     * @param nexTurnAfter
     */
    public void setNextTurnAfter(NextTurnAfter nexTurnAfter) {
        configControler.setNextTurnAfter(nexTurnAfter);
    }

    /**
     * sets the player1 color
     * @param player1Color
     */
    public void setPlayer1Color(PlayerColor player1Color) {
        configControler.setPlayer1Color(player1Color);
    }

    /**
     * sets the player1 name
     * @param player1Name
     */
    public void setPlayer1Name(String player1Name) {
        configControler.setPlayer1Name(player1Name);
    }

    /**
     * sets the player2 name
     * @param player2Name
     */
    public void setPlayer2Name(String player2Name) {
        configControler.setPlayer2Name(player2Name);
    }

    /**
     * Sets the player1 token color randomly
     */
    public void setPlayer1ColorRandomly() {
        Random rand = new Random();
        int x = rand.nextInt(2);
        if (x == 1) {
            setPlayer1Color(PlayerColor.white);
        } else {
            setPlayer1Color(PlayerColor.black);
        }
    }

    public AbstractTab getViewUpdated() {
        updateView();
        return configView;
    }
}