import java.io.File;

/**
 * Model class representing RankingMDControler
 * @author Hex team
 */
public class RankingMDControler extends AbstractMDControler {
    
    /**rankingFileHCEasy of the RankingMDControler */
    private File rankingFileHCEasy;
    /**rankingFileHCMedium of the RankingMDControler */
    private File rankingFileHCMedium;
    /**rankingFileHCDifficult of the RankingMDControler */
    private File rankingFileHCDifficult;
    /**rankingFileHCExpert of the RankingMDControler */
    private File rankingFileHCExpert;
    /**rankingFileHH of the RankingMDControler */
    private File rankingFileHH;

    /**
     * Constructor for RankingMDControler.
     */
    public RankingMDControler() {
        super();
        
        String saveRankingDir = appDir + fileSeparator + "rankings" + fileSeparator;
        createDirectoryIfDoesntExist(saveRankingDir);

        rankingFileHCEasy = createFileIfDoesntExist(saveRankingDir + "rankingHCeasy");
        rankingFileHCMedium = createFileIfDoesntExist(saveRankingDir + "rankingHCmedium");
        rankingFileHCDifficult = createFileIfDoesntExist(saveRankingDir + "rankingHCdifficult");
        rankingFileHCExpert = createFileIfDoesntExist(saveRankingDir + "rankingHCexpert");
        rankingFileHH = createFileIfDoesntExist(saveRankingDir + "rankingHH");
    }
    
    /**
     * Select the good file to write in according to the value of rankingType
     * @param rankingType the rankingType to selectFile 
     */
    private File selectFile(TypeOfRanking rankingType) {
        File f = null;
        switch (rankingType) {
            case HCeasy:
                f = rankingFileHCEasy;
                break;
            case HCmedium:
                f = rankingFileHCMedium;
                break;
            case HCdifficult:
                f = rankingFileHCDifficult;
                break;
            case HCexpert:
                f = rankingFileHCExpert;
                break;
            case HH:
                f = rankingFileHH;
                break;
        }
        return f;
    }

    /**
     * Read the content of the corresponding ranking file.
     * @param rankingType which is used to find the file 
     * @return the content
     */
    public String readData(TypeOfRanking rankingType) {
        File myFile = selectFile(rankingType);
        return readStringFromFile(myFile);
    }

    /**
     * Write in the corresponding ranking file
     * @param rankingType which is used to find the file 
     * @param content the content to write
     * @return true if the file has been successfuly written, false otherwise
     */
    public boolean writeData(String content, TypeOfRanking rankingType) {
        File myFile = selectFile(rankingType);
        return writeStringInFile(content, myFile);
    }
    
}