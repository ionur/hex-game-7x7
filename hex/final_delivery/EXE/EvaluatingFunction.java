/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;

/**
 *This class evaluate the best move according to the status of the board
 * @author Hex Group
 */
public class EvaluatingFunction {

    private static Graph graph;
    private final static int winningValue = -1;
    private final static int bridgeValue = 0;
    private static ArrayList<Move> result;

    /**
     * Sort the cells of the board according to the possible best move
     * @param board Board fo the game
     * @return Array of ordered cells
     */
    private static ArrayList<Move> sortResult(ArrayList<Move> steps) {

        for (int i = 0; i < steps.size(); i++) {
            int minimum = i;
            Move var;
            for (int j = i; j < steps.size(); j++) {
                if (steps.get(j).getWeight() < steps.get(minimum).getWeight()) {
                    minimum = j;
                }
            }
            var = steps.get(i);
            steps.set(i, steps.get(minimum));
            steps.set(minimum, var);

        }
        return steps;
    }

    /**
     * Method that apply the evaluating function
     * @param board Board of the game
     * @return The list of Cell ordered according the best move the player can do
     */
    public static ArrayList<Move> applyEvaluatingFunction(Board board) {
        result = new ArrayList<Move>();

        Graph.generateGraph(board, PlayerColor.black);

        Dijkstra.applyDijkstra(Graph.getUpBlackEdge());
        addToResult(board, Graph.getUpBlackEdge());

        Dijkstra.applyDijkstra(Graph.getDownBlackEdge());
        addToResult(board, Graph.getDownBlackEdge());

        Graph.generateGraph(board, PlayerColor.white);

        Dijkstra.applyDijkstra(Graph.getDownWhiteEdge());
        addToResult(board, Graph.getDownWhiteEdge());

        Dijkstra.applyDijkstra(Graph.getUpWhiteEdge());
        addToResult(board, Graph.getUpWhiteEdge());

        result = sortResult(result);

        for (Move m : result) {
            m.setWeight(-m.getWeight());
        }

        return result;
    }

    /**
     * Method that add the weight of the step in the grapgh, putting the value to -1 if a step is a winning move
     */
    private static void addToResult(Board board, Step begin) {
        boolean finalTurn = winningTurn();
        for (int i = 0; i < Graph.getGraph().length; i++) {
            boolean isIn = false;


            for (int j = 0; j < result.size(); j++) {
                if (Graph.getGraph()[i].getCell().getColor() == PlayerColor.noColor && result.get(j).getCoords().equals(Graph.getGraph()[i].getCell().getCoordinates())) {
                    if (winningMove(Graph.getGraph()[i])) {
                        result.get(j).setWeight(winningValue);
                    } else if (bridgeMove(Graph.getGraph()[i], board, begin) && !finalTurn) {
                        result.get(j).setWeight(bridgeValue);
                    } else if (result.get(j).getWeight() <= 0) {
                    } else {
                        result.get(j).setWeight(result.get(j).getWeight() + Graph.getGraph()[i].getWeight());
                    }

                    isIn = true;
                }
            }
            if (!isIn && Graph.getGraph()[i].getCell().getColor() == PlayerColor.noColor) {

                if (bridgeMove(Graph.getGraph()[i], board, begin) && !finalTurn) {
                    Graph.getGraph()[i].setWeight(bridgeValue);
                }
                if (winningMove(Graph.getGraph()[i])) {
                    Graph.getGraph()[i].setWeight(winningValue);
                }
                Move move = new Move(Graph.getGraph()[i].getCell().getCoordinates(), Graph.getGraph()[i].getWeight());
                result.add(move);
            }

        }
    }

    private static boolean winningTurn() {
        for (int i = 0; i < Graph.getGraph().length; i++) {
            if (winningMove(Graph.getGraph()[i])) {
                return true;
            }
        }


        return false;

    }

    private static boolean bridgeMove(Step step, Board board, Step begin) {

        if (step.getWeight() == 2 && step.getCellx() == board.getSize() && begin.equals(Graph.getUpBlackEdge())) {
            for (int i = 0; i < step.getNeighbors().size(); i++) {
                if (step.getNeighbors().get(i).getX() == 1) {
                    step.setWeight(-1);
                    return true;
                }
            }
        }

        if (step.getWeight() == 2 && step.getCellx() == 1 && begin.equals(Graph.getDownBlackEdge())) {
            for (int i = 0; i < step.getNeighbors().size(); i++) {
                if (step.getNeighbors().get(i).getX() == board.getSize()) {
                    return true;
                }
            }

        }


        if (step.getWeight() == 2 && step.getCelly() == board.getSize() && begin.equals(Graph.getDownWhiteEdge())) {
            for (int i = 0; i < step.getNeighbors().size(); i++) {
                if (step.getNeighbors().get(i).getY() == 1) {
                    return true;
                }
            }

        }

        if (step.getWeight() == 2 && step.getCelly() == 1 && begin.equals(Graph.getUpWhiteEdge())) {
            for (int i = 0; i < step.getNeighbors().size(); i++) {
                if (step.getNeighbors().get(i).getY() == board.getSize()) {
                    return true;
                }
            }

        }
        return false;

    }

    /**
     * Tells if the player with the current color won
     * @param board 
     * @param color 
     * @return if the player won
     */
    public static boolean playerWon(Board board, PlayerColor color) {
        Graph.generateGraph(board, color);
        return Graph.playerWon(color);
    }

    /**
     * 
     * @param step
     * @return
     */
    private static boolean winningMove(Step step) {

        if (Graph.getDownBlackEdge().getNeighbors().contains(step.getCell()) && Graph.getUpBlackEdge().getNeighbors().contains(step.getCell())) {
            return true;
        }

        if (Graph.getUpWhiteEdge().getNeighbors().contains(step.getCell()) && Graph.getDownWhiteEdge().getNeighbors().contains(step.getCell())) {
            return true;
        }

        return false;
    }

    /**
     * Store the value of every cell in a given vector
     * @param result 
     * @return The vector of the stored values
     */
    private static String stringResult(ArrayList<Step> result) {
        String stringRes = "";
        for (int i = 0; i < result.size(); i++) {
            stringRes = stringRes + "(" + result.get(i).getCellx() + "," + result.get(i).getCelly() + "," + result.get(i).getCellz() + "=>" + result.get(i).getWeight() + ")";
        }
        return stringRes;
    }
}
