import java.util.List;

/**
 * Create the MonteCarlo algorithm to compute the next best move
 * @author hex team
 */
public class MonteCarlo extends Strategy {

    /**
     * Compute the next best move with MonteCarlo Algorithm
     * @param board
     * @param playerColor
     * @param width 
     * @param depth 
     * @return
     */
    @Override
    public Coordinates computeBestMove(Board board, PlayerColor playerColor,
            int width, int depth) {
        treeRoot = new Node();
        treeRoot.setColor(playerColor);
        treeRoot.setvalue(-1);
        treeRoot.setboard(board);

        int[] res = monte_carlo(width, treeRoot, depth);

        return new Coordinates(res[1], res[2], res[3]);
    }

    /**
     * compute the next best move with MonteCarlo Algorithm
     * @param iterations
     * @param player
     * @param depth
     * @return
     */
    private int[] monte_carlo(int iterations, Node player, int depth) {
        int[] bestvalue = {99999999, 0, 0, 0, 0};
        creatTree(player, depth, iterations);

        for (int i = 0; i < iterations; i++) {
            int[] value = player.selectAction(depth);
            if (value[0] < bestvalue[0]) {
                bestvalue[0] = value[0];
                bestvalue[1] = value[1];
                bestvalue[2] = value[2];
                bestvalue[3] = value[3];
                bestvalue[4] = value[4];
            }
        }
        return bestvalue;
    }

    private void creatTree(Node root, int depth, int iteratoins) {
        List<int[]> nextMoves = root.generateMoves(iteratoins);
        if (depth != 0) {
            for (int[] move : nextMoves) {
                Node child = new Node();
                root.childrenlist.add(child);
                for (Cell c : root.cells) {
                    child.cells.add(c);
                }
                Coordinates coordinates = new Coordinates(move[0], move[1], move[2]);
		child.setWeight(move[3]);
                if (root.getColor() == PlayerColor.black) {
                    child.setColor(PlayerColor.white);
                    Cell cell = new Cell(PlayerColor.white, coordinates);
                    child.cells.add(cell);
                    child.generateNextBoard(child.cells, root.board.getSize());
                } else if (root.getColor() == PlayerColor.white) {
                    child.setColor(PlayerColor.black);
                    Cell cell = new Cell(PlayerColor.black, coordinates);
                    child.cells.add(cell);
                    child.generateNextBoard(child.cells, root.board.getSize());
                }
                child.depth = root.depth;
                child.depth++;
                child.setCoordinate(coordinates);
                creatTree(child, depth - 1, iteratoins);
                root.childrenlist.add(child);
            }
        }

    }
}
