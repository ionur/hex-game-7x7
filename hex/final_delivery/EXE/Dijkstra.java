/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;

/**
 *This class implement the Dijkstra algotirthm using the two-distance method.
 * @author Hex group
 */
public class Dijkstra {

    private static ArrayList<Step> evaluated = new ArrayList();

    /**
     * Method that apply Dijkstra on the board to every cell of the board given a starting point
     * @param begin Starting point of Dijkstra
     */
    public static void applyDijkstra(Step begin) {

        evaluated.clear();
        setDefaultGraph();

        addValueInAllNeighbors(begin, 1, evaluated);

        addValueInAllNeighbors(begin, 1, evaluated);

        while (evaluated.size() <= Graph.getGraph().length) {

            Step next = Graph.getNextToEvaluate(evaluated);

            next.setWeight(next.getPossibleWeights().get(1));
            addValueInAllNeighbors(next, next.getWeight() + 1, evaluated);
        }

        for (int i = 0; i < Graph.getGraph().length; i++) {
            if (Graph.getGraph()[i].getCell().getColor() != PlayerColor.noColor) {
                Graph.getGraph()[i].setWeight(Graph.getGraph()[i].getColorValue());
            }
        }

    }

    /**
     * Add a value to the possible value of a given Cell
     * @param i Value to add
     * @param cell Cell where i add the value
     */
    private static void addPossibleValue(int i, Step step) {

        step.getPossibleWeights().add(i);

    }

    /**
     * Add a value in all the neighbors of a given cell if they have not been evaluated jet
     * @param cell Cell where i will take the neighbors
     * @param i Value to add
     * @param evaluated Cells already evaluated
     */
    private static void addValueInAllNeighbors(Step step, int i, ArrayList<Step> evaluated) {

        for (int j = 0; j < step.getNeighbors().size(); j++) {
            if (!evaluated.contains(step.getNeighbors().get(j))) {
                addPossibleValue(i, Graph.findStep(step.getNeighbors().get(j)));
            }

        }
        if (!evaluated.contains(step)) {
            evaluated.add(step);
        }

    }

    /**
     * Set the initial values of every cell in the board
     */
    private static void setDefaultGraph() {
        for (int i = 0; i < Graph.getGraph().length; i++) {

            Graph.getGraph()[i].getPossibleWeights().clear();
            Graph.getGraph()[i].setWeight(101);

        }
    }

//    /**
//     * This functin String the maatrix of value
//     * @param values matrix of values to String
//     * @return the String of the values
//     */
//    public static String toString(int[] values) {
//        String result = "";
//        for (int i = 0; i < values.length; i++) {
//            result = result + " " + values[i];
//        }
//        result = result + ";";
//
//        return result;
//    }
}
