import java.util.ArrayList;

/**
 * 
 * @author Hex team
 */
public class Step {

    private Cell cell;
    /**Possible values of the cell */
    private ArrayList<Cell> neighbors;
    private ArrayList<Integer> possibleWeights;
    private int weight;
    private int dijkstraValue;
    private final int defaultValue = 0;
    private final int colorValue = 99999;

    /**
     * 
     * @param cell
     */
    public Step(Cell cell) {
        this.cell = cell;
        neighbors = new ArrayList<Cell>();
        possibleWeights = new ArrayList<Integer>();
        weight = defaultValue;
        dijkstraValue = defaultValue;
    }

    /**
     * 
     * @return
     */
    public int getDefaultValue() {
        return defaultValue;
    }

    /**
     * 
     * @return
     */
    public int getColorValue() {
        return colorValue;
    }

    /**
     * 
     * @return
     */
    public Cell getCell() {
        return cell;
    }

    /**
     * 
     * @return
     */
    public int getCellx() {
        return cell.getX();
    }

    /**
     * 
     * @return
     */
    public int getCelly() {
        return cell.getY();
    }

    /**
     * 
     * @return
     */
    public int getCellz() {
        return cell.getZ();
    }

    /**
     * 
     * @return
     */
    public int getDijkstraValue() {
        return dijkstraValue;
    }

    /**
     * 
     * @param dVal
     */
    public void setDijkstraValue(int dVal) {
        dijkstraValue = dVal;
    }

    /**
     * Getter for the neighbors of the cell
     * @return the list of neighbors of the cell
     */
    public ArrayList<Cell> getNeighbors() {
        return neighbors;
    }

    /**
     * Adds a neighbors to the list of neighbors of the cell
     * @param newNeighbor the new neighbor to add
     */
    public void addNeighbor(Cell newNeighbor) {
        neighbors.add(newNeighbor);
    }

    /**
     * Deletes all the neighbors of the cell
     */
    public void deleteNeighbors() {
        neighbors.clear();
    }

    /**
     * Getter for the possible weights of the cell
     * @return the list of possible weights of the cell
     */
    public ArrayList<Integer> getPossibleWeights() {
        return possibleWeights;
    }

    /**
     * Adds a possible weight to the list of weights of the cell
     * @param newWeight the new weight to add
     */
    public void addPossibleWeight(int newWeight) {
        possibleWeights.add(newWeight);
    }

    /**
     * Getter for the weight of the cell
     * @return the weight of the cell
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Setter for the weight of the cell
     * @param weight the weight to set
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * Cleans the neighbors of the cell: remove from the neighborood the cell itself and all the
     * cells that have a color which is not noColor
     */
    public void cleanNeighbors() {
        for (int i = 0; i < this.neighbors.size(); i++) {

            if ((neighbors.get(i).getColor() != PlayerColor.noColor) || (neighbors.get(i).getCoordinates().equals(getCell().getCoordinates()))) {
                this.getNeighbors().remove(i);

                i--;
            }
        }
    }
}
