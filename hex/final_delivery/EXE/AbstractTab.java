import java.awt.BorderLayout;

import javax.swing.JPanel;

/**
 * Abstract Tab inherited by all the views
 * @author Hex team
 */
public abstract class AbstractTab extends JPanel {

    /**
     * Constructor
     */
    public AbstractTab() {
        super();
        setLayout(new BorderLayout());
    }
}