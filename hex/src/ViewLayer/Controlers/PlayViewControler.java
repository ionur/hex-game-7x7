package ViewLayer.Controlers;

import DomainLayer.Controlers.ConfigControler;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import DomainLayer.Controlers.GameControler;
import DomainLayer.Controlers.TreeControler;
import Enumerations.Color;
import Enumerations.Level;
import Enumerations.Mode;
import Enumerations.PlayerNumber;
import Enumerations.TypeOfRanking;
import ViewLayer.Views.AbstractTab;
import ViewLayer.Views.BoardPanel;
import ViewLayer.Views.InformationsPanel;
import ViewLayer.Views.PlayView;
import ViewLayer.Views.TreePanel;
import javax.swing.Timer;

/**
 * Controler for the play view
 * @author Hex team
 */
public class PlayViewControler {

    private PlayView playView;
    private BoardPanel boardPanel;
    private InformationsPanel informationsPanel;
    private TreePanel treePanel;
    private ConfigControler configControler;
    private javax.swing.Timer updateTimeTimer;
    private javax.swing.Timer turnTimer;
    private boolean isSituationBeingDefined;
    private boolean isGameRunning;
    private boolean waitingForUserClic;
    private boolean waitingForNextTurnClick;
    private boolean isGamePaused;
    private boolean hasGameBeenStopped;
    private boolean isBetweenTwoTurns;

    /**
     * Constructor
     */
    public PlayViewControler() {
        configControler = GameControler.getConfigControler();

        boardPanel = new BoardPanel(this, configControler.getBoardSize());
        informationsPanel = new InformationsPanel(this,
                getName(PlayerNumber.player1), getScore(PlayerNumber.player1),
                getTime(PlayerNumber.player1), getName(PlayerNumber.player2),
                getScore(PlayerNumber.player2),
                getTime(PlayerNumber.player2));
        treePanel = new TreePanel(this);

        playView = new PlayView(boardPanel, informationsPanel, treePanel);

        setAllBooleansToFalse();

        updateTimeTimer = new javax.swing.Timer(100, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                int timeP1Ms = GameControler.getPlayControler().getTime(PlayerNumber.player1);
                int timeP2Ms = GameControler.getPlayControler().getTime(PlayerNumber.player2);
                if (timeP1Ms == 0 || timeP2Ms == 0) {
                    endOfGame(true);
                    updateTimeTimer.stop();
                }
                informationsPanel.updateTimes(timeP1Ms, timeP2Ms);

            }
        });
    }

    private void setAllBooleansToFalse() {
        isSituationBeingDefined = false;
        isGameRunning = false;
        waitingForUserClic = false;
        waitingForNextTurnClick = false;
        isGamePaused = false;
        hasGameBeenStopped = false;
        isBetweenTwoTurns = false;
    }

    /**
     * Getter for the play view
     * @return the play view
     */
    public AbstractTab getView() {
        return playView;
    }

    private void startNextTurn(boolean immediateTransition) {
        updateScores();
        informationsPanel.putFrameCurrentPlayer(
                GameControler.getPlayControler().getCurrentPlayerNumber());
        if (GameControler.getPlayControler().isEndOfGame()) {
            endOfGame(false);
            return;
        }

        if (!immediateTransition) {
            applyTransitionRule();
            return;
        }

        Color color = GameControler.getPlayControler().getCurrentPlayerColor();

        int coords[] = GameControler.getPlayControler().startNextTurn();
        if (coords == null) {
            //it's a human's turn
            setWaitingForUserClic(true);
        } else {
            //it was a computer move
            boardPanel.colorCell(coords[0], coords[1], color, true);
            updateTree();
            startNextTurn(false);
        }
    }

    /**
     * Starts to define the situation
     * @param isRestart is true is it is a restart of the same game
     */
    public void startGameBeforeDefiningSituation(boolean isRestart) {
        if (isRestart) {
            configControler = GameControler.getSavedConfigControler();
        } else {
            configControler = GameControler.getConfigControler();
        }
        setAllBooleansToFalse();
        setIsSituationBeingDefined(true);
        boardPanel.resetLastComputerMove();

        updateInformationPanel();

        boardPanel.updateBoardColors(GameControler.getPlayControler().getBoard());
    }

    /**
     * Starts the game after defininf the situation
     */
    public void startGameAfterDefiningSituation() {
        setAllBooleansToFalse();
        setIsSituationBeingDefined(false);
        setIsGameRunning(true);
        boardPanel.resetLastComputerMove();
        boardPanel.updateBoardColors(GameControler.getPlayControler().getBoard());
        updateInformationPanel();
        updateTimeTimer.start();

        startNextTurn(true);
    }

    private void applyTransitionRule() {
        int timeToWait = 0;
        //isGameRunning = false ??

        switch (configControler.getNextTurnAfter()) {
            case clickFromUser:
                setWaitForNextTurnClick(true);
                break;
            case twoSecondsWait:
                timeToWait = 2000;
                break;
            case fiveSecondsWait:
                timeToWait = 5000;
                break;
            case tenSecondsWait:
                timeToWait = 10000;
                break;
        }

        informationsPanel.removeFrames();
        setIsBetweenTwoTurns(true);
        if (!waitingForNextTurnClick) {
            turnTimer = new Timer(timeToWait, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    turnTimer.stop();
                    setIsBetweenTwoTurns(false);
                    startNextTurn(true);
                }
            });

            turnTimer.start();
        }

    }

    private String getName(PlayerNumber player) {
        String name = "";
        if (player == PlayerNumber.player1) {
            name = configControler.getPlayer1Name();
        }
        if (player == PlayerNumber.player2) {
            name = configControler.getPlayer2Name();
        }
        return name;
    }

    private int getScore(PlayerNumber player) {
        int score = GameControler.getPlayControler().getScore(player);
        return score;
    }

    private int getTime(PlayerNumber player) {
        int time = GameControler.getPlayControler().getTime(player);
        return time;
    }

    /**
     * Puts the game in pause
     */
    public void pauseGame() {
        if (waitingForUserClic) {
            GameControler.getPlayControler().pauseGame();
            setIsGamePaused(true);
        }
    }

    /**
     * Continues the game
     */
    public void continueGame() {
        if (isGamePaused) {
            GameControler.getPlayControler().continueGame();
            setIsGamePaused(false);
        }
    }

    private void setIsGamePaused(boolean isGamePaused) {
        this.isGamePaused = isGamePaused;
        updateButtonEnabilitiesAndStatus();

    }

    private void setIsGameRunning(boolean isGameRunning) {
        this.isGameRunning = isGameRunning;
        updateButtonEnabilitiesAndStatus();

    }

    private void setIsSituationBeingDefined(boolean isSituationBeingDefined) {
        this.isSituationBeingDefined = isSituationBeingDefined;
        updateButtonEnabilitiesAndStatus();

    }

    private void setWaitForNextTurnClick(boolean waitForNextTurnClick) {
        this.waitingForNextTurnClick = waitForNextTurnClick;
        updateButtonEnabilitiesAndStatus();

    }

    private void setWaitingForUserClic(boolean waitingForUserClic) {
        this.waitingForUserClic = waitingForUserClic;
        updateButtonEnabilitiesAndStatus();

    }

    private void setIsBetweenTwoTurns(boolean isBetweenTwoTurns) {
        this.isBetweenTwoTurns = isBetweenTwoTurns;
        updateButtonEnabilitiesAndStatus();
    }

    /**
     * Forced termination of the game
     */
    public void stopToPlay() {
        setHasGameBeenStopped(true);
        setIsGameRunning(false);
        setIsSituationBeingDefined(false);
        updateTimeTimer.stop();
    }

    /**
     * Handler for the click on the hint button
     */
    public void handleHintClick() {
        boardPanel.markHint(GameControler.getPlayControler().giveAHint());
        updateScores();
    }

    /**
     * Handler for the click on the undo button
     */
    public void handleUndoClick() {
        if (isSituationBeingDefined || isGameRunning) {
            GameControler.getPlayControler().undo(isSituationBeingDefined);
            boardPanel.resetLastComputerMove();
            boardPanel.updateBoardColors(GameControler.getPlayControler().getBoard());
            updateScores();
        }
    }

    /**
     * Handler for the click on the board
     * @param x the x coordinate
     * @param y the y coordinate
     */
    public void handleBoardClic(int x, int y) {
        Color color = GameControler.getPlayControler().getCurrentPlayerColor();
        int coords[] = {x, y};
        if (isSituationBeingDefined) {
            if (GameControler.getPlayControler().defineSituation(coords)) {
                boardPanel.colorCell(x, y, color, false);
            }
        } else if (isGameRunning && waitingForUserClic) {
            if (GameControler.getPlayControler().playHumanMove(coords)) {
                setWaitingForUserClic(false);
                boardPanel.colorCell(x, y, color, false);

                startNextTurn(false);
            }
        }
    }

    /**
     * Handler for the click on the next turn button
     */
    public void handleNextTurnClick() {
        if (waitingForNextTurnClick) {
            setWaitForNextTurnClick(false);
            startNextTurn(true);
        }
    }

    private void setHasGameBeenStopped(boolean hasGameBeenStopped) {
        this.hasGameBeenStopped = hasGameBeenStopped;
        updateButtonEnabilitiesAndStatus();
    }

    private PlayerNumber getCurrentPlayerNumber() {
        return GameControler.getPlayControler().getCurrentPlayerNumber();
    }

    private TypeOfRanking getTypeOfRaking() {
        if (configControler.getMode() == Mode.HH) {
            return TypeOfRanking.HH;
        }
        Level level = configControler.getLevel();
        if (level == Level.easy) {
            return TypeOfRanking.HCeasy;
        }
        if (level == Level.medium) {
            return TypeOfRanking.HCmedium;
        }
        if (level == Level.difficult) {
            return TypeOfRanking.HCdifficult;
        }
        if (level == Level.expert) {
            return TypeOfRanking.HCexpert;
        }
        return null;
    }

    private void endOfGame(boolean timeIsUp) {
        setAllBooleansToFalse();
        setHasGameBeenStopped(true);
        GameControler.getPlayControler().applyTimePenalitiesOnScore();
        updateScores();
        if (timeIsUp) {
            informationsPanel.setPlayerLostLabel(GameControler.getPlayControler().getCurrentPlayerColor());
            return;
        }
        informationsPanel.setPlayerWonLabel(GameControler.getPlayControler().getCurrentPlayerColor());

        PlayerNumber player = getCurrentPlayerNumber();

        if (GameControler.getPlayControler().isComputerPlayer(player)) {
            return;
        }

        String name = null;
        if (player == PlayerNumber.player1) {
            name = configControler.getPlayer1Name();
        } else {
            name = configControler.getPlayer2Name();
        }

        ViewsControler.getInstance().handleNewScore(getTypeOfRaking(),
                GameControler.getPlayControler().getScore(player),
                name);
    }

    private void updateTree() {
        treePanel.updateTree(TreeControler.getLastComputedTree());
    }

    private void updateScores() {
        informationsPanel.updateScores(
                GameControler.getPlayControler().getScore(PlayerNumber.player1),
                GameControler.getPlayControler().getScore(PlayerNumber.player2));
    }

    private void updateButtonEnabilitiesAndStatus() {
        informationsPanel.updateButtonsAndStatus(
                configControler.isUndoEnabled(),
                waitingForNextTurnClick,
                isGameRunning,
                isGamePaused,
                isSituationBeingDefined,
                waitingForUserClic,
                hasGameBeenStopped,
                isBetweenTwoTurns);
    }

    private void updateNames() {
        informationsPanel.updateNames(
                configControler.getPlayer1Name(),
                configControler.getPlayer2Name());
    }

    private void updateTimes() {
        informationsPanel.updateTimes(
                GameControler.getPlayControler().getTime(PlayerNumber.player1),
                GameControler.getPlayControler().getTime(PlayerNumber.player2));
    }

    private void updateInformationPanel() {
        updateNames();
        updateScores();
        updateTimes();
        informationsPanel.removeFrames();
    }
}
