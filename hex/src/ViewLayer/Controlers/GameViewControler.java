package ViewLayer.Controlers;

import DomainLayer.Controlers.GameControler;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * Controler for the load/save/start game functionalities
 * @author Hex team
 */
public class GameViewControler {

    private GameControler gameControler;
    private final JFileChooser fileChooser;

    /**
     * Constructor
     */
    public GameViewControler() {
        gameControler = new GameControler();
        fileChooser = new JFileChooser();
    }

    /**
     * Opens the dialog box to choose the file to load
     */
    public void loadGame() {
        UIManager.put("FileChooser.openDialogTitleText", "Load a game");
        UIManager.put("FileChooser.openButtonText", "Load");
        UIManager.put("FileChooser.openButtonToolTipText", "LoadSelectedFile");
        SwingUtilities.updateComponentTreeUI(fileChooser);
        
        int returnVal = fileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            if (!gameControler.loadGame(
                    fileChooser.getSelectedFile().getAbsolutePath())) {
                JOptionPane.showMessageDialog(null, 
                        "The game could not be loaded", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * Opens the dialog box to choose the file in which we want to save
     */
    public void saveGame() {
        UIManager.put("FileChooser.openDialogTitleText", "Save a game");
        UIManager.put("FileChooser.openButtonText", "Save");
        UIManager.put("FileChooser.openButtonToolTipText", "SaveSelectedFile");
        SwingUtilities.updateComponentTreeUI(fileChooser);
        
        SwingUtilities.updateComponentTreeUI(fileChooser);
        
        int returnVal = fileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            if (!gameControler.saveGame(
                    fileChooser.getSelectedFile().getAbsolutePath())) {
                JOptionPane.showMessageDialog(null, 
                        "The game could not be loaded", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Creates a new game
     */
    public void createGame() {
        gameControler.createGame();
    }

    /**
     * Restarts the same game
     */
    public void restartGame() {
        gameControler.restartSameGame();
    }
    
}
