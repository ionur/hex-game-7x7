package ViewLayer.Controlers;

import DomainLayer.Controlers.RankingControler;
import Enumerations.TypeOfRanking;
import ViewLayer.Views.AbstractTab;
import ViewLayer.Views.RankingView;
import javax.swing.JOptionPane;

/**
 * Controler for the ranking view
 * @author Hex team
 */
public class RankingViewControler {

    private RankingView rankingView;
    private RankingControler rankingControler;

    /**
     * Constructor
     */
    public RankingViewControler() {
        rankingView = new RankingView(this);
        rankingControler = new RankingControler();
    }

    /**
     * Getter for the ranking view
     * @param typeOfRanking the type of ranking
     * @return the ranking view
     */
    public AbstractTab getView(TypeOfRanking typeOfRanking) {
        updateView(typeOfRanking);

        return rankingView;
    }

    private void updateView(TypeOfRanking typeOfRanking) {
        //RankingView view = selectView(typeOfRanking);
        try {
            rankingView.fill(rankingControler.exportData(typeOfRanking));
        } catch (RuntimeException e) {
            JOptionPane.showMessageDialog(null,
                    "The ranking could not be loaded",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Handle new score for the ranking view
     * @param rankingType is type of ranking
     * @param score 
     * @param name
     */
    public void handleNewScore(TypeOfRanking rankingType,
            int score, String name) {
        rankingControler.handleNewScore(rankingType, score, name);
        updateView(rankingType);
    }
}