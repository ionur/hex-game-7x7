package ViewLayer.Views;

import Enumerations.Color;
import Enumerations.PlayerNumber;
import ViewLayer.Controlers.PlayViewControler;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

/**
 * The view regrouping boardPanel, informationPanel and treePanel
 * @author Hex team
 */
public class PlayView extends AbstractTab {

    protected PlayViewControler playViewControler;
    private JSplitPane splitPane;
    private JPanel playPanel;
    private GridBagConstraints lim;
    private GridBagLayout layout;

    /**
     * Constructor for the play view
     * @param boardPanel the board panel
     * @param informationsPanel the informations panel
     * @param treePanel the tree panel
     */
    public PlayView(BoardPanel boardPanel,
    InformationsPanel informationsPanel, TreePanel treePanel) {
        lim = new GridBagConstraints();
        layout = new GridBagLayout();
        playPanel = new JPanel();

        this.setLayout(layout);

        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, playPanel, treePanel);
        lim.weightx = 1;
        lim.weighty = 1;
        lim.fill = GridBagConstraints.BOTH;
        layout.setConstraints(splitPane, lim);
        this.add(splitPane);

        playPanel.setLayout(layout);
        lim.weightx = 0.8;
        lim.fill = GridBagConstraints.BOTH;
        layout.setConstraints(boardPanel, lim);
        playPanel.add(boardPanel);

        lim.weightx = 0.1;
        lim.gridx = 1;
        lim.fill = GridBagConstraints.BOTH;
        layout.setConstraints(informationsPanel, lim);
        playPanel.add(informationsPanel);
    }
}