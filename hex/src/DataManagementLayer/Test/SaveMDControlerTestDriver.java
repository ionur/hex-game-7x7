package DataManagementLayer.Test;

import DataManagementLayer.SaveMDControler;
import Tools.AbstractTestDriver;

/**
 * TestDriver for SaveMDControlerTestDriver class
 * @author Hex team
 */
public class SaveMDControlerTestDriver extends AbstractTestDriver {

     /**
     * Driver
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        SaveMDControler controler = null;

        while (true) {
            try {
                String[] cmd = getNewCmd();
                int i = 0;


                i = Integer.parseInt(cmd[0]);


                switch (i) {
                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;
                    case 1:
                        controler = new SaveMDControler();
                        print("");
                        break;
                    case 2:
                        print(controler.readData(cmd[1]));
                        break;
                    case 3:
                        print(controler.writeData(cmd[1], cmd[2]));
                        break;

                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class SaveMDControler ########");
        print("1 -> SaveMDControler()");
        print("2 attr1-> String readData(String fileName)");
        print("3 attr1 attr2 -> boolean writeData(String content, String fileName)");
        print("");
    }
}
