package DataManagementLayer.Test;

import DataManagementLayer.RankingMDControler;
import Enumerations.TypeOfRanking;
import Tools.AbstractTestDriver;

/**
 * TestDriver for RankingMDControlerTestDriver class
 * @author Hex team
 */
public class RankingMDControlerTestDriver extends AbstractTestDriver {
    
    /**
     * Driver
     * @param args
     */
    public static void main(String[] args) {
        
        displayWelcomeMessage();

        RankingMDControler controler = null;

        while (true) {
            try {
                String[] cmd = getNewCmd();
                int i = 0;


                i = Integer.parseInt(cmd[0]);


                switch (i) {
                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;
                    case 1:
                        controler = new RankingMDControler();
                         print(controler);
                        break;
                    case 2:
                        print(controler.readData(TypeOfRanking.valueOf(cmd[1])));                        
                        break;
                    case 3:
                        print(controler.writeData(cmd[1],TypeOfRanking.valueOf(cmd[2])));
                        break;
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class RankingMDControler ########");
        print("1 -> RankingMDControler()");
        print("2 attr1 -> String readData(TypeOfRanking rankingType)"
                        + "\t attr1 **must be** HCeasy or HCmedium or HCdifficult or HCexpert or HH");

        print("3 attr1 attr2 -> boolean writeData(String content, TypeOfRanking rankingType)"
                        + "\t attr2 **must be** HCeasy or HCmedium or HCdifficult or HCexpert or HH");
        print("");
    }
}
