package Enumerations;

/**
 * 
 * @author Hex team
 */
public enum TypeOfRanking {

    /**
     * 
     */
    HCeasy,

  /**
   * 
   */
  HCmedium,

  /**
   * 
   */
  HCdifficult,

  /**
   * 
   */
  HCexpert,

  /**
   * 
   */
  HH;

}