package Enumerations;

/**
 * 
 * @author Hex team
 */
public enum Color {

    /**
     * 
     */
    white,
    
    /**
     * 
     */
    black,
    
    /**
     * 
     */
    noColor;

}