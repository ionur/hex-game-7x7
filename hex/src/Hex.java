
import ViewLayer.Controlers.ViewsControler;
import javax.swing.UIManager;

/**
 * Entry point of the application
 * @author Hex team
 */
public class Hex {
	
	

    /**
     * Main of the Hex game, entry point of the application
     * @param args
     */
    public static void main(String[] args) {

    	try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
        }

        javax.swing.SwingUtilities.invokeLater(
                new Runnable() {

                    @Override
                    public void run() {
                        ViewsControler.getInstance();
                    }
                });
    }
}
