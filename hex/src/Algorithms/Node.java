/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import Enumerations.Color;
import DomainLayer.Models.Board;
import DomainLayer.Models.Cell;
import DomainLayer.Models.Coordinates;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;



/**
 * Apply for the global value
 * @author hex team
 */
class Globalvalue{
    public static Map mMap = new HashMap();
}
/**
 * Create the Node class to generate the searching tree
 * @author hex team
 */
public abstract class Node {

    private Color color;
    private Coordinates coordinate;
    public Board board;
    public List<Cell> cells = new ArrayList<Cell>();
    public List<int[]> nextMoves = new ArrayList<int[]>();
    public int depth = 0;
    private int weight = -1;
    Globalvalue global = new Globalvalue();

    /**
     * set the weight of this node
     * @param w
     */
    public void setWeight(int w) {
        this.weight = w;
    }

    /**
     * get the weight of this node
     * @return
     */
    public int getWeight() {
        return this.weight;
    }

    /**
     * get the color of this node
     * @return
     */
    public Color getColor() {
        return color;
    }

    /**
     * set the color of this node
     * @param color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * generate the next new board with its parents' moves for this node
     * @param cells
     * @param boardSize
     */
    public Board generateNextBoard(List<Cell> cells, int boardSize) {
        board = new Board(boardSize);
        for (Cell c : cells) {
            board.addToken(c.getColor(), c.getCoordinates());
        }
        return board;
    }

    /**
     * get the coordinate x of this node
     * @return coordinate x
     */
    public int getx() {
        return coordinate.getX();
    }

    /**
     * get the coordinate y of this node
     * @return coordinate y
     */
    public int gety() {
        return coordinate.getY();
    }

    /**
     * get the coordinate z of this node
     * @return coordinate z
     */
    public int getz() {
        return coordinate.getZ();
    }

    /**
     * set the coordinate of this node
     * @param co
     */
    public void setCoordinate(Coordinates co) {
        this.coordinate = co;
    }

    /**
     * get the coordinate of this node
     * @return coordinate of this node
     */
    public Coordinates getCoordinate() {
        return this.coordinate;
    }

    /**
     * Give a new board to this node
     * @param board
     */
    public void setboard(Board board) {
        this.board = board;
    }

    /**
     * generate all the next moves from this node
     * @param width
     * @return a list of next moves
     */
    public List<int[]> generateMoves(int width) {
        ArrayList<Move> moves = new ArrayList<Move>();
        moves = EvaluatingFunction.applyEvaluatingFunction(this.board);
        
        for (Move s : moves) {
                Coordinates coordinates = new Coordinates(s.getCoords().getX(), s.getCoords().getY(), s.getCoords().getZ());
                List<Cell> cells = new ArrayList<Cell>();
                Cell cell = null;
                for (Cell c : this.cells) {
                    cells.add(c);
                }
                if (this.getColor() == Color.black) {
                    cell = new Cell(Color.white, coordinates);
                }
                if (this.getColor() == Color.white) {
                    cell = new Cell(Color.black, coordinates);
                }
                cells.add(cell);
                String str = generateNextBoard(cells, this.board.getSize()).convert();
                if(global.mMap.get(str)!= null){
                    nextMoves.add(new int[]{s.getCoords().getX(), s.getCoords().getY(), s.getCoords().getZ(), (Integer) global.mMap.get(str)});
                }
                else{
                    global.mMap.put(str, moves.get(0).getWeight());
                    nextMoves.add(new int[]{s.getCoords().getX(), s.getCoords().getY(), s.getCoords().getZ(), s.getWeight()});
                }
                width--;
                if (width == 0) {
                    break;
            }
        }
        return nextMoves;
    }





}
