/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import DomainLayer.Models.Board;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


/**
 *
 * @author Administrator
 */
public class NodeMonte extends NodeRoot {
 
    private int[] totvalue = {1, -1, -1, -1, -1};
    public int nVisits = 1;
    public List<NodeMonte> Monte_childrenlist = new ArrayList<NodeMonte>();
    public NodeRoot noderoot = new NodeRoot();
    //Globvalue glob = new Globvalue();
    
 
    /**
     * evaluate the node'board weight
     * @param board1
     * @return the node'board weight
     */
    
    public int[] evaluate(Board board1) {
        ArrayList<Move> moves = new ArrayList<Move>();
        moves = EvaluatingFunction.applyEvaluatingFunction(board);
        
        int x = moves.get(0).getCoords().getX();
        int y = moves.get(0).getCoords().getY();
        int z = moves.get(0).getCoords().getZ();
        
        String str = board.convert();
        if(global.mMap.get(str)!= null){
            return new int[]{(Integer) global.mMap.get(str),x,y,z};
        }
        global.mMap.put(str, moves.get(0).getWeight());
        
        return new int[]{moves.get(0).getWeight(),x,y,z};

    }
 
    /**
     * generate a path in the tree with the limitation of depth
     * @param depth
     * @return the coordinate of the child of the root node and this child's weight
     */
    public int[] selectAction(int depth) {
        int i = 1;
        int j = 1;
        Random generator = new Random();
        List<NodeMonte> visited = new LinkedList<NodeMonte>();
        NodeMonte cur = this;
        cur.depth = 0;
        visited.add(cur);

        int counter = generator.nextInt(depth);
        while (counter > 1) {
            int tempdepth = cur.depth;
            cur = cur.select();
            cur.depth = tempdepth + 1;
            if(cur.depth == 1){
                totvalue[1] = cur.getx();
                totvalue[2] = cur.gety();
                totvalue[3] = cur.getz();
            }
            visited.add(cur);
            cur.nVisits += i;
            i++;
            counter--;
        }
        int tempdepth = cur.depth;
        NodeMonte newNode = cur.select();
        newNode.depth = tempdepth + 1;      
        visited.add(newNode);
        int[] result=evaluate(newNode.board);
        newNode.totvalue[0] = result[0];
        int temp = newNode.totvalue[0];
        if(newNode.depth == 1){
            totvalue[1] = newNode.getx();
            totvalue[2] = newNode.gety();
            totvalue[3] = newNode.getz();
        }
        Collections.reverse(visited);
        for (NodeMonte c : visited) {
            c.totvalue[0] = temp - j;
            j++;
        }

        totvalue[4] = visited.size();
        return totvalue;
    }

    private NodeMonte select() {
        NodeMonte selected = null;
        Random r = new Random();
        int random = r.nextInt(100);
        int bestValue = -99999999;

        for (NodeMonte c : Monte_childrenlist) {
            int uctValue = (int) (c.totvalue[0] / (c.nVisits)
                    + Math.sqrt(Math.log(nVisits + 1) / (c.nVisits)) + random);
            if (uctValue > bestValue) {
                selected = c;
                bestValue = uctValue;
            }
        }
        return selected;
    }
}
