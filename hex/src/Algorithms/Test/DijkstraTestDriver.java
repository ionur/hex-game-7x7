package Algorithms.Test;


import Algorithms.Dijkstra;
import Algorithms.Graph;
import Enumerations.Color;
import DomainLayer.Models.Board;
import DomainLayer.Models.Coordinates;
import Tools.AbstractTestDriver;

/**
 * 
 * @author Hex team
 */
public class DijkstraTestDriver extends AbstractTestDriver {

	
	
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
	
	Board board=null;
	Graph graph=null;

    
    displayWelcomeMessage();
    while (true) {
        try {
        	
            String[] cmd = getNewCmd();
            int i = 0;
            i = Integer.parseInt(cmd[0]);
            
            switch (i) {
            
            case -1:
                displayQuitting();
                return;
            case 0:
                displayHelp();
                break;

            case 1:

            	switch(Integer.parseInt(cmd[1])){
//            	case 0:
//            		print("\n"+Dijkstra.stringRes()+"\n");
//            		break;
//            	case 1:
//            		print("\n"+Dijkstra.stringRes()+"\n");
//            		break;
//            	case 2:
//            		print("\n"+Dijkstra.stringRes()+"\n");
//            		break;
//            	case 3:
//            		print("\n"+Dijkstra.stringRes()+"\n");
//            		break;
//            	}
//            	break;
//
//            case 2:
//            	print("\n"+Dijkstra.stringRes()+"\n");
//            	break;
            case 3:
            	board = new Board(Integer.parseInt(cmd[1]));
            	print("Board created\n");
            	break;
            case 4:
            	board.addToken(Color.valueOf(cmd[1]), new Coordinates(Integer.parseInt(cmd[2]), Integer.parseInt(cmd[3])) );
            	print("token added\n");
            	break;

            case 5:
            	//graph = new Graph(board,Color.valueOf(cmd[1]));
            	print("");
            	break;
            	
           
            
            
            }
            }
        } catch (Exception ex) {
            
        }
    }
    
	}
	
	 private static void displayHelp() {
	    print("\n######## Methods of the class Dijkstra ########");
        print("1 attr1 -> applyDijkstra(Cell begin)\n\t0 -> UpBlackEdge" +
                "\n\t1 -> DownBlackEdge"+
                "\n\t2 -> UpWhiteEdge"+
                "\n\t3 -> DownWhiteEdge");
        print("\n######## Methods to create a Board ########");

        print("2 attr1 -> Create board(int size)");
        print("3 attr1 attr2 atttr3-> addToken(Color color, int x, int y)");
        print("\n######## Methods to create the Graph ########");
        print("4 attr1 -> Create Graph(Board board, Color color)\n\t attr1 is can be only the string 'white' or 'black'");

        
	    }

}
