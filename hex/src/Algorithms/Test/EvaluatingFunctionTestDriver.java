package Algorithms.Test;

import java.util.ArrayList;

import Algorithms.Dijkstra;
import Algorithms.EvaluatingFunction;
import Algorithms.Move;
import Algorithms.Step;
import Enumerations.Color;
import DomainLayer.Models.Board;
import DomainLayer.Models.Cell;
import DomainLayer.Models.Coordinates;
import Tools.AbstractTestDriver;

/**
 * 
 * @author Hex team
 */
public class EvaluatingFunctionTestDriver extends AbstractTestDriver {
	
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
    	
        Board board = null;

        
		displayWelcomeMessage();
		

        while (true) {
            
            	
                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);
                
                switch (i) {
                
                case -1:
                    displayQuitting();
                    return;
                case 0:
                    displayHelp();
                    break;
                case 1:
                	EvaluatingFunction.applyEvaluatingFunction(board);
                	print(stringResult(EvaluatingFunction.applyEvaluatingFunction(board)));
                	break;
                case 2:
                	print(EvaluatingFunction.playerWon(board, Color.valueOf(cmd[1])));
                	print("");
                	break;
                case 3:
                	board = new Board(Integer.parseInt(cmd[1]));
                	print("Board created\n");
                	break;
                case 4:
                	board.addToken(Color.valueOf(cmd[1]), new Coordinates(Integer.parseInt(cmd[2]), Integer.parseInt(cmd[3])) );
                	print("token added\n");
                	break;
                }
                


        
        }

	}
	
    private static void displayHelp() {
        print("1 ->applyEvaluatingFuntion(Board board)");
    	print("2  attr1-> boolean playerWon(Board board, Color color)" 
    			+ "\t attr1 **must be** noColor or white or black");
    	print("######################################");
    	print("3 attr1 -> Create board(int size)");
        print("4 attr1 attr2 atttr3-> addToken(Color color, int x, int y)");
        print("");
    
    
    }
    
    
    /**
     * 
     * @param result
     * @return
     */
    public static String stringResult(ArrayList<Move> result)
    {
        String stringRes="";
        for(int i=0; i<result.size(); i++)
        	stringRes=stringRes+"("+result.get(i).getCoords().getX()+result.get(i).getCoords().getY()+result.get(i).getCoords().getY()+result.get(i).getWeight()+")";
                return stringRes;
    }
}
