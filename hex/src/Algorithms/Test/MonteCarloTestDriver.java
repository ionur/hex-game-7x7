package Algorithms.Test;

import Algorithms.MonteCarlo;
import DomainLayer.Models.Board;
import DomainLayer.Models.Coordinates;
import Enumerations.Color;
import Tools.AbstractTestDriver;

/**
 * test the Montecarlo class
 * @author hex team
 */
public class MonteCarloTestDriver extends AbstractTestDriver {

    /**
     * Driver for Montecarlo Algorithm
     * @param args
     */
    public static void main(String[] args) {

        Board board = null;
        MonteCarlo mon = null;

        displayWelcomeMessage();
        while (true) {
            try {

                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);

                switch (i) {

                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;
                    case 1:
                        board = new Board(Integer.parseInt(cmd[1]));
                        print("Board created");
                        break;
                    case 2:
                        board.addToken(Color.valueOf(cmd[1]),
                                new Coordinates(Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3])));
                        print("token added");
                        break;

                    case 3:
                        mon = new MonteCarlo();
                        Coordinates coords = mon.computeBestMove(board,
                                Color.valueOf(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]));
                        print("(" + coords.getX() + "," + coords.getY() + ")");

                        break;

                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class MonteCarlo ########");
        print("1 attr1 -> Create board(int size)");
        print("2 attr1 attr2 atttr3-> addToken(Color color, int x, int y)");
        print("3 attr1 attr2 attr3 -> computeBestMove(Color color, int widht, int depth)");
        print("");
    }
}
