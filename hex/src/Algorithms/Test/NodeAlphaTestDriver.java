/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Algorithms.Test;

import DomainLayer.Models.Cell;
import Algorithms.NodeAlpha;
import Enumerations.Color;
import DomainLayer.Models.Board;
import DomainLayer.Models.Coordinates;
import java.util.ArrayList;
import java.util.List;
import Tools.AbstractTestDriver;

/**
 * test the Node class
 * @author Hex team
 */
public class NodeAlphaTestDriver extends AbstractTestDriver {
    /**
     * Driver for NodeAlpha
     * @param args
     */
    public static void main(String[] args) {
        displayWelcomeMessage();

        Board board = new Board(6);
        int width = 3;
        NodeAlpha node = new NodeAlpha();
        Color color = null;
        Cell cell = null;
        List<int[]> nextMoves = new ArrayList<int[]>();

        while (true) {
            try {
                String[] cmd = getNewCmd();
                int i = 0;

                i = Integer.parseInt(cmd[0]);

                 switch (i) {
                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;

                    case 1:
                        node.setColor(Color.valueOf(cmd[1]));
                        break;
                    case 2:
                        print(node.getColor());
                        break;
                    case 3:
                        node.setalpha(Integer.parseInt(cmd[1]));
                        break;
                    case 4:
                        print(node.getalpha());
                        print("");
                        break;
                    case 5:
                        node.setbeta(Integer.parseInt(cmd[1]));
                        break;
                    case 6:
                        print(node.getbeta());
                        print("");
                        break;
                    case 7:
                        node.setboard(board);
                        print("The board finish setting");
                        break;
                    //case 8:
                        //node.setvalue(node.evaluate(board)[0]);
                        //print("The board evaluation value is: " + node.getvalue());
                        //break;
                    case 9:
                        width = Integer.parseInt(cmd[1]);
                        break;
                    case 10:
                        nextMoves = node.generateMoves(width);
                        LOOP: for (int[] move : nextMoves){
                           print("The next move coordinate is: { "+ move[0] + ", "+ move[1] + ", " + move[2] + " }");
                           width--;

                            if (width == 0) {
                                break LOOP;
                             }
                        }
                        break;
                    case 11:
                        int j = 0;
                        for (int[] move : nextMoves){
                            NodeAlpha nodeopp = new NodeAlpha();

                            Coordinates coordinate = new Coordinates(move[0], move[1], move[2]);
                            if(color == Color.black){
                                cell =  new Cell(Color.white,coordinate);
                            }
                            else {
                                cell =  new Cell(Color.black,coordinate);
                            }

                            nodeopp.cells.add(cell);
                            nodeopp.generateNextBoard(nodeopp.cells,6);
                            j++;
                            print("The board"+ j + " is generated");

                        }
                        break;
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }
     private static void displayHelp() {
        print("\n######## Methods of the class Cell ########");
        print("1 attr1  -> setColor(Color color)");
        print("2 -> getColor()");
        print("3 attr1  -> setalpha(int alpha)");
        print("4 -> getalpha()");
        print("5 attr1  -> setbeta(int beta)");
        print("6 -> getbeta()");
        print("7 -> setboard()");
        //print("8 -> evaluate(Board board)");
        print("9 attr1  ->  setwidth(int width)");
        print("10 ->  generateMoves(int width)");
        print("11 ->  generateNextBoard(List<Cell> cells)");
     }
}
