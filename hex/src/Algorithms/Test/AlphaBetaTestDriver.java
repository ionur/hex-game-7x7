package Algorithms.Test;

import Algorithms.AlphaBeta;
import DomainLayer.Models.Board;
import DomainLayer.Models.Coordinates;
import Enumerations.Color;
import Tools.AbstractTestDriver;

/**
 *
 * @author Hex Group
 */
public class AlphaBetaTestDriver extends AbstractTestDriver {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        Board board = null;
        AlphaBeta ab = null;

        displayWelcomeMessage();
        while (true) {
            try {

                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);

                switch (i) {

                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;
                    case 1:
                        board = new Board(Integer.parseInt(cmd[1]));
                        print("Board created");
                        break;
                    case 2:
                        board.addToken(Color.valueOf(cmd[1]),
                                new Coordinates(Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3])));
                        print("token added");
                        break;
                    case 3:
                        ab = new AlphaBeta();
                        Coordinates coords = ab.computeBestMove(board,
                                Color.valueOf(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]));
                        print("(" + coords.getX() + "," + coords.getY() + ")");
                        break;

//                    case 9:
//                        ab = new AlphaBeta();
//                        node = new Node();
//                        node.setColor(Color.valueOf(cmd[1]));
//                        node.setboard(board);
//                        ab.minimax(Integer.parseInt(cmd[3]), node, Integer.parseInt(cmd[2]));
//                        node.getTreeMinimax(node);
//                        for (Node n : node.treeNode) {
//                            print("the depth of this node is " + n.depth);
//                            print("the coordinate of the node is Coodinates{" + n.getx() + " " + n.gety() + " " + n.getz() + "}");
//                            print("the weight of the node is " + n.getWeight());
//                        }
//                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class AlphaBeta ########");
        print("1 attr1 -> Create board(int size)");
        print("2 attr1 attr2 attr3-> addToken(Color color, int x, int y)");
        print("3 attr1 attr2 attr3 -> computeBestMove(Color color, int widht, int depth)");
        //print("9 attr1 attr2 attr3 printTheTree(Color color, int widht, int depth)");
        print("");


    }
}
