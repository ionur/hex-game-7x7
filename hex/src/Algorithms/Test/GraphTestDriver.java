package Algorithms.Test;


import Algorithms.Graph;
import Algorithms.Step;
import Enumerations.Color;
import DomainLayer.Models.Board;
import DomainLayer.Models.Cell;
import DomainLayer.Models.Coordinates;
import Tools.AbstractTestDriver;

/**
 * 
 * @author Hex team
 */
public class GraphTestDriver extends AbstractTestDriver {

	
	
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
	
	Board board=null;
	Graph graph=null;
	Cell cell = null;

    
    displayWelcomeMessage();
    while (true) {
        try {
        	
            String[] cmd = getNewCmd();
            int i = 0;
            i = Integer.parseInt(cmd[0]);
            
            switch (i) {
            
            case -1:
                displayQuitting();
                return;
            case 0:
                displayHelp();
                break;

            case 1:
            	//graph = new Graph(board,Color.valueOf(cmd[1]));
            	print("created\n");
            	break;
            case 2:
            	//Step next=graph.getNextToEvaluate();
            	//print("The next step to evaluate has the cell with coordinates : ["+next.getCellx()+" ,"+next.getCelly()+" ,"+next.getCellz()+" ]");
            	break;
            case 3:	
            	print(graph.playerWon(Color.valueOf(cmd[1])));
            	print("");
            	break;
            case 4:	
            	print(graph.getUpWhiteEdge());
            	print("");
            	break;
            case 5:	
            	print(graph.getDownWhiteEdge());
            	print("");
            	break;
            case 6:	
            	print(graph.getUpBlackEdge());
            	print("");
            	break;
            case 7:	
            	print(graph.getDownBlackEdge());
            	print("");
            	break;
            case 8:	
            	print(graph.findStep(cell));
            	break;
            case 9:	
            	//print(graph.getEvaluated());
            	break;
            	
            	
            case 10:
            	board = new Board(Integer.parseInt(cmd[1]));
            	print("Board created\n");
            	break;
            case 11:
            	board.addToken(Color.valueOf(cmd[1]), new Coordinates(Integer.parseInt(cmd[2]), Integer.parseInt(cmd[3])) );
            	print("token added\n");
            	break;
            case 12:
            	cell=new Cell(Color.valueOf(cmd[1]), new Coordinates(Integer.parseInt(cmd[2]), Integer.parseInt(cmd[3])) );
            	print("token added\n");
            	break;
          
            	
            	

           
            
            
            }
        }catch (Exception ex) {
            
        }
    }
    
	}
	
	 private static void displayHelp() {
	    print("\n######## Methods of the class Dijkstra ########");
	    print("1 attr1 -> Create Graph(Board board, Color color)\n\t attr1 is can be only the string 'white' or 'black'");
        print("2  ->Step getNextToEvaluate()");
        print("3 attr1 -> boolean layerWon(Color color)\n\t attr1 is can be only the string 'white' or 'black'");
        print("4  ->Step getUpWhiteEdge()");
        print("5  ->Step getDownWhiteEdge()");
        print("6  ->Step getUpBlackEdge()");
        print("7  ->Step getDownBlackEdge()");
        print("8  attr1->Step findStep(Cell cell)");
        print("9  ->ArrayList<Step> getEvaluated()");
        
        print("\n######## Methods to create a Board ########");
        print("10 attr1 -> Create board(int size)");
        print("11 attr1 attr2 atttr3-> addToken(Color color, int x, int y)");
        print("12 attr1 attr2 atttr3-> createCell(Color color, int x, int y)");
        
	    }

}