package Algorithms.Test;

import Tools.AbstractTestDriver;
import Algorithms.Step;
import DomainLayer.Models.Cell;
import DomainLayer.Models.Coordinates;
import Enumerations.Color;



/**
 * 
 * @author Hex team
 */
public class StepTestDriver extends AbstractTestDriver {
	
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
    	
    	Step step=null;
    	Cell cell = null;
    	
        

        
		displayWelcomeMessage();
		

        while (true) {
            try {
            	
                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);
                
                switch (i) {
                
                case -1:
                    displayQuitting();
                    return;
                case 0:
                    displayHelp();
                    break;
                case 1:
                	step = new Step (cell);
                	print(step);
                	break;
                case 2:
                	step.getCell();
                	print(step.getCell());                	
                	
                	break;
                case 3:
                	step.getCellx();
                	print(step.getCell());
                	break;
                case 4:
                	step.getCelly();
                	print(step.getCell());
                	break;
                case 5:
                	step.getCellz();
                	print(step.getCell());
                	break;
                case 6:
                	step.getDijkstraValue();
                	print(step.getDijkstraValue());
                	break;
                case 7:
                	step.setDijkstraValue(Integer.parseInt(cmd[1]));
                	print(step.getDijkstraValue()+"\n");
                	break;
                case 8:
                	print(step.getNeighbors());
                	print("");
                	break;
                case 9:
                	step.addNeighbor(cell);
                	print("");
                	break;
                case 10:
                	step.cleanNeighbors();
                	print("neighbor vector size: "+step.getNeighbors().size()+"\n");
                	break;
                case 11:
                	print(step.getPossibleWeights());
                	break;
                case 12:
                	step.addPossibleWeight(Integer.parseInt(cmd[1]));
                	print("");
                	break;
                case 13:
                	step.getWeight();
                	print("");
                	break;
                case 14:
                	step.setWeight(Integer.parseInt(cmd[1]));
                	print("");
                	break;
                case 15:
                	step.cleanNeighbors();
                	print(step.getNeighbors()+"\n");
                	break;
                case 16:
                	
                	print(step.getDefaultValue()+"\n");
                	break;
                case 17:
                	step.cleanNeighbors();
                	print(step.getColorValue()+"\n");
                	break;
                	
                case 18:
                	Color color = Color.valueOf(cmd[1]);
                	Coordinates coordinates = new Coordinates(
                            Integer.parseInt(cmd[1]),
                            Integer.parseInt(cmd[2]),
                            Integer.parseInt(cmd[3]));
                	cell = new Cell(color, coordinates);
                	
                	break;

                
                	
                
                }
                
            }catch (Exception ex) {
                    displayErrorMessage();
	            }
        
        }

	}
	
    private static void displayHelp() {
        print("1 -> void Step(Cell cell)");
        print("2 -> Cell getCell()");
        print("3 -> int getCellx()");
        print("4 -> int getCelly()");
        print("5 -> int getCellz()");
        print("6 -> int getDijkstraValue()");
        print("7 attr 1-> int setDijkstraValue(int val)");
        print("8 -> ArrayList<Cell> getNeighbors()");
        print("9 -> void addNeighbors()");
        print("10 -> void deleteNeighbors()");
        print("11 -> ArrayList<Integer> getPossibleWeights()");
        print("12 attr1-> void addPossibleWeights(int val)");
        print("13 -> int getWeight()");
        print("14 -> int setWeight()");
        print("15 -> Cell getCell()");
        print("15 -> int getDefaultValue()");
        print("16 -> int getColoredValue()");
        print("\n###########################\n");
        print("17 attr1 attr2 atttr3-> createCell(Color color, int x, int y)");
        print("");
    
    
    }
}
