package Algorithms;

import java.util.ArrayList;

import DomainLayer.Models.Board;
import DomainLayer.Models.Cell;
import DomainLayer.Models.Coordinates;
import Enumerations.Color;

/**
 * This class rappresent the structure of a Board and provide the method to orerate on it
 * @author Prestec
 */
public class Graph {

    /**Cells in the board */
    private static Step[] graph;
    private static Board board;
    /**Ghost edge over the upper white edge     */
    private static final Step upWhiteEdge = new Step(new Cell(Color.white, new Coordinates(7, 7, 7)));
    /**Ghost edge over the lower white edge     */
    private static final Step downWhiteEdge = new Step(new Cell(Color.white, new Coordinates(0, 0, 0)));
    /**Ghost edge over the upper black edge     */
    private static final Step upBlackEdge = new Step(new Cell(Color.black, new Coordinates(0, 0, 0)));
    /**Ghost edge over the lower black edge     */
    private static final Step downBlackEdge = new Step(new Cell(Color.black, new Coordinates(7, 7, 7)));

    /**
     * Constructor of Graph
     * @param gameBoard 
     * @param color 
     */
    public static void generateGraph(Board gameBoard, Color color) {

        board = gameBoard;


        Cell[] cells = exractCells(color);
        graph = new Step[cells.length];
        for (int i = 0; i < cells.length; i++) {
            graph[i] = new Step(cells[i]);
        }

        findAllneighbors(color);

    }

    /**
     * This method find the all the neighbors of every cell on the board depending on the Color of the turn 
     * @param turn , Color of the player's turn 
     */
    private static void findAllneighbors(Color turn) {

        deleteAllNeighbors();

        for (int i = 0; i < graph.length; i++) {

            findneighbors(graph[i], turn);
            if (graph[i].getCell().getColor() != Color.noColor) {
                graph[i].cleanNeighbors();
            }

        }

        if (turn == Color.black) {

            linkBlackGhostEdge(downBlackEdge, board.getSize());
            linkBlackGhostEdge(upBlackEdge, 1);

        } else if (turn == Color.white) {

            linkWhiteGhostEdge(downWhiteEdge, 1);
            linkWhiteGhostEdge(upWhiteEdge, board.getSize());
        }



    }

    /**
     * This method find the neighbors of a given cell
     * @param cell ,cell used
     * @param turn , Color  of the player's turn
     * @param neighbors , neighbors of the cell it's usefull to avoid loops
     * @return The list of neighbors of the cell
     */
    private static ArrayList<Cell> findneighbors(Step step, Color turn) {

        for (int i = 0; i < graph.length; i++) {


            if (neighbours(step.getCell(), graph[i].getCell()) && graph[i].getCell().getColor() == Color.noColor && !step.getNeighbors().contains(graph[i].getCell())) {
                step.getNeighbors().add(graph[i].getCell());
            }

            if (neighbours(step.getCell(), graph[i].getCell()) && graph[i].getCell().getColor() == turn && !step.getNeighbors().contains(graph[i].getCell())) {
                if (!step.getNeighbors().contains(graph[i])) {
                    step.getNeighbors().add(graph[i].getCell());
                }
                addUnique(step, findneighbors(graph[i], turn));
            }

        }
        return step.getNeighbors();

    }

    /**
     * Method that verify if two given Cells "a" and "b" are neibhours
     * @param a First cell needed for the method
     * @param b Second cell needed for the method
     * @return A boolean that shows if the two cells are neighbors
     */
    private static boolean neighbours(Cell a, Cell b) {
        if ((Math.pow(a.getX() - b.getX(), 2) == 1 && Math.pow(a.getY() - b.getY(), 2) == 1 && a.getZ() == b.getZ())) {
            return true;
        } else if (Math.pow(a.getY() - b.getY(), 2) == 1 && Math.pow(a.getZ() - b.getZ(), 2) == 1 && a.getX() == b.getX()) {
            return true;
        } else if (Math.pow(a.getX() - b.getX(), 2) == 1 && Math.pow(a.getZ() - b.getZ(), 2) == 1 && a.getY() == b.getY()) {
            return true;
        } else {
            return false;

        }
    }

    /**
     * Method that link the black ghost edges to the board
     * @param ghost Ghost edge to link
     * @param num Number that identify the raw used to link the ghost edges to the board
     */
    private static void linkBlackGhostEdge(Step ghost, int num) {
        for (int i = 0; i < graph.length; i++) {
            if (graph[i].getCell().getColor() == Color.noColor && graph[i].getCell().getX() == num) {
                if (!ghost.getNeighbors().contains(graph[i].getCell())) {
                    ghost.getNeighbors().add(graph[i].getCell());
                }
            } else if (graph[i].getCell().getColor() == Color.black && graph[i].getCell().getX() == num) {
                for (int j = 0; j < graph[i].getNeighbors().size(); j++) {
                    if (!ghost.getNeighbors().contains(graph[i].getNeighbors().get(j))) {
                        ghost.getNeighbors().add(graph[i].getNeighbors().get(j));
                        // ghost.getNeighbors().add(e)
                    }
                }
            }
        }



        for (int i = 0; i < ghost.getNeighbors().size(); i++) {
            addUnique(findStep(ghost.getNeighbors().get(i)), ghost.getNeighbors());

        }

    }

    /**
     * Method that link the white ghost edges to the board
     * @param ghost Ghost edge to link
     * @param num Number that identify the raw used to link the ghost edges to the board
     */
    private static void linkWhiteGhostEdge(Step ghost, int num) {
        for (int i = 0; i < graph.length; i++) {
            if (graph[i].getCell().getColor() == Color.noColor && graph[i].getCell().getY() == num) {
                if (!ghost.getNeighbors().contains(graph[i].getCell())) {
                    ghost.getNeighbors().add(graph[i].getCell());
                }
            } else if (graph[i].getCell().getColor() == Color.white && graph[i].getCell().getY() == num) {

                ghost.getNeighbors().addAll(graph[i].getNeighbors());

            }
        }


        for (int i = 0; i < ghost.getNeighbors().size(); i++) {
            addUnique(findStep(ghost.getNeighbors().get(i)), ghost.getNeighbors());
        }


    }

    /**
     * Add in the first ArrayList all the elements of the second ArrayList that are already not in it
     * @param tofill ArrayList where i have to add elements
     * @param tostudy ArrayList i use to add elements in the first one
     */
    private static void addUnique(Step tofill, ArrayList<Cell> tostudy) {

        for (int i = 0; i < tostudy.size(); i++) {
            if (!tofill.getNeighbors().contains(tostudy.get(i))) {
                tofill.getNeighbors().add(tostudy.get(i));
            }
        }

    }

    /**
     * Store the value of every cell in a given vector
     * @param values Vector of values to be stored
     * @return The vector of the stored values
     */
    /**
     * Find the next cell to be evaluated in the Dijkstra algorithm
     * @param evaluated Vector fo cells already evaluated
     * @return The next value to be evaluated in the Dijkstra algorithm
     */
    public static Step getNextToEvaluate(ArrayList<Step> evaluated) {
        Step next = new Step(new Cell(Color.white, new Coordinates(0, 0, 0)));
        next.getPossibleWeights().add(next.getColorValue());
        next.getPossibleWeights().add(next.getColorValue());
        for (int i = 0; i < graph.length; i++) {
            if ((!(evaluated.contains(graph[i]))) && graph[i].getPossibleWeights().size() > 1) {

                if (graph[i].getPossibleWeights().get(1) < next.getPossibleWeights().get(1)) {

                    next = graph[i];
                }
            }


        }

        return next;
    }

    /**
     * Delete all the neighbors of every cell in the board
     */
    private static void deleteAllNeighbors() {
        for (int i = 0; i < graph.length; i++) {

            graph[i].deleteNeighbors();

        }
        downBlackEdge.getNeighbors().clear();
        upBlackEdge.getNeighbors().clear();
        downWhiteEdge.getNeighbors().clear();
        upWhiteEdge.getNeighbors().clear();
    }

    /**
     * Satys if the player with the given color won
     * @param color color of the player
     * @return boolean that says if the player won
     */
    public static boolean playerWon(Color color) {
        boolean won = false;
        if (color == Color.black) {
            won = blackWon();
        }
        if (color == Color.white) {
            won = whiteWon();
        }

        return won;
    }

    /**
     * This method discover if the black player won
     * @return True if the player won
     */
    private static boolean blackWon() {
        boolean won = false;
        for (int i = 0; i < upBlackEdge.getNeighbors().size(); i++) {
            if (upBlackEdge.getNeighbors().get(i).getX() == board.getSize() && upBlackEdge.getNeighbors().get(i).getColor() == Color.black) {
                return won = true;
            }


        }
        return won;

    }

    /**
     * This method discover if the white player won
     * @return True if the player won
     */
    private static boolean whiteWon() {
        boolean won = false;
        for (int i = 0; i < upWhiteEdge.getNeighbors().size(); i++) {
            if (upWhiteEdge.getNeighbors().get(i).getY() == 1 && upWhiteEdge.getNeighbors().get(i).getColor() == Color.white) {
                return won = true;
            }

        }
        return won;

    }

    /**
     * Counts all the not colored cells and all the cells with a given color in board
     * @param color color used to count
     * @return the number of the cells matched
     */
    private static int countCells(Color color) {
        int num = 0;

        for (int i = 1; i <= board.getSize(); i++) {
            for (int j = 1; j <= board.getSize(); j++) {
                if (board.getCell(i, j).getColor() == color || board.getCell(i, j).getColor() == Color.noColor) {
                    num++;
                }

            }
        }
        return num;

    }

    /**
     * Extraxt from board all the not colored cells and all the cell that has a given color
     * @param color Color used to extract the cells in board
     * @return A list of particular cell in board
     */
    private static Cell[] exractCells(Color color) {

        Cell[] cell = new Cell[countCells(color)];
        int k = 0;
        for (int i = 1; i <= board.getSize(); i++) {
            for (int j = 1; j <= board.getSize(); j++) {
                if (board.getCell(i, j).getColor() == color || board.getCell(i, j).getColor() == Color.noColor) {
                    cell[k] = board.getCell(i, j);
                    k++;
                }
            }
        }

        return cell;


    }

    /**
     * Get the down black edge
     * @return down black edge
     */
    public static Step getDownBlackEdge() {
        return downBlackEdge;
    }

    /**
     * Get the down white edge
     * @return down white edge
     */
    public static Step getDownWhiteEdge() {
        return downWhiteEdge;
    }

    /**
     * Get the up black edge
     * @return up black edge
     */
    public static Step getUpBlackEdge() {
        return upBlackEdge;
    }

    /**
     * Get the up white edge
     * @return up white edge
     */
    public static Step getUpWhiteEdge() {
        return upWhiteEdge;
    }

    /**
     * 
     * @return
     */
    public static Step[] getGraph() {
        return graph;
    }

    /**
     * 
     * @param cell
     * @return
     */
    public static Step findStep(Cell cell) {
        for (int i = 0; i < graph.length; i++) {
            if (graph[i].getCell().getCoordinates().equals(cell.getCoordinates())) {
                return graph[i];
            }
        }
        return null;

    }
}
