package Algorithms;

import DomainLayer.Models.Board;
import DomainLayer.Models.Coordinates;
import Enumerations.Color;

/**
 * The abstract class for the strategies
 * @author Hex team
 */
public abstract class Strategy {
    
    /**
     * The root of the last computed tree, shared by all the classes of type Strategy
     */
    protected static NodeRoot treeRoot;
    
    /**
     * Apply the strategy by computing the best move according to the strategy
     * for this player on this board
     * @param board the board
     * @param playerColor the color of the player which has to play
     * @param width 
     * @param depth 
     * @return the coordinates where the player is going to play
     */
    public abstract Coordinates computeBestMove(Board board, Color playerColor, 
            int width, int depth);
    
    /**
     * Static getter for the root of the computed tree
     * @return the root of the tree
     */

    public static NodeRoot getTreeRoot() {
        return treeRoot;
    }
}