package DomainLayer.Models;


/**
 * Coordinates class. represents the coordinates of the token in the game
 * @author Hex team
 */
public class Coordinates {
    
    private int x;
    
    private int y;
    
    private int z;

    /**
     * constructor of the class
     * @param x represents the x line
     * @param y represents the y line
     */
    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
        this.z = x+y-1;
    }
    
    /**
     * constructor of the class
     * @param x represents the x line
     * @param y represents the y line
     * @param z represents the z line
     */
    public Coordinates(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    /**
     * constructor of the class
     * @param coords 
     */
    public Coordinates(Coordinates coords) {
        this.x = coords.x;
        this.y = coords.y;
        this.z = coords.z;
    }

    /**
     * gets the x coordinates
     * @return
     */
    public int getX() {
        return x;
    }

    /**
     * sets the x coordinates
     * @param x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * gets the y coordinates
     * @return
     */
    public int getY() {
        return y;
    }

    /**
     * sets the y coordinates
     * @param y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * gets the z coordinates
     * @return
     */
    public int getZ() {
        return z;
    }

    /**
     * sets the z coordinates
     * @param z
     */
    public void setZ(int z) {
        this.z = z;
    }
    
    /**
     * return if the Coorditaces are the same
     *@param coord 
     * @return
     */
    public boolean equals(Coordinates coord)
    {
    	if(x==coord.getX() && y==coord.getY() && z==coord.getZ() )
    		return true;
    	else return false;
    }
}
