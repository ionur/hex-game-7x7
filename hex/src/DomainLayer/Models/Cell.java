package DomainLayer.Models;

import Enumerations.Color;

/**
 * Model class representing one cell of the board 
 * @author Hex team
 */
public class Cell {
    /**Color of the cell */
    private Color color;
    /**Coordinates of the cell */
    private Coordinates coordinates;


    /**
     * Constructor for cell
     * @param color the color of the cell
     * @param coordinates the coordinates of the cell
     */
    public Cell(Color color, Coordinates coordinates) {
        this.color = color;
        this.coordinates = coordinates;
    }
    
    /**
     * Getter for the color of the cell
     * @return the color of the cell
     */
    public Color getColor() {
        return color;
    }

    /**
     * Setter for the color of the cell
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * Getter for the coordinates of the cell
     * @return the coordinates of the sel
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }

    /**
     * Getter for the X coordinate of the cell
     * @return the X coordinate of the cell
     */
    public int getX() {
        return coordinates.getX();
    }

    /**
     * Getter for the Y coordinate of the cell
     * @return the Y coordinate of the cell
     */
    public int getY() {
        return coordinates.getY();
    }

    /**
     * Getter for the Z coordinate of the cell
     * @return the Z coordinate of the cell
     */
    public int getZ() {
        return coordinates.getZ();
    }

    /**
     * Setter for the coordinates of the cell
     * @param coordinates the coordinates of the cell
     */
    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
}
