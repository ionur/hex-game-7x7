package DomainLayer.Models;

import Enumerations.Color;
import Enumerations.Level;
import Enumerations.NextTurnAfter;
import Enumerations.Mode;

/**
 * 
 * @author Hex team
 */
public class Configuration {

    private Mode modeOfGame;
    private boolean undoEnabled;
    private Level level;
    private int playingTimeInMs;
    private NextTurnAfter nexTurnAfter;
    private int boardSize;
    private String player1Name;
    private String player2Name;
    private Color player1Color;


    /*
     * Constructor for the default config
     */
    public Configuration() {
        modeOfGame = Mode.HC;
        undoEnabled = true;
        level = Level.medium;
        playingTimeInMs = 120000;
        nexTurnAfter = NextTurnAfter.immediate;
        boardSize = 7;
        player1Name = "Player1";
        player2Name = "Player2";
        player1Color = Color.white;
    }

    /**
     * Constructor for the class
     * @param modeOfGame is mode of the game
     * @param undoEnabled holds if the undo is enabled
     * @param level the level of the game
     * @param playingTimeInMs holds the playing time of the game
     * @param nexTurnAfter
     * @param boardSize the size of the board
     * @param player1Name player name
     * @param player2Name player name
     * @param player1Color token color of the player
     */
    public Configuration(Mode modeOfGame,
            boolean undoEnabled,
            Level level,
            int playingTimeInMs,
            NextTurnAfter nexTurnAfter,
            int boardSize,
            String player1Name,
            String player2Name,
            Color player1Color) {
        this.modeOfGame = modeOfGame;
        this.undoEnabled = undoEnabled;
        this.level = level;
        if (playingTimeInMs < 0) {
            throw new RuntimeException("Playing time must be bigger than 0");
        }
        this.playingTimeInMs = playingTimeInMs;
        this.nexTurnAfter = nexTurnAfter;
        if (boardSize < 0) {
            throw new RuntimeException("Board size must be bigger than 0");
        }
        this.boardSize = boardSize;
        this.player1Name = player1Name;
        this.player2Name = player2Name;
        this.player1Color = player1Color;
    }

    /**
     * Gets the level of the game
     * @return
     */
    public Level getLevel() {
        return level;
    }

    /**
     * Gets the mode of the game
     * @return
     */
    public Mode getModeOfGame() {
        return modeOfGame;
    }

    /**
     * Get the nextTurnAfter parameter
     * @return
     */
    public NextTurnAfter getNexTurnAfter() {
        return nexTurnAfter;
    }

    /**
     * Gets the playing time of the game in ms
     * @return
     */
    public int getplayingTimeInMs() {
        return playingTimeInMs;
    }

    /**
     * returns if undo is enabled
     * @return
     */
    public boolean isUndoEnabled() {
        return undoEnabled;
    }

    /**
     * set the level of the game
     * @param level
     */
    public void setLevel(Level level) {
        this.level = level;
    }

    /**
     * sets the mode of the game
     * @param modeOfGame
     */
    public void setModeOfGame(Mode modeOfGame) {
        this.modeOfGame = modeOfGame;
    }

    /**
     * 
     * @param nexTurnAfter
     */
    public void setNexTurnAfter(NextTurnAfter nexTurnAfter) {
        this.nexTurnAfter = nexTurnAfter;
    }

    /**
     * sets the playing time in Ms
     * @param playingTimeInMs
     */
    public void setplayingTimeInMs(int playingTimeInMs) {
        if (playingTimeInMs < 0) {
            throw new RuntimeException("Playing time must be bigger than 0");
        }
        this.playingTimeInMs = playingTimeInMs;
    }

    /**
     * sets the undo feature in the game
     * @param undoEnabled
     */
    public void setUndoEnabled(boolean undoEnabled) {
        this.undoEnabled = undoEnabled;
    }

    /**
     * gets the board size 
     * @return
     */
    public int getBoardSize() {
        return boardSize;
    }

    /**
     * set the board size of the game
     * @param boardSize
     */
    public void setBoardSize(int boardSize) {
        if (boardSize < 0) {
            throw new RuntimeException("Board size must be bigger than 0");
        }
        this.boardSize = boardSize;
    }

    /**
     * returns the player 1 name
     * @return
     */
    public String getPlayer1Name() {
        return player1Name;
    }

    /**
     * sets the player1 name
     * @param player1Name
     */
    public void setPlayer1Name(String player1Name) {
        this.player1Name = player1Name;
    }

    /**
     * returns the player2 name
     * @return
     */
    public String getPlayer2Name() {
        return player2Name;
    }

    /**
     * sets the player2 name
     * @param player2Name
     */
    public void setPlayer2Name(String player2Name) {
        this.player2Name = player2Name;
    }

    /**
     * gets the player1 color
     * @return
     */
    public Color getPlayer1Color() {
        return player1Color;
    }

    /**
     * sets the player1 color
     * @param player1Color
     */
    public void setPlayer1Color(Color player1Color) {
        this.player1Color = player1Color;
    }
}