package DomainLayer.Models;

import java.util.Date;

/**
 * Model class representing ranking.
 * @author Hex team
 */
public class Ranking {
    
    /**Score of ranking*/
    private int score;
    /**Name of ranking*/
    private String name;
    /**Date of ranking*/
    private Date date;

    /**
     * Constructor for ranking.
     * @param score The score of ranking.
     * @param name  The name of ranking.
     * @param date  The date of ranking.
     */
    public Ranking(int score, String name, Date date) {

        this.score = score;
        this.name = name;
        this.date = date;
    }

    /**
     * Setter for the score of the ranking.
     * @param score The score to setScore.
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Setter for the name of the ranking.
     * @param name The name to setName.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Setter for the date of the ranking.
     * @param date The date to setDate.
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Getter for the score of the ranking.
     * @return the score of the ranking.
     */
    public int getScore() {
        return score;
    }

    /**
     * Getter for the name of ranking.
     * @return the name of the ranking.
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for the date of ranking.
     * @return the date of the ranking.
     */
    public Date getDate() {
        return date;
    }
}