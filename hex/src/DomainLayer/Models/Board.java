package DomainLayer.Models;

import Enumerations.Color;

/**
 * This class represent the structure of a Board and provide the method to operate on it
 * @author Prestec
 */
public class Board {

    /**size for the cell */
    private int size;
    /**Cells in the board */
    private Cell[][] cells;

    /**
     * Constructor of board
     * @param size Size of the board
     */
    public Board(int size) {
        if (size < 0) {
            throw new RuntimeException("Board size must be bigger than 0");
        }
        this.size = size;

        cells = new Cell[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                cells[i][j] = new Cell(Color.noColor, new Coordinates(i + 1, j + 1));
            }
        }

    }

    /**
     * Getter for the cell
     * @param x
     * @param y
     * @return the cell
     */
    public Cell getCell(int x, int y) {
        return cells[x - 1][y - 1];
    }

    /**
     * Get the cell with the given coordinates
     * @param coords Coordinated of the cell
     * @return The cell corresponding to the coordinates
     */
    public Cell getCell(Coordinates coords) {
        return cells[coords.getX() - 1][coords.getY() - 1];
    }

    /**
     * Get the size of the cell
     * @return The size of the cell
     */
    public int getSize() {
        return size;
    }

    /**
     * Add a token to the board coloring one cell
     * @param color Color of the token
     * @param x Coordinate X of the to color
     * @param y Coordinate Y of the to color
     * @param z Coordinate Z of the to color
     */
    public void addToken(Color color, int x, int y, int z) {
        cells[x - 1][y - 1].setColor(color);
    }

    /**
     * Add a token to the board coloring one cell
     * @param color Color of the token
     * @param coord Coordinates of the token to color
     */
    public void addToken(Color color, Coordinates coord) {
        addToken(color, coord.getX(), coord.getY(), coord.getZ());
    }

    /**
     * Remove a token from the board
     * @param x Coordinate X of the to color
     * @param y Coordinate Y of the to color
     * @param z Coordinate Z of the to color
     */
    public void removeToken(int x, int y, int z) {
        cells[x - 1][y - 1].setColor(Color.noColor);
    }

    /**
     * Remove a token from the board
     * @param coord Coordinates of the token to remove
     */
    public void removeToken(Coordinates coord) {
        removeToken(coord.getX(), coord.getY(), coord.getZ());
    }

    /**
     * Verify if a cell is occupied by a token
     * @param coords Coordinates of the cell
     * @return A boolean that shows if the cell is occupied or not
     */
    public boolean isCellOccupied(Coordinates coords) {
        return (getCell(coords).getColor() != Color.noColor);
    }

    /**
     * Export the color of every cell in the board
     * @return Vector of the cell in the board
     */
    public Color[][] exportColors() {
        Color[][] res = new Color[size][size];

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                res[i][j] = cells[i][j].getColor();
            }
        }

        return res;
    }
    
    /**
     * give a hash number to one board
     * @return the hash number
     */
     @Override
     public int hashCode() {
        String str = this.convert();
        return str.hashCode();
     }
     
     /**
     * convert the board to a String type
     * @return the string converted
     */
     public String convert(){
            String str = "";
            String stradd = "";
            for (int i = 1; i <= size; i++) {
                for (int j = 1; j <= size; j++) {
                    if(this.getCell(i, j).getColor() == Color.noColor){
                        stradd = Integer.toString(0);
                    }
                    if(this.getCell(i, j).getColor() == Color.black){
                        stradd = Integer.toString(1);
                    }
                    if(this.getCell(i, j).getColor() == Color.white){
                        stradd = Integer.toString(2);
                    }
                    str = str.concat(stradd);
                }
            }
        return  str;           
     }
}