package DomainLayer.Models;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class providing a timer
 * @author Hex team
 */
public class Timer {

    private int timeMilliSeconds;
    private javax.swing.Timer timer;

    /**
     * Constructor for Timer, which is a decreasing chronometer
     * @param initTimeMilliSeconds the initial time of the timer
     */
    public Timer(int initTimeMilliSeconds) {
        timeMilliSeconds = initTimeMilliSeconds;
        timer = new javax.swing.Timer(1, new ActionListener() {

            // Method called at each timer tic
            @Override
            public void actionPerformed(ActionEvent event) {
                if (timeMilliSeconds > 0) {
                    timeMilliSeconds--;
                }
            }
        });

    }

    /**
     * Getter for the remaining time in ms
     * @return the remaining time
     */
    public int getTimeMilliSeconds() {
        return timeMilliSeconds;
    }

    /**
     * Starting the chronometer
     */
    public void start() {
        timer.start();
    }

    /**
     * Stoping the chronometer
     */
    public void stop() {
        timer.stop();
    }

    /**
     * To see whether the chronometer is running or not
     * @return true if the chronometer is running
     */
    public boolean isRunning() {
        return timer.isRunning();
    }
}
