package DomainLayer.Models.Test;

import DomainLayer.Models.Ranking;
import Tools.AbstractTestDriver;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * TestDriver for Ranking class.
 * @author Hex team
 */
public class RankingTestDriver extends AbstractTestDriver {

    /**
     * Driver
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        Date date = null;
        Ranking ranking = null;


        while (true) {
            try {
                String[] cmd = getNewCmd();
                int i = 0;


                i = Integer.parseInt(cmd[0]);


                switch (i) {
                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;
                    case 1:
                        ranking = new Ranking(Integer.parseInt(cmd[1]), cmd[2], date);
                        print(ranking);
                        break;
                    case 2:
                        ranking.setScore(Integer.parseInt(cmd[1]));
                        print("");
                        break;
                    case 3:
                        ranking.setName(cmd[1]);
                        print("");
                        break;
                    case 4:
                        ranking.setDate(date);
                        print("");
                        break;
                    case 5:
                        print(ranking.getScore());
                        break;
                    case 6:
                        print(ranking.getName());
                        break;
                    case 7:
                        print(ranking.getDate());
                        break;
                    case 8:
                        
                        date = new GregorianCalendar(Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]),
                                Integer.parseInt(cmd[4]),
                                Integer.parseInt(cmd[5]),
                                Integer.parseInt(cmd[6])).getTime();
                        print(date);
                        break;
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class Ranking ########");
        print("1 attr1 attr2 -> Ranking(int score, String name, Date date)");
        print("2 attr1 -> void setScore(int score)");
        print("3 attr1 -> void setName(String name)");
        print("4 -> void setDate(Date date)");
        print("5 -> int getScore()");
        print("6 -> String getName()");
        print("7 -> Date getDate()");

        print("\n######## Tools to create parameters ########");
        print("WARNING: Objects are null by default and lists are empty");
        print("8 attr1 attr2 attr3 attr4 attr5 attr6 -> chooseDate(int year, int month, int date, int hour, int minute, int second) ");

    }
}
