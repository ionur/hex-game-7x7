package DomainLayer.Models.Test;

import Enumerations.Color;
import DomainLayer.Models.Cell;
import DomainLayer.Models.Coordinates;
import Tools.AbstractTestDriver;
import java.util.ArrayList;

/**
 * TestDriver for Cell class
 * @author Hex team
 */
public class CellTestDriver extends AbstractTestDriver {

    /**
     * Driver
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        Cell cell = null;
        Color color = null;
        Coordinates coordinates = null;
        ArrayList<Cell> neighbors = new ArrayList<Cell>();
        Cell neighbor = null;
        ArrayList<Integer> possibleWeights = new ArrayList<Integer>();
        String savedCell = null;

        while (true) {
            try {
                String[] cmd = getNewCmd();
                int i = 0;


                i = Integer.parseInt(cmd[0]);


                switch (i) {
                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;

                    //here are the call to the methods
                    case 1:
                        cell = new Cell(Color.valueOf(cmd[1]), new Coordinates(
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3])));
                        print(cell);
                        break;
                    case 2:
                        print(cell.getColor());
                        break;
                    case 3:
                        cell.setColor(Color.valueOf(cmd[1]));
                        print("");
                        break;
                    case 4:
                        print(cell.getCoordinates());
                        break;
                    case 5:
                        print(cell.getX());
                        break;
                    case 6:
                        print(cell.getY());
                        break;
                    case 7:
                        print(cell.getZ());
                        break;
                    case 8:
                        cell.setCoordinates(new Coordinates(
                                Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2])));
                        print("");
                        break;
                    //here is the parameters creation
                    case 9:
                        coordinates = new Coordinates(
                                Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]));
                        print("Coordinates are now: " + coordinates);
                        break;    

                    //default value
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class Cell ########");
        print("1 attr1 attr2 attr3-> Cell(Color color, Coordinates coordinates)");
        print("2 -> Color getColor()");
        print("3 attr1 -> SetColor(Color)");
        print("4 -> Coordinates GetCoordinates()");
        print("5 -> int getX()");
        print("6 -> int getY()");
        print("7 -> int getZ()");
        print("8 attr1 attr2 -> SetCoordinates(Coordinates)");
        print("");
    }
}
