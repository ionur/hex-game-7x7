package DomainLayer.Models.Test;

import DomainLayer.Models.Coordinates;
import Tools.AbstractTestDriver;

/**
 * 
 * @author Hex team
 */
public class CoordinatesTestDriver extends AbstractTestDriver {

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        Coordinates coordinates = null;
        Coordinates coord = null;

        while (true) {
            try {

                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);

                switch (i) {

                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;

                    case 1:
                        coordinates = new Coordinates(
                                Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]));
                        break;

                    case 2:
                        coordinates = new Coordinates(
                                Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]));
                        break;
                    case 3:
                        coord = new Coordinates(coordinates);
                        break;
                    case 4:
                        print(coordinates.getX());
                        break;
                    case 5:
                        print(coordinates.getY());
                        break;
                    case 6:
                        print(coordinates.getZ());
                        break;
                    case 7:
                        coordinates.setX(Integer.parseInt(cmd[1]));
                        print("");
                        break;
                    case 8:
                        coordinates.setY(Integer.parseInt(cmd[1]));
                        print("");
                        break;
                    case 9:
                        coordinates.setZ(Integer.parseInt(cmd[1]));
                        print("");
                        break;
                    case 10:
                        print(coordinates.equals(new Coordinates(
                                Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]))));
                        break;
                        
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {

        print("\n######## Methods of the class Coordinates ########");
        print("1 attr1 attr2 -> Coordinates(int x, int y)");
        print("2 attr1 attr2 attr3 -> Coordinates(int x, int y, int z)");
        print("3 -> Coordinates(Coordinates coordinates)");
        print("4 -> int getX()");
        print("5 -> int getY()");
        print("6 -> int getZ()");
        print("7 attr1 -> void setX(int x)");
        print("8 attr1 -> void setY(int y)");
        print("9 attr1 -> void setZ(int z)");
        print("10 attr1 attr2 attr3 -> equals(Coordinates coord)");
        print("");
    }
}
