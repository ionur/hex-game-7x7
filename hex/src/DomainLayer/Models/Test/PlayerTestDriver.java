package DomainLayer.Models.Test;

import Algorithms.AlphaBeta;
import Algorithms.MonteCarlo;
import Algorithms.Strategy;
import DomainLayer.Models.Board;
import DomainLayer.Models.Coordinates;
import DomainLayer.Models.Player;
import Enumerations.Color;
import Tools.AbstractTestDriver;

/**
 * 
 * @author Hex team
 */
public class PlayerTestDriver extends AbstractTestDriver {

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        Player player = null;
        Board board = null;

        while (true) {
            try {

                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);


                switch (i) {

                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;

                    case 1:
                        Strategy strategy;
                        if (Integer.valueOf(cmd[5]) == 1) {
                            strategy = new AlphaBeta();
                        } else {
                            strategy = new MonteCarlo();
                        }

                        player = new Player(Color.valueOf(cmd[1]),
                                Integer.valueOf(cmd[2]),
                                Integer.valueOf(cmd[3]),
                                Boolean.parseBoolean(cmd[4]),
                                strategy);
                        print("");
                        break;

                    case 2:
                        print(player.getColor());
                        break;

                    case 3:
                        player.setColor(Color.valueOf(cmd[1]));
                        print("");
                        break;
                    case 4:
                        print(player.getScore());
                        break;
                    case 5:
                        player.setScore(Integer.valueOf(cmd[1]));
                        print("");
                        break;
                    case 6:
                        print(player.getTimeRemaining());
                        break;
                    case 7:
                        player.startThinking();
                        print("");
                        break;
                    case 8:
                        player.stopThinking();
                        break;
                    case 9:
                        print(player.isThinking());
                        break;
                    case 10:
                        print(player.isComputer());
                        break;
                    case 11:
                        player.setIsComputer(Boolean.valueOf(cmd[1]));
                        print("");
                        break;
                    case 12:
                        Strategy strategy2;
                        if (Integer.valueOf(cmd[1]) == 1) {
                            strategy2 = new AlphaBeta();
                        } else {
                            strategy2 = new MonteCarlo();
                        }
                        player.setStrategy(strategy2);
                        print("");
                        break;
                    case 13:
                        Coordinates coords = player.applyStrategy(board,
                                Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]));
                        print("(" + coords.getX() + "," + coords.getY() + ")");
                        break;
                    case 14:
                        print(player.isTimeOver());
                        break;
                    case 15:
                        board = new Board(Integer.valueOf(cmd[1]));
                        print("Board created");
                        break;
                    case 16:
                        board.addToken(Color.valueOf(cmd[1]),
                                new Coordinates(Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3])));
                        print("Token added");
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {

        print("\n######## Methods of the class Player ########");
        print("1 attr1 attr2 attr3 attr4 attr5 -> Player(Color color, int score, int timeRemaining, boolean isComputer, Strategy strategy)"
                + "\n \tattr5 is 1 for AlphaBeta, something else for MonteCarlo");
        print("2 -> getColor()");
        print("3 attr1 -> setColor(Color color)");
        print("4 -> getScore()");
        print("5 attr1 -> setScore(int x)");
        print("6 -> getTimeRemaining()");
        print("7 -> startThinking()");
        print("8 -> stopThinking()");
        print("9 -> isThinking()");
        print("10 -> isComputer()");
        print("11 attr1 -> setIsComputer(Boolean isComputer)");
        print("12 attr1 -> setStrategy()"
                + "\n \tattr1 is 1 for AlphaBeta, something else for MonteCarlo");
        print("13 attr1 attr2 -> applyStrategy(Board board,int width, int depth)");
        print("14 -> isTimeOver()");
        print("\n######## Tools to create parameters ########");
        print("15 attr1 -> Board(int)");
        print("16 attr1 attr2 attr3 -> addAToken(Color color, int x, int y)");
        print("");
    }
}
