package DomainLayer.Models.Test;

import Tools.AbstractTestDriver;
import DomainLayer.Models.Board;
import DomainLayer.Models.Cell;
import DomainLayer.Models.Coordinates;
import Enumerations.Color;

/**
 * 
 * @author Hex team
 */
public class BoardTestDriver extends AbstractTestDriver {

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        Board board = null;


        while (true) {
            try {

                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);


                switch (i) {

                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;

                    case 1:
                        board = new Board(Integer.valueOf(cmd[1]));
                        print("");
                        break;
                    case 2:
                        Cell a = board.getCell(Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]));
                        print(a.getColor() + " " + a.getX() + " " + a.getY());
                        break;
                    case 3:
                        Cell c = board.getCell(new Coordinates(Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3])));
                        print(c.getColor().toString() + " " + c.getX() + " " + c.getY() + " " + c.getZ());
                        break;
                    case 4:
                        print(board.getSize());
                        break;
                    case 5:
                        board.addToken(Color.valueOf(cmd[1]), Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]),
                                Integer.parseInt(cmd[4]));
                        print("token added");
                        break;
                    case 6:
                        board.addToken(Color.valueOf(cmd[1]), new Coordinates(Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]),
                                Integer.parseInt(cmd[4])));
                        print("token added");
                        break;
                    case 7:
                        board.removeToken(Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]));
                        print("token removed");
                        break;
                    case 8:
                        board.removeToken(new Coordinates(Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3])));
                        print("token removed");
                        break;
                    case 9:
                        boolean result = board.isCellOccupied(new Coordinates(Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3])));
                        print(result);
                        break;
                    case 10:
                        Color[][] exportColors = board.exportColors();
                        for (Color[] array : exportColors) {
                            for (Color y : array) {
                                System.out.print(y + " ");
                            }
                            print("");
                        }
                        break;

                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {

        print("\n######## Methods of the class Board ########");
        print("1 attr1 -> Board(int)");
        print("2 attr1 attr2 -> getCell(int x, int y)");
        print("3 attr1 attr2 attr3 -> getCell(int x, int y, int z)");
        print("4 -> getSize()");
        print("5 attr1 attr2 attr3 attr4 -> addToken(Color color, int x, int y, int z)");
        print("6 attr1 attr2 attr3 attr4 -> addToken(Color color, Coordinates coords)"
                + "attr2 attr3 attr4 are representing the coords values");
        print("7 attr1 attr2 attr3 -> removeToken(int x, int y, int z)");
        print("8 attr1 attr2 attr3 -> removeToken(Coordinates coords)"
                + "attr1 attr2 attr3 are representing the coords values");
        print("9 attr1 attr2 attr3 -> isCellOccupied(Coordinates coords)"
                + "attr1 attr2 attr3 are representing the coords values");
        print("10 -> exportColors()");
        print("");



    }
}
