package DomainLayer.Models.Test;

import DomainLayer.Models.Timer;
import Tools.AbstractTestDriver;

/**
 * TestDriver for Timer class
 * @author Hex team
 */
public class TimerTestDriver extends AbstractTestDriver {

    /**
     * Driver
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        Timer timer = null;


        while (true) {
            try {
                String[] cmd = getNewCmd();
                int i = 0;


                i = Integer.parseInt(cmd[0]);


                switch (i) {
                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;

                    //here are the call to the methods
                    case 1:
                        timer = new Timer(Integer.parseInt(cmd[1]));
                        print(timer);
                        break;
                    case 2:
                        print(timer.getTimeMilliSeconds());
                        break;
                    case 3:
                        timer.start();
                        print("started");
                        break;
                    case 4:
                        timer.stop();
                        print("stoped");
                        break;
                    case 5:
                        print(timer.isRunning());
                        break;
                    case 6:
                        print("Sleeping for a wile");
                        Thread.sleep(Integer.parseInt(cmd[1]));
                        break;

                    //default value
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class Timer ########");
        print("1 attr1 -> Timer(int initTimeMilliSeconds)");
        print("2 -> int getTimeMilliSeconds()");
        print("3 -> void start()");
        print("4 -> void stop()");
        print("5 -> boolean isRunning()");
        print("\n######## For testing by command file ########");
        print("6 attr1 -> waitFor(int timeInMs)");
        print("");
    }
}
