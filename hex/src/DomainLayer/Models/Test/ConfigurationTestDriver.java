package DomainLayer.Models.Test;

import Enumerations.Color;
import Enumerations.NextTurnAfter;
import Enumerations.Level;
import Enumerations.Mode;
import DomainLayer.Models.Configuration;
import Tools.AbstractTestDriver;

/**
 * 
 * @author Hex team
 */
public class ConfigurationTestDriver extends AbstractTestDriver {

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        Configuration config = null;


        while (true) {
            try {

                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);


                switch (i) {

                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;

                    case 1:
                        config = new Configuration();
                        print("");
                        break;

                    case 2:
                        config = new Configuration(Mode.valueOf(cmd[1]),
                                Boolean.parseBoolean(cmd[2]),
                                Level.valueOf(cmd[3]),
                                Integer.parseInt(cmd[4]),
                                NextTurnAfter.valueOf(cmd[5]),
                                Integer.parseInt(cmd[6]),
                                cmd[7],
                                cmd[8],
                                Color.valueOf(cmd[9]));
                        print(config);
                        break;

                    case 3:
                        config.setUndoEnabled(Boolean.parseBoolean(cmd[1]));
                        print("");
                        break;
                    case 4:
                        config.setplayingTimeInMs(Integer.parseInt(cmd[1]));
                        print("");
                        break;
                    case 5:
                        config.setNexTurnAfter(NextTurnAfter.valueOf(cmd[1]));
                        print("");
                        break;
                    case 6:
                        config.setLevel(Level.valueOf(cmd[1]));
                        print("");
                        break;
                    case 7:
                        config.setModeOfGame(Mode.valueOf(cmd[1]));
                        print("");
                        break;
                    case 8:
                        config.setBoardSize(Integer.parseInt(cmd[1]));
                        print("");
                        break;
                    case 9:
                        config.setPlayer1Name(String.valueOf(cmd[1]));
                        print("");
                        break;
                    case 10:
                        config.setPlayer2Name(String.valueOf(cmd[1]));
                        print("");
                        break;
                    case 11:
                        config.setPlayer1Color(Color.valueOf(cmd[1]));
                        print("");
                        break;
                    case 12:
                        print(config.isUndoEnabled());
                        break;
                    case 13:
                        print(config.getplayingTimeInMs());
                        print("");
                        break;
                    case 14:
                        print(config.getNexTurnAfter());
                        print("");
                        break;
                    case 15:
                        print(config.getLevel());
                        print("");
                        break;
                    case 16:
                        print(config.getModeOfGame());
                        print("");
                        break;
                    case 17:
                        print(config.getBoardSize());
                        print("");
                        break;
                    case 18:
                        print(config.getPlayer1Name());
                        print("");
                        break;
                    case 19:
                        print(config.getPlayer2Name());
                        print("");
                        break;
                    case 20:
                        print(config.getPlayer1Color());
                        print("");
                        break;         

                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        //!!!!! to correct !!!!!!

        print("\n######## Methods of the class Configuration ########");
        print("1 -> Configuration()");
        print("2 attr1 attr2 attr3 attr4 attr5 attr6 attr7 attr8 attr9-> Configuration(Mode modeOfGame, boolean undoEnabled, Level level, int playingTime, NextTurnAfter nexTurnAfter, int boardSize, String player1Name, String player2Name, Color player1Color)");
        print("3 attr1 -> setUndo(Boolean)");
        print("4 attr1 -> setPlayingTime(int)");
        print("5 attr1 -> NextTurnAfter(NextTurnAfter)");
        print("6 attr1 -> setLevel(Level)");
        print("7 attr1 -> setMode(Mode)");
        print("8 attr1 -> setBoardSize(int)");
        print("9 attr1 -> setPlayer1Name(string)");
        print("10 attr1 -> setPlayer2Name(string)");
        print("11 attr1 -> setPlayer1Color(Color)");
        print("12 -> isUndoEnabled()");
        print("13 -> getPlayingTime()");
        print("14 -> getNextTurnAfter()");
        print("15 -> getLevel()");
        print("16 -> getModeOfGame()");
        print("17 -> getBoardSize()");
        print("18 -> getPlayer1Name()");
        print("19 -> getPlayer2Name()");
        print("20 -> getPlayer1Color()");
        print("");
    }
}
