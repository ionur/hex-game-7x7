package DomainLayer.Controlers.Test;

import DomainLayer.Controlers.ConfigControler;
import Enumerations.Color;
import Enumerations.Level;
import Enumerations.Mode;
import Enumerations.NextTurnAfter;
import Tools.AbstractTestDriver;

/**
 * 
 * @author Hex team
 */
public class ConfigControlerTestDriver extends AbstractTestDriver {

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        ConfigControler controler = null;
        String saveConfig = "";


        while (true) {
            try {

                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);


                switch (i) {

                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;

                    case 1:
                        controler = new ConfigControler();
                        print(controler);
                        break;

                    case 2:
                        controler = new ConfigControler(Mode.valueOf(cmd[1]),
                                Boolean.parseBoolean(cmd[2]),
                                Level.valueOf(cmd[3]),
                                Integer.parseInt(cmd[4]),
                                NextTurnAfter.valueOf(cmd[5]),
                                Integer.parseInt(cmd[6]),
                                cmd[7],
                                cmd[8],
                                Color.valueOf(cmd[9]));
                        print(controler);
                        break;

                    case 3:
                        controler.setUndo(Boolean.parseBoolean(cmd[1]));
                        print("");
                        break;
                    case 4:
                        controler.setplayingTimeInMs(Integer.parseInt(cmd[1]));
                        print("");
                        break;
                    case 5:
                        controler.setNextTurnAfter(NextTurnAfter.valueOf(cmd[1]));
                        print("");
                        break;
                    case 6:
                        controler.setLevel(Level.valueOf(cmd[1]));
                        print("");
                        break;
                    case 7:
                        controler.setMode(Mode.valueOf(cmd[1]));
                        print("");
                        break;
                    case 8:
                        controler.setBoardSize(Integer.parseInt(cmd[1]));
                        print("");
                        break;
                    case 9:
                        controler.setPlayer1Name(String.valueOf(cmd[1]));
                        print("");
                        break;

                    case 10:
                        controler.setPlayer2Name(cmd[1]);
                        print("");
                        break;
                    case 11:
                        controler.setPlayer1Color(Color.valueOf(cmd[1]));
                        print("");
                        break;
                    case 12:
                        print(controler.isUndoEnabled());
                        break;
                    case 13:
                        print(controler.getplayingTimeInMs());
                        break;
                    case 14:
                        print(controler.getNextTurnAfter());
                        break;
                    case 15:
                        print(controler.getLevel());
                        break;
                    case 16:
                        print(controler.getMode());
                        break;
                    case 17:
                        print(controler.getBoardSize());
                        break;
                    case 18:
                        print(controler.getPlayer1Name());
                        break;
                    case 19:
                        print(controler.getPlayer2Name());
                        break;
                    case 20:
                        print(controler.getPlayer1Color());
                        break;
                    case 21:
                        saveConfig = controler.saveConfigAsString();
                        print(saveConfig);
                        break;
                    case 22:
                        print(controler.setConfigFromString(saveConfig));
                        break;
                    


                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {

        print("\n######## Methods of the class ConfigControler ########");
        print("1 -> ConfigControler()");
        print("2 attr1 ... attr9 -> ConfigControler(Mode modeOfGame, "
                + "boolean undoEnabled, Level level, int playingTime, "
                + "NextTurnAfter nexTurnAfter, int boardSize, "
                + "String player1Name, String player2Name, Color player1Color)"
                + "\n\t attr1 **must be** HH or HC or CC"
                + "\n\t attr2 **must be** true or false"
                + "\n\t attr3 **must be** easy or medium or difficult or expert"
                + "\n\t attr5 **must be** clickFromUser or twoSecondsWait or "
                + "fiveSecondsWait or tenSecondsWait"
                + "\n\t attr9 **must be** white or black");
        print("3 attr1 -> void setUndo(Boolean)");
        print("4 attr1 -> void setPlayingTime(int)");
        print("5 attr1 -> void setNextTurnAfter(NextTurnAfter)");
        print("6 attr1 -> void setLevel(Level)");
        print("7 attr1 -> void setMode(Mode)");
        print("8 attr1 -> void setBoardSize(int)");
        print("9 attr1 -> void setPlayer1Name(string)");
        print("10 attr1 -> void setPlayer2Name(string)");
        print("11 attr1 -> void setPlayer1Color(Color)");
        print("12 -> boolean isUndoEnabled()");
        print("13 -> int getPlayingTime()");
        print("14 -> NextTurnAfter getNextTurnAfter()");
        print("15 -> Level getLevel()");
        print("16 -> Mode getMode()");
        print("17 -> int getBoardSize()");
        print("18 -> String getPlayer1Name()");
        print("19 -> String getPlayer2Name()");
        print("20 -> Color getPlayer1Color()");
        print("21 -> String saveConfigAsString()");
        print("22 -> boolean setConfigFromString(String string)");
        print("");
    }
}
