package DomainLayer.Controlers.Test;

import Enumerations.TypeOfRanking;
import DomainLayer.Controlers.RankingControler;
import Tools.AbstractTestDriver;

/**
 * TestDriver for RankingControler class
 * @author Hex team
 */
public class RankingControlerTestDriver extends AbstractTestDriver {
    
    /**
     * Driver
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        RankingControler controler = null;
       

        while (true) {
            try {
                String[] cmd = getNewCmd();
                int i = 0;


                i = Integer.parseInt(cmd[0]);


                switch (i) {
                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;
                    case 1:
                        controler = new RankingControler();
                        print(controler);
                        break;
                    case 2:
                        print(controler.getRankingsSize()); 
                        break;
                    case 3:
                        print(controler.handleNewScore(TypeOfRanking.valueOf(cmd[1]), Integer.parseInt(cmd[2]), cmd[3]));
                        break;
                    case 4:
                        Object[][] res = controler.exportData(TypeOfRanking.valueOf(cmd[1]));
                        for (Object[] array : res) {
                            for (Object o : array) {
                                System.out.print(o+" ");
                            }
                            print("");
                        }
                        break;
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class RankingControler ########");
        print("1 -> RankingControler()");
        print("2 -> int getRankingsSize()");
        print("3 attr1 attr2 attr3 -> boolean handleNewScore(TypeOfRanking rankingType, int score, String name"
                        + "\t attr1 **must be** HCeasy or HCmedium or HCdifficult or HCexpert or HH");
        print("4 attr1 -> Object[][] exportData(TypeOfRanking rankingType)");
        print("");
    }
}