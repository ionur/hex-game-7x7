/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DomainLayer.Controlers.Test;

import DomainLayer.Controlers.GameControler;
import Enumerations.Color;
import Enumerations.Level;
import Enumerations.Mode;
import Enumerations.NextTurnAfter;
import Tools.AbstractTestDriver;

/**
 *
 * @author Hex team
 */
public class GameControlerTestDriver extends AbstractTestDriver {

    /**
     * Driver
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        GameControler controler = null;

        while (true) {
            try {
                String[] cmd = getNewCmd();
                int i = 0;


                i = Integer.parseInt(cmd[0]);


                switch (i) {
                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;

                    //here are the call to the methods
                    case 1:
                        controler = new GameControler();
                        print(controler);
                        break;
                    case 5:
                        controler.createGame();
                        print("");
                        break;
                    case 6:
                        controler.restartSameGame();
                        print("");
                        break;
                    case 7:
                        print(controler.saveGame(System.getProperty("user.dir")
                                +System.getProperty("file.separator")+cmd[1]));
                        break;
                    case 8:
                        print(controler.loadGame(System.getProperty("user.dir")
                                +System.getProperty("file.separator")+cmd[1]));
                        break;
                    case 10:
                        print(GameControler.getConfigControler());
                        break;
                    case 11:
                        print(GameControler.getPlayControler());
                        break;
                    case 12:
                        print(controler.toString());
                        break;


                    //here is the parameters creation
                    case 13:
                        GameControler.getConfigControler().setUndo(Boolean.parseBoolean(cmd[1]));
                        print("changed.");
                        break;
                    case 14:
                        GameControler.getConfigControler().setplayingTimeInMs(Integer.parseInt(cmd[1]));
                        print("changed.");
                        break;
                    case 15:
                        GameControler.getConfigControler().setNextTurnAfter(NextTurnAfter.valueOf(cmd[1]));
                        print("changed.");
                        break;
                    case 16:
                        GameControler.getConfigControler().setLevel(Level.valueOf(cmd[1]));
                        print("changed.");
                        break;
                    case 17:
                        GameControler.getConfigControler().setMode(Mode.valueOf(cmd[1]));
                        print("changed.");
                        break;
                    case 18:
                        GameControler.getConfigControler().setBoardSize(Integer.parseInt(cmd[1]));
                        print("changed.");
                        break;
                    case 19:
                        GameControler.getConfigControler().setPlayer1Name(cmd[1]);
                        print("changed.");
                        break;
                    case 20:
                        GameControler.getConfigControler().setPlayer2Name(cmd[1]);
                        print("changed.");
                        break;
                    case 21:
                        GameControler.getConfigControler().setPlayer1Color(Color.valueOf(cmd[1]));
                        print("changed.");
                        break;
                    case 22:
                        int[] res = GameControler.getPlayControler().startNextTurn();
                        if (res == null) {
                            print("Current player is human, please call now 23");
                        } else {
                            print("Computer played ("+res[0]+","+res[1]+") : end of the turn");
                        }
                        break;
                    case 23:
                        int[] selection = {Integer.parseInt(cmd[1]),
                            Integer.parseInt(cmd[2])};
                        boolean correct = GameControler.getPlayControler().playHumanMove(selection);
                        print(correct);
                        if (correct) {
                            print("Player played, end of the turn");
                        } else {
                            print("Incorrect move, please call 23 again");
                        }
                        break;


                    //default value
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class GameControler ########");
        print("1 -> GameControler()");
        print("5 -> void createGame()");
        print("6 -> void restartSameGame()");
        print("7 attr1 -> boolean saveGame(String name)"
                + "\t attr1 **must be** without spaces");
        print("8 attr1 -> boolean loadGame(String name)"
                + "\t attr1 **must be** player1 or player2");
        print("10 -> ConfigControler getConfigControler()");
        print("11 -> PlayControler getPlayControler()");
        print("12 -> String toString()");
        print("13 attr1 -> setUndo(Boolean)");
        print("14 attr1 -> setPlayingTime(int)");
        print("15 attr1 -> NextTurnAfter(NextTurnAfter)");
        print("16 attr1 -> setLevel(Level)");
        print("17 attr1 -> setMode(Mode)");
        print("18 attr1 -> setBoardSize(int)");
        print("19 attr1 -> setPlayer1Name(string)");
        print("20 attr1 -> setPlayer2Name(string)");
        print("21 attr1 -> setPlayer1Color(Color)");
        print("22 -> startNextTurn()");
        print("23 attr1 attr2 -> playHumanMove(int x, int y)");
        print("");
    }
}