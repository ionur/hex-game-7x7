/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DomainLayer.Controlers.Test;

import Algorithms.AlphaBeta;
import Algorithms.MonteCarlo;
import Algorithms.Strategy;
import DomainLayer.Controlers.TreeControler;
import DomainLayer.Models.Board;
import DomainLayer.Models.Coordinates;
import DomainLayer.Models.Player;
import Enumerations.Color;
import Tools.AbstractTestDriver;

/**
 * to test the class of TreeControler.java
 * @author hex team
 */
public class TreeControlerTestDriver extends AbstractTestDriver {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        Player player = null;
        Board board = null;
        AlphaBeta ab = null;
        MonteCarlo mc = null;

        displayWelcomeMessage();
        while (true) {
            try {

                String[] cmd = getNewCmd();
                int i = 0;
                i = Integer.parseInt(cmd[0]);

                switch (i) {

                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;
                    case 2:
                        print(TreeControler.getLastComputedTree());
                        break;
                    case 3:
                        Strategy strategy;
                        if (Integer.valueOf(cmd[5]) == 1) {
                            strategy = new AlphaBeta();
                        } else {
                            strategy = new MonteCarlo();
                        }

                        player = new Player(Color.valueOf(cmd[1]),
                                Integer.valueOf(cmd[2]),
                                Integer.valueOf(cmd[3]),
                                Boolean.parseBoolean(cmd[4]),
                                strategy);
                        print("");
                        break;

                    case 4:
                        board = new Board(Integer.parseInt(cmd[1]));
                        print("Board created");
                        break;
                    case 5:
                        board.addToken(Color.valueOf(cmd[1]),
                                new Coordinates(Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3])));
                        print("token added");
                        break;

                    case 6:
                        Coordinates coords = player.applyStrategy(board,
                                Integer.parseInt(cmd[1]),
                                Integer.parseInt(cmd[2]));
                        print("(" + coords.getX() + "," + coords.getY() + ")");
                        break;

                }
            } catch (Exception ex) {
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class TreeControler ########");
        print("2 -> ArrayList<int[]> getLastComputedTree()");
        print("3 attr1 attr2 attr3 attr4 attr5 -> void changeCurrentPlayer(Color color, int score, int timeRemaining, boolean isComputer, Strategy strategy)"
                + "\n \tattr5 is 1 for AlphaBeta, something else for MonteCarlo");
        print("4 attr1 -> void createBoard(int size)");
        print("5 attr1 attr2 atttr3 -> void addToken(Color color, int x, int y)");
        print("6 attr1 attr2 applyPlayerStrategy(int widht, int depth)");

        print("");


    }
}