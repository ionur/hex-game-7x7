package DomainLayer.Controlers.Test;

import Algorithms.AlphaBeta;
import Algorithms.MonteCarlo;
import Enumerations.Color;
import DomainLayer.Controlers.ConfigControler;
import DomainLayer.Controlers.PlayControler;
import DomainLayer.Models.Board;
import DomainLayer.Models.Coordinates;
import DomainLayer.Models.Player;
import Enumerations.PlayerNumber;
import Tools.AbstractTestDriver;
import java.util.Stack;

/**
 * Test Driver for PlayControler class
 * @author Hex team
 */
public class PlayControlerTestDriver extends AbstractTestDriver {

    /**
     * Driver
     * @param args
     */
    public static void main(String[] args) {

        displayWelcomeMessage();

        PlayControler controler = null;
        Player player1 = null;
        Player player2 = null;
        ConfigControler configControler = new ConfigControler();
        Board board = new Board(configControler.getBoardSize());
        Stack<Coordinates> oldMoves = new Stack<Coordinates>();

        String currPlayer = null;

        while (true) {
            try {
                String[] cmd = getNewCmd();
                int i = 0;


                i = Integer.parseInt(cmd[0]);


                switch (i) {
                    case -1:
                        displayQuitting();
                        return;
                    case 0:
                        displayHelp();
                        break;
                    case 1:
                        controler = new PlayControler(player1, player2, board, configControler);
                        print("");
                        break;
                    case 2:
                        currPlayer = controler.getCurrentPlayerAsString();
                        print(currPlayer);
                        break;
                    case 3:
                        print(controler.setCurrentPlayerFromString(currPlayer));
                        break;
                    case 4:
                        int[] res = controler.startNextTurn();
                        if (res == null) {
                            print("Current player is human, please call now 23");
                        } else {
                            print("Computer played ("+res[0]+","+res[1]+") : end of the turn");
                        }
                        break;
                    case 5:
                        int[] selection = {Integer.parseInt(cmd[1]),
                            Integer.parseInt(cmd[2])};
                        boolean correct = controler.playHumanMove(selection);
                        print(correct);
                        if (correct) {
                            print("Player played, end of the turn");
                        } else {
                            print("Incorrect move, please call 5 again");
                        }
                        break;
                    case 6:
                        int[] hint = controler.giveAHint();
                        for (int j : hint) {
                            System.out.print(j + " ");
                        }
                        break;
                    case 7:
                        controler.undo(false);
                        print("");
                        break;
                    case 8:
                        controler.pauseGame();
                        print("");
                        break;
                    case 9:
                        controler.continueGame();
                        print("");
                        break;
                    case 10:
                        print(controler.isEndOfGame());
                        break;
                    case 11:
                        int[] selection2 = {Integer.parseInt(cmd[1]),
                            Integer.parseInt(cmd[2])};
                        print(controler.defineSituation(selection2));
                        break;
                    case 12:
                        Color[][] col = controler.getBoard();
                        if (col == null) {
                            print("null");
                        }
                        for (Color[] cols : col) {
                            for (Color c : cols) {
                                System.out.print(c.toString()+" ");
                            }
                            print("");
                        }
                        break;
                    case 13:
                        Stack<Coordinates> stack = controler.getOldMoves();
                        int size = stack.size();
                        for (int j = 0; j < size; ++j) {
                            Coordinates c = stack.get(size - j - 1);
                            print("("+c.getX() + "," + c.getY() + ")");
                        }
                        print("");
                        break;
                    case 14:
                        controler.setOldMoves(oldMoves);
                        print("");
                        break;
                    case 21:
                        print(controler.getBoardSize());
                        break;
                    case 22:
                        print(controler.getTime(PlayerNumber.valueOf(cmd[1])));
                        break;
                    case 23:
                        print(controler.getScore(PlayerNumber.valueOf(cmd[1])));
                        break;
                    case 24:
                        print(controler.getCurrentPlayerColor());
                        break;
                        
                    //here is the parameters creation
                    case 15:
                        player1 = new Player(Color.valueOf(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]),
                                Boolean.valueOf(cmd[4]),
                                new AlphaBeta());
                        print("changed.");
                        break;
                    case 16:
                        player2 = new Player(Color.valueOf(cmd[1]),
                                Integer.parseInt(cmd[2]),
                                Integer.parseInt(cmd[3]),
                                Boolean.valueOf(cmd[4]),
                                new MonteCarlo());
                        print("changed.");
                        break;
                    case 17:
                        configControler.setUndo(Boolean.parseBoolean(cmd[1]));
                        print("changed.");
                        break;
                    case 18:
                        board = new Board(configControler.getBoardSize());
                        print("board created.");
                        break;
                    case 19:
                        oldMoves.push(new Coordinates(Integer.parseInt(cmd[1]), 
                                Integer.parseInt(cmd[2])));
                        print("move added.");
                        break;
                    case 20:
                        oldMoves.clear();
                        print("stack cleared");
                        break;
                        
                    default:
                        displayErrorMessage();
                        break;
                }
            } catch (Exception ex) {
                print(ex.getMessage());
                displayErrorMessage();
            }
        }

    }

    private static void displayHelp() {
        print("\n######## Methods of the class PlayControler ########");
        print("1 -> PlayControler(Player player1, Player player2, Board board, "
                + "ConfigControler configControler)");
        print("2 -> String getCurrentPlayerAsString()");
        print("3 -> boolean setCurrentPlayerFromString(String oneOrTwo)");
        print("4 -> int[] startNextTurn()");
        print("5 attr1 attr2 -> boolean playHumanMove(int[] selectedCell)");
        print("6 -> int[] giveAHint()");
        print("7 -> void undo()");
        print("8 -> void pauseGame()");
        print("9 -> void continueGame()");
        print("10 -> boolean isEndOfGame()");
        print("11 attr1 attr2 -> boolean defineSituation(int[] selectedCell)");
        print("12 -> Color[] getBoard()");
        print("13 -> Stack<Coordinates> getOldMoves()");
        print("14 -> void setOldMoves(Stack<Coordinates> oldMoves)");
        print("21 -> int getBoardSize()");
        print("22 attr1 -> int getTime(PlayerNumber player)"
                + "\t attr1 **must be** player1 or player2");
        print("23 attr1 -> int getScore(PlayerNumber player)"
                + "\t attr1 **must be** player1 or player2");
        print("24 -> Color getCurrentPlayerColor()");
                
        print("\n######## Tools to create parameters ########");
        print("15 attr1 attr2 attr3 attr4 -> changePlayer1(Color color, "
                + "int score, int timeRemaining, boolean isComputer)");
        print("16 attr1 attr2 attr3 attr4 -> changePlayer2(Color color, "
                + "int score, int timeRemaining, boolean isComputer)");
        print("17 attr1 -> enableUndo(boolean enabled)");
        print("18 -> createBroad()");
        print("19 attr1 attr2 -> addAMoveToTheStack(int x, int y)");
        print("20 -> clearStack()");
    }
}