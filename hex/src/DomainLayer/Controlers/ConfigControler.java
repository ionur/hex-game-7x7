package DomainLayer.Controlers;

import DomainLayer.Models.Configuration;
import Enumerations.Color;
import Enumerations.Level;
import Enumerations.Mode;
import Enumerations.NextTurnAfter;

/**
 * Controler for the Configuration model
 * @author Hex team
 */
public class ConfigControler {

    private Configuration config;

    /**
     * Constructor for ConfigControler: default config
     */
    public ConfigControler() {
        config = new Configuration();
    }

    /**
     * Parametrized constructor for ConfigControler
     * @param modeOfGame
     * @param undoEnabled
     * @param level
     * @param playingTimeInMs
     * @param nexTurnAfter
     * @param boardSize
     * @param player1Name
     * @param player2Name
     * @param player1Color
     */
    public ConfigControler(Mode modeOfGame,
            boolean undoEnabled,
            Level level,
            int playingTimeInMs,
            NextTurnAfter nexTurnAfter,
            int boardSize,
            String player1Name,
            String player2Name,
            Color player1Color) {

        config = new Configuration(
                modeOfGame,
                undoEnabled,
                level,
                playingTimeInMs,
                nexTurnAfter,
                boardSize,
                player1Name,
                player2Name,
                player1Color);
    }
    
    /**
     * 
     * @param configControler
     */
    public ConfigControler(ConfigControler configControler) {
        config = new Configuration(
                configControler.getMode(),
                configControler.isUndoEnabled(),
                configControler.getLevel(),
                configControler.getplayingTimeInMs(),
                configControler.getNextTurnAfter(),
                configControler.getBoardSize(),
                configControler.getPlayer1Name(),
                configControler.getPlayer2Name(),
                configControler.getPlayer1Color()
                );
    }

    /**
     * Setter for undoEnabled
     * @param enabled to say whether undo is enabled or not
     */
    public void setUndo(boolean enabled) {
        config.setUndoEnabled(enabled);
    }

    /**
     * Setter for the time
     * @param time the time in ms
     */
    public void setplayingTimeInMs(int time) {
        config.setplayingTimeInMs(time);
    }

    /**
     * Setter for nextTurnAfter, which is how we will go from a turn of the human player
     * to a turn of the computer player
     * @param newChoice the new value
     */
    public void setNextTurnAfter(NextTurnAfter newChoice) {
        config.setNexTurnAfter(newChoice);
    }

    /**
     * Setter for level
     * @param level the level to set
     */
    public void setLevel(Level level) {
        config.setLevel(level);
    }

    /**
     * Setter for mode
     * @param mode the mode to set
     */
    public void setMode(Mode mode) {
        config.setModeOfGame(mode);
    }

    /**
     * Setter for the size of the board
     * @param size the size to set
     */
    public void setBoardSize(int size) {
        config.setBoardSize(size);
    }

    /**
     * Setter for the name of player 1
     * @param name the name to set
     */
    public void setPlayer1Name(String name) {
        config.setPlayer1Name(name);
    }

    /**
     * Setter for the name of player2
     * @param name the name to set
     */
    public void setPlayer2Name(String name) {
        config.setPlayer2Name(name);

    }

    /**
     * Setter for the color of player1
     * @param player1Color the color to set
     */
    public void setPlayer1Color(Color player1Color) {
        config.setPlayer1Color(player1Color);
    }

    /**
     * Getter for undoEnabled
     * @return true if undoIsEnabled in the configuration
     */
    public boolean isUndoEnabled() {
        return config.isUndoEnabled();
    }

    /**
     * Getter for the playing time (in Ms)
     * @return the time of the configuration
     */
    public int getplayingTimeInMs() {
        return config.getplayingTimeInMs();
    }

    /**
     * Getter for nextTurnAfter
     * @return the value of nextTurnAfter of the configuration
     */
    public NextTurnAfter getNextTurnAfter() {
        return config.getNexTurnAfter();
    }

    /**
     * Getter for level
     * @return the level of the configuration
     */
    public Level getLevel() {
        return config.getLevel();
    }

    /**
     * Getter for the mode
     * @return the mode of the configuration
     */
    public Mode getMode() {
        return config.getModeOfGame();
    }

    /**
     * Getter for the board size
     * @return the board size of the configuration
     */
    public int getBoardSize() {
        return config.getBoardSize();
    }

    /**
     * Getter for the name of player1
     * @return the name of player1 in the configuration
     */
    public String getPlayer1Name() {
        return config.getPlayer1Name();
    }

    /**
     * Getter for the name of player2
     * @return the name of player2 in the configuration
     */
    public String getPlayer2Name() {
        return config.getPlayer2Name();
    }

    /**
     * Getter for the color of player1
     * @return the color of player1 in the configuration
     */
    public Color getPlayer1Color() {
        return config.getPlayer1Color();
    }

    /**
     * Transforms the configuration in a String to write it in a file
     * and retrieve it later, in another execution oh Hex for example
     * @return the String representation containing the configuration 
     * fields separated by ','
     */
    public String saveConfigAsString() {
        return config.getModeOfGame() + "," 
                + config.isUndoEnabled() + "," 
                + config.getLevel() + "," 
                + config.getplayingTimeInMs() + "," 
                + config.getNexTurnAfter() + "," 
                + config.getBoardSize() + "," 
                + config.getPlayer1Name() + "," 
                + config.getPlayer2Name() + "," 
                + config.getPlayer1Color();
    }

    /**
     * Do the exact opposite operation than saveAsString: retrieve the Configuration
     * object from the String
     * @param string a result of a previous saveAsString call
     * @return true if string has been parsed correctly and the configuration
     * successfully setted
     */
    public boolean setConfigFromString(String string) {
        try {
            String configuration[] = string.split(",");
            if (configuration.length != 9) {
                return false;
            }

            Mode mode = Mode.valueOf(configuration[0]);
            boolean undo = Boolean.parseBoolean(configuration[1]);
            Level level = Level.valueOf(configuration[2]);
            int time = Integer.parseInt(configuration[3]);
            NextTurnAfter turn = NextTurnAfter.valueOf(configuration[4]);
            int size = Integer.parseInt(configuration[5]);
            String p1Name = configuration[6];
            String p2Name = configuration[7];
            Color p1Color = Color.valueOf(configuration[8]);

            config = new Configuration(mode, undo, level, time, turn, size, 
                    p1Name, p2Name, p1Color);
            
            return true;
            
        } catch (Exception ex) {
            return false;
        }
    }
}