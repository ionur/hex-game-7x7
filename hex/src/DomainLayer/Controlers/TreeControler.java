/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DomainLayer.Controlers;

import Algorithms.Node;
import Algorithms.NodeRoot;
import Algorithms.Strategy;
import java.util.ArrayList;
import java.util.List;

/**
 * Create the treecontroler to pass the tree to the upper layer
 * @author hex team
 */
public class TreeControler {

    /**
     * Generate the tree with a list of elements
     * @return
     */
    public static ArrayList<int[]> getLastComputedTree() {
        ArrayList<int[]> treelist = new ArrayList<int[]>();
        List<NodeRoot> treeNode = Strategy.getTreeRoot().getTree();
        for (Node n : treeNode) {
            int[] ele = {n.depth, n.getWeight(), n.getx(), n.gety(), n.getz()};
            treelist.add(ele);
        }
        return treelist;
    }
}
