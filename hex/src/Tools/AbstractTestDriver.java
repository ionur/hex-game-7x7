package Tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Hex team
 */
public abstract class AbstractTestDriver {

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    /**
     * 
     * @return
     */
    protected static String[] getNewCmd() {
        try {
            System.out.print("> ");
            return reader.readLine().split(" ");
        } catch (IOException ex) {
            Logger.getLogger(AbstractTestDriver.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    /**
     * 
     * @param o 
     */
    protected static void print(Object o) {
        System.out.println(o);
    }

//    /**
//     * 
//     * @param correct
//     * @param methodName
//     */
//    protected static void validate(boolean correct, String methodName) {
//        if (correct) {
//            print("ok for " + methodName);
//        } else {
//            print("WARNING: ko for " + methodName);
//        }
//    }
    
    /**
     * 
     */
    protected static void displayRememberMessage() {
        print("Remember that:   0 -> help   -1 -> quit");
    }

    /**
     * 
     */
    protected static void displayErrorMessage() {
        print("#################################");
        print("ERROR, check the help please :-)");
        displayRememberMessage();
        print("#################################");
    }
    
    /**
     * 
     */
    protected static void displayWelcomeMessage() {
        print("Welcome in this driver, please input your commands.");
        displayRememberMessage();
    }
    
    /**
     * 
     */
    protected static void displayQuitting() {
        print("Quitting driver... end of tests.");
    }
}
